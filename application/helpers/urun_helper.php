<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');

if (! function_exists('GetUrun')) {
	/**
	 * this function return full product data
	 */
	function GetUrun($ref)
	{
		log_message('error','GetUrun calisti.');
		$CI = &get_instance();
//		log_message('error',$ref);
		if(isset($ref)){
//			log_message('error','if e girdi.');
			$obj = array ();
			/**
			 * it will get product categories
			 * tr: ürün kategorilerinin çekilmesi
			 */
			$CI->load->model('Sys_Kat_Baglanti_model');
			$obj['kategoriler'] = array();
			$result = $CI->Sys_Kat_Baglanti_model->Get(array('ref'=>$ref));
			if ($result['Result']) {
				$obj['kategoriler'] = $result['Data'];
			}

//			log_message('error',json_encode($obj));

			/**
			 * it will get product groups
			 * tr: ürün gruplarının bulunması (UT falan)
			 */
			$CI->load->model('Sys_Grup_Baglanti_model');
			$CI->load->model('Sys_Grup_model');
			$CI->load->model('Sys_Grup_Tip_model');
			$CI->load->model('Personel_model');
			$CI->load->model('Per_Iletisim_model');
			$obj['gruplar'] = array();
			/**
			 * first we will look for product categories
			 * tr: ilk olarak kategorilerin dahil olduğu gruplara bakılır
			 */
			if(isset($obj['kategoriler']) && count($obj['kategoriler'])>0){
				foreach($obj['kategoriler'] as $k){
					/**
					 * it will find group connections
					 * tr: grup bağlantılarını bulur
					 */
					$result = $CI->Sys_Grup_Baglanti_model->Get(array('gr_bag_tip'=>'U','gr_bag_kat'=>1,'gr_bag_id'=>$k->sys_kat_id));
//								log_message('error',json_encode($result));
					if ($result['Result']) {
						foreach($result['Data'] as $r){
							$obj['gruplar'][$r->ref] = array('ref'=>$r->ref);
						}
					}
				}
			}
			/**
			 * second we will look for product
			 * tr: ikinci olarak ürünün dahil olduğu gruplara bakılır
			 */
			$result = $CI->Sys_Grup_Baglanti_model->Get(array('gr_bag_tip'=>'U','gr_bag_kat'=>0,'gr_bag_id'=>$obj['urun']->id));
//			log_message('error',json_encode($result));
			if ($result['Result']) {
				foreach($result['Data'] as $r){
					$obj['gruplar'][$r->ref] = array('ref'=>$r->ref);
				}
			}
			//log_message('error',json_encode($obj['gruplar']));
			/**
			 * last we will look for group details
			 * tr: son olarak grupların detaylarına bakılır
			 */
			foreach($obj['gruplar'] as $k=>$v){
				$ref=$v["ref"];
				/**
				 * it will find group
				 * tr: grubu bulur
				 */
				$result = $CI->Sys_Grup_model->Get(array('ref'=>$ref,'gr_durum'=>'1'));
				if ($result['Result']) {
					$obj['gruplar'][$ref]['gad']=$result['Data'][0]->gr_baslik;
					$sorumlu = $result['Data'][0]->gr_sorumlu;
					$tip = $result['Data'][0]->gr_tip;
					/**
					 * it will find group type
					 * tr: grup tipini bulur
					 */
					$result = $CI->Sys_Grup_Tip_model->Get(array('id'=> $tip));
					if ($result['Result']) {
						$obj['gruplar'][$ref]['gtip'] = $result['Data'][0]->baslik_dil_key;
						$obj['gruplar'][$ref]['gtipid'] = $tip;

					}
					/**
					 * it will find group supervisor
					 * tr: grup yöneticisini bulur
					 */
					if(strlen(trim($sorumlu))>0){
						$result = $CI->Personel_model->Get(array('id'=>$sorumlu));
						if ($result['Result']) {
							$obj['gruplar'][$ref]['gsref'] =$result['Data'][0]->ref;
							$obj['gruplar'][$ref]['gsorumlu'] = $result['Data'][0]->per_ad;
							$obj['gruplar'][$ref]['gsimg'] = $result['Data'][0]->per_img;
							$result1 = $CI->Per_Iletisim_model->Get(array('ref'=>$result['Data'][0]->ref));
							if ($result1['Result']) {
								$obj['gruplar'][$ref]['gstel'] = $result1['Data'][0]->per_il_ceptel;
								$obj['gruplar'][$ref]['gsmail'] = $result1['Data'][0]->per_il_mail;
							}
						}
					}
				}
			}

			return array('message' => 'İşlem Başarılı..', 'result' => true, 'data' => $obj);
		} else {
			return array('message' => 'Referans Numarası Belirtiniz', 'result' => false);
		}

	}
}
