<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');
 
if (! function_exists('_checklogin')) {
    /**
     * this function will check user key is real or fake...
     */
    function _checklogin($key,$hash)
    {   
       
        $CI = & get_instance();
        $CI->load->model('Kul_Key_model');
        //if key is exist trasnaction will continue
        $Token =  $CI->Kul_Key_model->Get(array('kul_key_token' => $key, 'kul_key_hash' => $hash));
        if ($Token['Result']) {
            return true;
        }
        return false;
    }
}

if (! function_exists('_crypt')) {
    function _crypt($id)
    {
        // get main CodeIgniter object
        $CI =& get_instance();
        $password =md5((new DateTime(date('Y-m-d H:i:s')))->setTimezone(new DateTimeZone('Europe/Berlin'))->format('Y-m-d').$id);
        $key = substr(hash('sha256', $password, false), 0, 32);
        return base64_encode($key);
    }
}

if (! function_exists('_sendMail')) {
    function _sendMail($to,$value,$template)
    {   
            //sistemin mail atması
            /*$CI =& get_instance();
            $template = file_get_contents(__DIR__.'/mail_templates/'.$template);
            foreach($value as $key => $value)
            {
                $template = str_replace('{{ '.$key.' }}', $value, $template);
            }
            $CI->email->from('kbbciloglu@gmail.com', 'Sumo System');
            $CI->email->to($to);  
            $CI->email->subject('About Login Info');
            $CI->email->message($template);
            print_r($CI->email->send());
            die;*/
            return ['Result'=>true,'Data'=>''];
    }
}

if (! function_exists('_log')) {
    function _log($data)
    {   
        $CI = & get_instance();
        try{
            $CI->load->model('Sys_Log_model');
            $model =  $CI->Sys_Log_model;
            return $model->Add($data);
        }catch (Exception $th) {
            log_message('error',  $CI->db->last_query());
            $errsection = $CI->db->error();
            return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
        }
      
    }
}

