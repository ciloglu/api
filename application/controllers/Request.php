<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('display_errors', 1);
class Request extends CI_Controller {
	public function index() {
		//parameters did come from client
		$param = $this->input->post();
		$requestUri = $param['request'];
		if (substr($requestUri, 0, 1) !== '/') {
			// Geçersiz request formatı. Doğrusu: /controller/method
			die('WTF¿');
		}
		//gelen reques üzerinde sınıf ve metod ayrımı yapılır
		$req = explode('/', substr($requestUri, 1));
		$cont = $req[0];
		$meth = $req[1];
		//data is decrypted and converted to array
		$postData = json_decode(base64_decode($param['data']),true);
		//token check 
		if($meth != 'login' && $meth!='forgot'){
			$hash=md5($postData['_ip'] .$postData['_ua']);
			//token is checked
			if(!_checklogin($param['token'],$hash)){
				return $this->output
					->set_content_type('application/json')
					->set_status_header(403)
					->set_output(json_encode(['message' => 'Key yok hocam', 'result' => false]));
			}
		}
		//worker classes will include '_w'
		$cont .= '_w';
		//library is called
		$this->load->library($cont);
		//class is created
		$cl = new $cont();
		//data sended to function and response taken
		$response = $cl->$meth($postData);
		//response sended to client as json object with '200' header
		return $this->output
				->set_content_type('application/json')
				->set_status_header(200)
				->set_output(json_encode($response));
	}
}
