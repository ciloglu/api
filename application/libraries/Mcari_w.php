<?php defined('BASEPATH') or exit('No direct script access allowed');
class Mcari_w
{
    /**
     * @var CI_Controller
     *
     */
    private $CI;
    public function __construct()
    {
        $this->CI = &get_instance();
    }

    /**
     * this method will do client  transaction
     * if transactions is success it will return true
     * TR : Cari işlemlerinin gerçekleştiği bölgedir.
     */
    public function carievents($params)
    {
        $result = false;
        $errsection = "Post datası Hatalı ..";
        $referance = str_replace(".", "", microtime(true));
        try {
            switch ($params['event']) {
                case "getdetail":
                    $data = [];
                    /**
                     * it will load client model
                     * tr : cari modelini yükler
                     */
                    $this->CI->load->model('Cari_model');
                    /**
                     * it will return client
                     * tr : cariyi döner
                     */
                    $result = $this->CI->Cari_model->Get($params['where']);
                    if ($result['Result']) {
                        /**
                         * it will set client data
                         * tr: cari datasını değişkene atar
                         */
                        $data['cari'] = $result['Data'][0];
                        /**
                         * it will set country and city info
                         * tr: ülke ve şehir bilgisini çeker
                         */
                        $this->CI->load->model('Sys_Sehirler_model');
                        $this->CI->load->model('Sys_Ulkeler_model');
                        if(isset($data['cari']->cari_ulke)){
                            $result = $this->CI->Sys_Ulkeler_model->Get(['id'=>$data['cari']->cari_ulke]);
                            if ($result['Result']) {
                                $data['cari']->cari_ulke=$result['Data'][0]->sys_ul_baslik;
                            }
                        }
                        if(isset($data['cari']->cari_sehir)){
                            $result = $this->CI->Sys_Sehirler_model->Get(['id'=>$data['cari']->cari_sehir]);
                            if ($result['Result']) {
                                $data['cari']->cari_sehir=$result['Data'][0]->sys_seh_baslik;
                            }
                        }




                        /**
                         * it will load client address model
                         * tr: cari adresler modelini yükler
                         */

                        $data['adresler'] = [];
                        $this->CI->load->model('Sys_Adres_model');
                        $result = $this->CI->Sys_Adres_model->Get(['sys_ad_tip' => 1, 'ref' => $data['cari']->ref]);
                        if ($result['Result']) {
                            $this->CI->load->model('Sys_Adrestipleri_model');
                            foreach ($result['Data'] as $k) {
                                $result1 = $this->CI->Sys_Adrestipleri_model->Get(['id' => $k->sys_ad_kat]);
                                if ($result['Result']) {
                                    $k->sys_ad_kat = $result1['Data'][0]->baslik_dil_key;
                                }
                                if(isset($k->sys_ad_ulke)){
                                    $result = $this->CI->Sys_Ulkeler_model->Get(['id'=>$k->sys_ad_ulke]);
                                    if ($result['Result']) {
                                        $k->sys_ad_ulke=$result['Data'][0]->sys_ul_baslik;
                                    }
                                }
                                if(isset($k->sys_ad_sehir)){
                                    $result = $this->CI->Sys_Sehirler_model->Get(['id'=>$k->sys_ad_sehir]);
                                    if ($result['Result']) {
                                        $k->sys_ad_sehir=$result['Data'][0]->sys_seh_baslik;
                                    }
                                }
                                array_push($data['adresler'], $k);
                            }
                        }
                        /**
                         * it will load client cargo model
                         * tr: cari kargo modelini yükler
                         */
                        $this->CI->load->model('Cari_Kargo_model');
                        $result = $this->CI->Cari_Kargo_model->Get(['ref' => $data['cari']->ref]);
                        if ($result['Result']) {
                            $data['kargo'] = $result['Data'][0];
                        }
                        /**
                         * it will load client finance model
                         * tr: cari finans modelini yükler
                         */
                        $this->CI->load->model('Cari_Finans_model');
                        $result = $this->CI->Cari_Finans_model->Get(['ref' => $data['cari']->ref]);
                        if ($result['Result']) {
                            $data['finans'] = $result['Data'][0];
                        }
                        /**
                         * it will load client bank model
                         * tr: cari banka modelini yükler
                         */
                        $this->CI->load->model('Cari_Banka_model');
                        $result = $this->CI->Cari_Banka_model->Get(['ref' => $data['cari']->ref]);
                        $data['bankalar'] = [];
                        if ($result['Result']) {
                            /**
                             * it will load system banks model
                             * tr: sistem bankalar modelini yükler
                             */
                            $this->CI->load->model('Sys_Bankalar_model');
                            foreach ($result['Data'] as $b) {
                                $result1 = $this->CI->Sys_Bankalar_model->Get(['id' => $b->cari_ban_id]);
                                if ($result1['Result']) {
                                    $b->cari_banka = $result1['Data'][0]->sys_ban_baslik . ' / ' . $result1['Data'][0]->sys_ban_sube;
                                }
                                array_push($data['bankalar'], $b);
                            }
                        }

                        /**
                         * it will load client parameters model
                         * tr: cari parametreler modelini yükler
                         */
                        $this->CI->load->model('Cari_Parametre_model');
                        $result = $this->CI->Cari_Parametre_model->Get(['ref' => $data['cari']->ref]);
                        if ($result['Result']) {
                            $data['parametre'] = $result['Data'][0];
                        }

                        /**
                         * it will get client categories
                         * tr: cari kategorilerinin çekilmesi
                         */
                        $this->CI->load->model('Sys_Kat_Baglanti_model');
                        $data['kategoriler'] = []; 
                        $result = $this->CI->Sys_Kat_Baglanti_model->Get(['ref' => $data['cari']->ref]);
                        if ($result['Result']) {
                            $data['kategoriler'] = $result['Data'];
                        }
                        
                        /**
                         * it will get client personnels
                         * tr: cari personellerin çekilmesi
                         */
                        $this->CI->load->model('Cari_Yetkili_model');
                        $data['yetkililer'] = []; 
                        $result = $this->CI->Cari_Yetkili_model->Get(['ref' => $data['cari']->ref]);
                        if ($result['Result']) {
                             /**
                             * it will load client personnel positions model
                             * tr: cari personel pozisyonları modelini yükler
                             */
                            $this->CI->load->model('Per_Pozisyon_model');
                            foreach ($result['Data'] as $y) {
                                $result1 = $this->CI->Per_Pozisyon_model->Get(['id' => $b->cari_yet_pozisyon]);
                                if ($result1['Result']) {
                                    $y->cari_yet_pozisyon = $result1['Data'][0]->baslik_dil_key;
                                }
                                array_push($data['yetkililer'], $y);
                            }
                        }

                        /**
                         * it will get client produtcs
                         * tr: cari ürünlerinin çekilmesi
                         */
                        $this->CI->load->model('Cari_Urun_model');
                        $data['urunler'] = []; 
                        $result = $this->CI->Cari_Urun_model->Get(["cari_urun.ref"=>$data['cari']->ref]);
                        if ($result['Result']) {
                            $data['urunler']=$result['Data'];
                        }
                        
                        /**
                         * it will get client groups
                         * tr: cari gruplarının bulunması (UT falan)
                         */
                        $this->CI->load->model('Sys_Grup_Baglanti_model');
                        $this->CI->load->model('Sys_Grup_model');
                        $this->CI->load->model('Sys_Grup_Tip_model');
                        $this->CI->load->model('Personel_model');
                        $this->CI->load->model('Per_Iletisim_model');
                        $data['gruplar'] = []; 
                        /**
                         * first we will look for product categories
                         * tr: ilk olarak kategorilerin dahil olduğu gruplara bakılır
                         */
                        if(isset($data['kategoriler']) && count($data['kategoriler'])>0){
                           
                            foreach($data['kategoriler'] as $k){
                                /**
                                 * it will find group connections
                                 * tr: grup bağlantılarını bulur
                                 */
                                $result = $this->CI->Sys_Grup_Baglanti_model->Get(['gr_bag_tip'=>'C','gr_bag_kat'=>1,'gr_bag_id'=>$k->sys_kat_id]);
                                if ($result['Result']) {
                                    foreach($result['Data'] as $r){
                                        $obj['gruplar'][$r->ref] = ['ref'=>$r->ref];
                                    }
                                }
                            }
                        }
                        /**
                         * second we will look for client
                         * tr: ikinci olarak carinin dahil olduğu gruplara bakılır
                         */
                        $result = $this->CI->Sys_Grup_Baglanti_model->Get(['gr_bag_tip'=>'C','gr_bag_kat'=>0,'gr_bag_id'=>$data['cari']->id]);
                        if ($result['Result']) {
                            foreach($result['Data'] as $r){
                                $obj['gruplar'][$r->ref] = ['ref'=>$r->ref];
                            }
                        }

                        /**
                         * last we will look for group details
                         * tr: son olarak grupların detaylarına bakılır
                         */
                        foreach($data['gruplar'] as $k=>$v){
                            $ref=$v["ref"];
                            /**
                             * it will find group 
                             * tr: grubu bulur
                             */
                            $result = $this->CI->Sys_Grup_model->Get(['ref'=>$ref]);
                            if ($result['Result']) {
                                $data['gruplar'][$ref]['gad']=$result['Data'][0]->gr_baslik;
                                $sorumlu = $result['Data'][0]->gr_sorumlu;
                                $tip = $result['Data'][0]->gr_tip;
                                /**
                                 * it will find group type
                                 * tr: grup tipini bulur
                                 */
                                $result = $this->CI->Sys_Grup_Tip_model->Get(['id'=> $tip ]);
                                if ($result['Result']) {
                                    $data['gruplar'][$ref]['gtip'] = $result['Data'][0]->baslik_dil_key;
                                }
                                /**
                                 * it will find group supervisor
                                 * tr: grup yöneticisini bulur
                                 */
                                if(strlen(trim($sorumlu))>0){
                                    $result = $this->CI->Personel_model->Get(['id'=>$sorumlu]);
                                    if ($result['Result']) {
                                        $data['gruplar'][$ref]['gsref'] =$result['Data'][0]->ref;
                                        $data['gruplar'][$ref]['gsorumlu'] = $result['Data'][0]->per_ad;
                                        $data['gruplar'][$ref]['gsimg'] = $result['Data'][0]->per_img;
                                        $result1 = $this->CI->Per_Iletisim_model->Get(['ref'=>$result['Data'][0]->ref]);
                                        if ($result1['Result']) {
                                            $data['gruplar'][$ref]['gstel'] = $result1['Data'][0]->per_il_ceptel;
                                            $data['gruplar'][$ref]['gsmail'] = $result1['Data'][0]->per_il_mail;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $data];
                case "trash":
                    $referance = $params['ref'];
                    $this->CI->db->trans_begin();
                    /*1- cari trashing transaction 
                    tr : cari çöpe atma işlemi*/
                    /*error message 
                    tr : hata mesajı
                    */
                    $errsection = "cari Çöpe Atılırken Hata ref=>" . $referance;
                    /* cari model loading
                    tr :cari modelinin yüklenmesi 
                    */
                    $this->CI->load->model('Cari_model');
                    $model = $this->CI->Cari_model;
                    /* get cari data
                    tr : cari datanın alınması
                    */
                    $obj = (array) $model->Get(['ref' => $referance])['Data'][0];
                    /*make status '0'
                    tr : durumun 0 yapılması
                    */
                    $obj['cari_sdurum'] = 0;
                    $obj['updated_by'] = $params['_wid'];
                    $obj['updated_on'] = date('Y-m-d h:i:s');
                    /*model sended for update 
                    tr : model güncellemeye gönderildi
                    */
                    $result = $model->Update($obj);
                    /* if there is not any problem
                    tr: eğer bir sorun yok ise
                    */
                    if ($result['Result']) {
                        /* accept query
                        tr : sorguyu kabul et
                        */
                        $this->CI->db->trans_commit();
                        _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $params['cari']['cari_kadi'], 'log_tip' => 'cari', 'ref' => 'ref-' . $referance, 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_CariCop']);
                        return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
                    } else {
                        log_message('error',  $this->CI->db->last_query());
                        //$errsection = $this->CI->db->error();
                        $this->CI->db->trans_rollback();
                        return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
                    }
                    break;
                case "add":
                    $this->CI->db->trans_begin();
                    //1- clients table insert
                    //error message
                    $errsection = "Cari Tablosu Eklenirken Hata ref=>" . $referance;
                    //it will load client model
                    $this->CI->load->model('Cari_model');
                    $model = $this->CI->Cari_model;
                    //it will create object 
                    $cobj = $params['cari'];
                    $cobj['created_by'] = $params['_wid'];
                    $cobj['ref'] = $referance;
                    $cobj['cari_durum'] = $cobj['cari_durum'] == "" ? 0 : $cobj['cari_durum'];
                    unset($cobj['id']);
                    $result = $model->Add($cobj);
                    if ($result) {
                        if (isset($params['cari_kargo'])) {
                            //2- cargo table insert
                            //error message
                            $errsection = "Cari Kargo Tablosuna Eklenirken Hata ref=>" . $referance;
                            //it will load client cargo  model
                            $this->CI->load->model('Cari_Kargo_model');
                            $model = $this->CI->Cari_Kargo_model;
                            $crobj = $params['cari_kargo'];
                            $crobj['created_by'] =  $params['_wid'];
                            $crobj['ref'] = $referance;
                            unset($crobj['id']);
                            $result = $model->Add($crobj);
                        }
                        if (isset($params['cari_finans'])) {
                            //3- finance table insert
                            //error message
                            $errsection = "Cari Finans Tablosuna Eklenirken Hata ref=>" . $referance;
                            //it will load client finance model
                            $this->CI->load->model('Cari_Finans_model');
                            $model = $this->CI->Cari_Finans_model;
                            $fobj = $params['cari_finans'];
                            $fobj['created_by'] =  $params['_wid'];
                            $fobj['ref'] = $referance;
                            unset($fobj['id']);
                            $result = $model->Add($fobj);
                        }
                        if (isset($params['cari_parametre'])) {
                            //4- parameters table insert
                            //error message
                            $errsection = "Cari Parametre Tablosuna Eklenirken Hata ref=>" . $referance;
                            //it will load client parameter model
                            $this->CI->load->model('Cari_Parametre_model');
                            $model = $this->CI->Cari_Parametre_model;
                            $cpobj = $params['cari_parametre'];
                            $cpobj['created_by'] =  $params['_wid'];
                            //make routedays string
                            $cpobj['cari_par_tgunu'] = "{ " . implode(", ", $cpobj['cari_par_tgunu']) . " }";
                            $cpobj['ref'] = $referance;
                            unset($cpobj['id']);
                            $result = $model->Add($cpobj);
                        }
                        if (isset($params['cari_yetkili']) && count($params['cari_yetkili']) > 0) {
                            //5- caris table insert
                            //error message
                            $errsection = "Cari cari Tablosuna Eklenirken Hata ref=>" . $referance;
                            //it will load client caris model
                            $this->CI->load->model('Cari_Yetkili_model');
                            $model = $this->CI->Cari_Yetkili_model;
                            foreach ($params['cari_yetkili'] as $p) {
                                unset($p['PId']);
                                unset($p['id']);
                                unset($p['isUpdated']);
                                $p['ref'] = $referance;
                                $p['created_by'] =  $params['_wid'];
                                $result = $model->Add($p);
                            }
                        }
                        if (isset($params['cari_adres']) && count($params['cari_adres']) > 0) {
                            //5- addresses table insert
                            //error message
                            $errsection = "Cari Adres Tablosuna Eklenirken Hata ref=>" . $referance;
                            //it will load client address model
                            $this->CI->load->model('Sys_Adres_model');
                            $model = $this->CI->Sys_Adres_model;
                            foreach ($params['sys_adres'] as $a) {
                                unset($a['PAId']);
                                unset($a['id']);
                                unset($a['isUpdated']);
                                $a['ref'] = $referance;
                                $a['sys_ad_tip'] = '1';
                                $a['created_by'] =  $params['_wid'];
                                $result = $model->Add($a);
                            }
                        }
                        /**
                         * 8- client special price table inserts
                         * tr : cari özel fiyat tablosu eklenmeleri
                         */
                        if (isset($params['cari_urun']) && count($params['cari_urun']) > 0) {
                            //error message
                            $errsection = "Cari Urun Tablosu Güncellenirken Hata ref=>" . $referance;
                            //it will load client caris model
                            $this->CI->load->model('Cari_Urun_model');
                            $model = $this->CI->Cari_Urun_model;
                            foreach ($params['cari_urun'] as $p) {
                                //if record is new then insert
                                if ($p['id'] == '') {
                                    unset($p['PId']);
                                    unset($p['cari_ur_ad']);
                                    unset($p['id']);
                                    unset($p['isUpdated']);
                                    $p['ref'] = $referance;
                                    $p['created_by'] =  $params['_wid'];
                                    $result = $model->Add($p);
                                }
                            }
                        }
                        /**
                         * 6-Client category transactions
                         * tr :cari kategorilerinin eklenmesi
                         */
                        if (isset($params['cari_kategori']) && count($params['cari_kategori']) > 0) {
                            /*error message 
                            tr :hata mesajı
                            */
                            $errsection = "Cari Kategori Tablosuna Eklenirken Hata ref=>" . $referance;
                            /**
                             * cari category model loading
                             * tr :cari kategori modelinin yüklenmesi 
                             */
                            $this->CI->load->model('Sys_Kat_Baglanti_model');
                            $model = $this->CI->Sys_Kat_Baglanti_model;
                            $cobj = [];
                            /**
                             * each connection will send to database
                             * tr : her bağlantı veri tabanına gönderilir
                             */
                            foreach ($params['cari_kategori'] as $c) {
                                $cobj['sys_kat_id'] = $c;
                                $cobj['sys_kat_tip'] = 'CARI';
                                $cobj['ref'] = $referance;
                                $cobj['created_by'] = $params['_wid'];
                                $result = $model->Add($cobj);
                            }
                        }
                        /**
                         * Client user table insert
                         * tr: cari kullanıcı tablosu yerleştirme
                         */
                        if (isset($params['cari_kullanici'])) {
                            /** 
                             * error message 
                             * tr :hata mesajı
                             */
                            $errsection = "Cari Kullanıcılar Tablosuna Eklenirken Hata ref=>" . $referance;
                            /** 
                             * users model loading
                             * tr :personel kullanıcı modelinin yüklenmesi 
                             */
                            $this->CI->load->model('Kullanicilar_model');
                            $model = $this->CI->Kullanicilar_model;
                            /**
                             * set personel user object 
                             * tr : personel kullanıcı objesi atandı
                             */
                            $cobj = $params['cari_kullanici'];
                            $cobj['created_by'] =  $params['_wid'];
                            $cobj['kul_tip'] =  'C';
                            $cobj['ref'] = $referance;
                            /**
                             * there is not id created yet so jus unset it
                             * tr : henüz id oluşturulmadı o yüzden ne gelirse modelden at
                             */
                            unset($cobj['id']);
                            /**
                             *  send clean model for insert
                             * tr : temiz modeli ekleme için gönder
                             */
                            $result = $model->Add($cobj);
                        }
                        /**
                         * 9- client bank table inserts
                         * tr : cari banka tablosu eklenmeleri
                         */
                        if (isset($params['cari_banka']) && count($params['cari_banka']) > 0) {
                            //error message
                            $errsection = "Cari Banka Tablosu Eklenirken Hata ref=>" . $referance;
                            //it will load client caris model
                            $this->CI->load->model('Cari_Banka_model');
                            $model = $this->CI->Cari_Banka_model;
                            foreach ($params['cari_banka'] as $p) {
                                //if record is new then insert
                                if ($p['id'] == '') {
                                    unset($p['PBId']);
                                    unset($p['id']);
                                    unset($p['isUpdated']);
                                    $p['ref'] = $referance;
                                    $p['created_by'] =  $params['_wid'];
                                    $result = $model->Add($p);
                                }
                            }
                        }
                    }
                    //if result is true it will return true
                    if ($result['Result']) {
                        $this->CI->db->trans_commit();
                        _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $params['cari']['cari_kadi'], 'log_tip' => 'cari', 'ref' => 'ref-' . $referance, 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_CariEklendi']);
                        return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
                    } else {
                        $errsection = $this->CI->db->error();
                        $this->CI->db->trans_rollback();
                        return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => isset($errsection) ? $errsection : 'Data Yok.'];
                    }
                    break;
                case "get":
                    //it will load client model
                    $this->CI->load->model('Cari_model');
                    //it will load client cargo model
                    $this->CI->load->model('Cari_Kargo_model');
                    //it will load client finance model
                    $this->CI->load->model('Cari_Finans_model');
                    //it will load client parameter model
                    $this->CI->load->model('Cari_Parametre_model');
                    //it will load client caris model
                    $this->CI->load->model('Cari_Yetkili_model');
                    //it will load client product prices model
                    $this->CI->load->model('Cari_Urun_model');
                    //it will load client address model
                    $this->CI->load->model('Sys_Adres_model');
                    //it will load client bank model
                    $this->CI->load->model('Cari_Banka_model');
                    //it will load client users model
                    $this->CI->load->model('Kullanicilar_model');
                    $cari = $this->CI->Cari_model;
                    $kargo = $this->CI->Cari_Kargo_model;
                    $finans = $this->CI->Cari_Finans_model;
                    $parametre = $this->CI->Cari_Parametre_model;
                    $yetkili = $this->CI->Cari_Yetkili_model;
                    $adres = $this->CI->Sys_Adres_model;
                    $urun = $this->CI->Cari_Urun_model;
                    $banka = $this->CI->Cari_Banka_model;
                    $kullanici = $this->CI->Kullanicilar_model;
                    if (isset($params['where'])) {
                        $result = $cari->Get($params['where']);
                        $kargo = $kargo->Get($params['where']);
                        $finans = $finans->Get($params['where']);
                        $parametre = $parametre->Get($params['where']);
                        $yetkili = $yetkili->Get($params['where']);
                        $adres = $adres->Get($params['where']);
                        $kullanici = $kullanici->Get($params['where']);
                        $banka = $banka->Get($params['where']);
                        $params['where']['cari_urun.ref'] = $params['where']['ref'];
                        unset($params['where']['ref']);
                        $urun = $urun->Get($params['where']);
                        $obj = [
                            'cari' => isset($result['Data']) ? $result['Data'][0] : [],
                            'kargo' => isset($kargo['Data']) ? $kargo['Data'][0] : [],
                            'finans' => isset($finans['Data']) ? $finans['Data'][0] : [],
                            'parametre' => isset($parametre['Data']) ? $parametre['Data'][0] : [],
                            'yetkili' => isset($yetkili['Data']) ? $yetkili['Data'] : [],
                            'adres' => isset($adres['Data']) ? $adres['Data'] : [],
                            'urun' => isset($urun['Data']) ? $urun['Data'] : [],
                            'kullanici' => isset($kullanici['Data']) ? $kullanici['Data'] : [],
                            'banka' => isset($banka['Data']) ? $banka['Data'] : [],
                        ];
                        return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $obj];
                    } else {
                        return ['message' => 'Referans Numarası Belirtiniz', 'result' => false];
                    }
                    break;
                case "update":
                    $referance = $params['ref'];
                    $this->CI->db->trans_begin();
                    /**
                     * 1- Client table  update
                     * tr  : cari tablosunun güncellenmesi
                     */
                    if (isset($params['cari'])) {
                        //error message
                        $errsection = "Cari Tablosu Güncelenirken Hata ref=>" . $referance;
                        //it will load client model
                        $this->CI->load->model('Cari_model');
                        $model = $this->CI->Cari_model;
                        //update client
                        $obj = (array) $model->Get(['id' => $params['cari']['id']])['Data'][0];
                        foreach ($params['cari'] as $k => $v) {
                            if (array_key_exists($k, $obj)) {
                                $obj[$k] = $v;
                            }
                        }
                        $obj['cari_durum'] = $params['cari']['cari_durum'] == "" ? 0 : $params['cari']['cari_durum'];
                        $obj['updated_by'] = $params['_wid'];
                        $obj['updated_on'] = date('Y-m-d h:i:s');
                        $result = $model->Update($obj);
                    }
                    /**
                     * 2- client cargo table update
                     * tr: cari kargo tablosunun güncellenmesi
                     */
                    if (isset($params['cari_kargo'])) {
                        //error message
                        $errsection = "Cari Kargo Tablosu Güncellenirken Hata ref=>" . $referance;
                        //it will load client cargo  model
                        $this->CI->load->model('Cari_Kargo_model');
                        $model = $this->CI->Cari_Kargo_model;
                        if ($params['cari_kargo']['id'] != '' && $params['cari_kargo']['id'] != '0') {
                            $obj = (array) $model->Get(['id' => $params['cari_kargo']['id']])['Data'][0];
                            foreach ($params['cari_kargo'] as $k => $v) {
                                if (array_key_exists($k, $obj)) {
                                    $obj[$k] = $v;
                                }
                            }
                            $obj['updated_by'] = $params['_wid'];
                            $obj['updated_on'] = date('Y-m-d h:i:s');
                            $result = $model->Update($obj);
                        } else {
                            $crobj = $params['cari_kargo'];
                            $crobj['created_by'] =  $params['_wid'];
                            $crobj['ref'] = $referance;
                            unset($crobj['id']);
                            $result = $model->Add($crobj);
                        }
                    }
                    /**
                     * 3- client finance table update
                     * tr: cari finans tablosunun güncellenmesi
                     */
                    if (isset($params['cari_finans'])) {
                        //error message
                        $errsection = "Cari Finans Güncelenirken Hata ref=>" . $referance;
                        //it will load client finans model
                        $this->CI->load->model('Cari_Finans_model');
                        $model = $this->CI->Cari_Finans_model;
                        if ($params['cari_finans']['id'] != '' && $params['cari_finans']['id'] != '0') {
                            $obj = (array) $model->Get(['id' => $params['cari_finans']['id']])['Data'][0];
                            foreach ($params['cari_finans'] as $k => $v) {
                                if (array_key_exists($k, $obj)) {
                                    $obj[$k] = $v;
                                }
                            }
                            $obj['updated_by'] = $params['_wid'];
                            $obj['updated_on'] = date('Y-m-d h:i:s');
                            $result = $model->Update($obj);
                        } else {
                            $fobj = $params['cari_finans'];
                            $fobj['created_by'] =  $params['_wid'];
                            $fobj['ref'] = $referance;
                            unset($fobj['id']);
                            $result = $model->Add($fobj);
                        }
                    }
                    /**
                     * 4- client parameters table update
                     * tr : cari parammetre tablosunun güncellenmesi
                     */
                    if (isset($params['cari_parametre'])) {
                        //error message
                        $errsection = "Cari Parametre Güncelenirken Hata ref=>" . $referance;
                        //it will load client parameters model
                        $this->CI->load->model('Cari_Parametre_model');
                        $model = $this->CI->Cari_Parametre_model;
                        if ($params['cari_parametre']['id'] != '' && $params['cari_parametre']['id'] != '0') {
                            $obj = (array) $model->Get(['id' => $params['cari_parametre']['id']])['Data'][0];
                            foreach ($params['cari_parametre'] as $k => $v) {
                                if (array_key_exists($k, $obj)) {
                                    $obj[$k] = $v;
                                }
                            }
                            $obj['cari_par_tgunu'] = "{ " . implode(", ", $params['cari_parametre']['cari_par_tgunu']) . " }";
                            $obj['updated_by'] = $params['_wid'];
                            $obj['updated_on'] = date('Y-m-d h:i:s');
                            $result = $model->Update($obj);
                        } else {
                            $cpobj = $params['cari_parametre'];
                            $cpobj['created_by'] =  $params['_wid'];
                            //make routedays string
                            $cpobj['cari_par_tgunu'] = "{ " . implode(", ", $cpobj['cari_par_tgunu']) . " }";
                            $cpobj['ref'] = $referance;
                            unset($cpobj['id']);
                            $result = $model->Add($cpobj);
                        }
                    }
                    /**
                     * 5- client personnel table  update
                     * tr : cari personel tablosunun güncellenmesi 
                     */
                    if (isset($params['cari_yetkili']) && count($params['cari_yetkili']) > 0) {
                        //error message
                        $errsection = "Cari cari Tablosu Güncellenirken Hata ref=>" . $referance;
                        //it will load client caris model
                        $this->CI->load->model('Cari_Yetkili_model');
                        $model = $this->CI->Cari_Yetkili_model;
                        foreach ($params['cari_yetkili'] as $p) {
                            log_message('error', json_encode($p));
                            //if record is new then insert
                            if ($p['id'] == '') {
                                unset($p['PId']);
                                unset($p['id']);
                                unset($p['isUpdated']);
                                $p['ref'] = $referance;
                                $p['created_by'] =  $params['_wid'];
                                $result = $model->Add($p);
                            } else {
                                //or update
                                if ($p['isUpdated'] == 1) {
                                    $obj = (array) $model->Get(['id' => $p['id']])['Data'][0];
                                    foreach ($p as $k => $v) {
                                        if (array_key_exists($k, $obj)) {
                                            $obj[$k] = $v;
                                        }
                                    }
                                    $obj['updated_by'] = $params['_wid'];
                                    $obj['updated_on'] = date('Y-m-d h:i:s');
                                    $result = $model->Update($obj);
                                }
                            }
                        }
                    }
                    /**
                     * 6- client address table update
                     * tr: cari adres tablosunun  güncellenmesi
                     */
                    if (isset($params['sys_adres']) && count($params['sys_adres']) > 0) {
                        //error message
                        $errsection = "Cari Adres Tablosu Güncellenirken Hata ref=>" . $referance;
                        //it will load client caris model
                        $this->CI->load->model('Sys_Adres_model');
                        $model = $this->CI->Sys_Adres_model;
                        foreach ($params['sys_adres'] as $p) {
                            //if record is new then insert
                            if ($p['id'] == '') {
                                unset($p['PAId']);
                                unset($p['id']);
                                unset($p['isUpdated']);
                                $p['ref'] = $referance;
                                $p['sys_ad_tip'] = '1';
                                $p['created_by'] =  $params['_wid'];
                                $result = $model->Add($p);
                            } else {
                                //or update
                                if ($p['isUpdated'] == 1) {
                                    $obj = (array) $model->Get(['id' => $p['id']])['Data'][0];
                                    foreach ($p as $k => $v) {
                                        if (array_key_exists($k, $obj)) {
                                            $obj[$k] = $v;
                                        }
                                    }
                                    $obj['updated_by'] = $params['_wid'];
                                    $obj['updated_on'] = date('Y-m-d h:i:s');
                                    $result = $model->Update($obj);
                                }
                            }
                        }
                    }
                    /**
                     * 7- personel category transactions
                     * tr : personel kategorilerinin eklenmesi
                     */
                    if (isset($params['cari_kategori']) && count($params['cari_kategori']) > 0) {
                        /**
                         * error message 
                         * tr :hata mesajı
                         */
                        $errsection = "Cari Kategori Tablosuna Eklenirken Hata ref=>" . $referance;
                        /**
                         * personel category model loading
                         *tr :personel kategori modelinin yüklenmesi 
                         */
                        $this->CI->load->model('Sys_Kat_Baglanti_model');
                        $model = $this->CI->Sys_Kat_Baglanti_model;
                        /**
                         * first remove old category connections for referance
                         * tr : ilk olarak eski kategori bağlantıları silinir
                         */
                        $model->Remove(array('ref' => $referance));
                        $cobj = [];
                        /**
                         * each connection will send to database
                         * tr : her bağlantı veri tabanına gönderilir
                         */
                        foreach ($params['cari_kategori'] as $c) {
                            $cobj['sys_kat_id'] = $c;
                            $cobj['sys_kat_tip'] = 'CARI';
                            $cobj['ref'] = $referance;
                            $cobj['created_by'] = $params['_wid'];
                            $result = $model->Add($cobj);
                        }
                    }
                    /**
                     * 8- client special price table update
                     * tr : cari özel fiyat tablosu güncellemesi
                     */
                    if (isset($params['cari_urun']) && count($params['cari_urun']) > 0) {
                        //error message
                        $errsection = "Cari Urun Tablosu Güncellenirken Hata ref=>" . $referance;
                        //it will load client caris model
                        $this->CI->load->model('Cari_Urun_model');
                        $model = $this->CI->Cari_Urun_model;
                        foreach ($params['cari_urun'] as $p) {
                            //if record is new then insert
                            if ($p['id'] == '') {
                                unset($p['PId']);
                                unset($p['id']);
                                unset($p['isUpdated']);
                                unset($p['cari_ur_ad']);
                                $p['ref'] = $referance;
                                $p['created_by'] =  $params['_wid'];
                                $result = $model->Add($p);
                            } else {
                                //or update
                                if ($p['isUpdated'] == 1) {
                                    $obj = (array) $model->Get(['cari_urun.id' => $p['id']])['Data'][0];
                                    foreach ($p as $k => $v) {
                                        if (array_key_exists($k, $obj)) {
                                            $obj[$k] = $v;
                                        }
                                    }
                                    unset($obj['cari_ur_ad']);
                                    unset($obj['ad']);
                                    $obj['updated_by'] = $params['_wid'];
                                    $obj['updated_on'] = date('Y-m-d h:i:s');
                                    $result = $model->Update($obj);
                                }
                            }
                        }
                    }
                    /**
                     * 9- Client user table insert
                     * tr: cari kullanıcı tablosu yerleştirme
                     */
                    if (isset($params['cari_kullanici'])) {
                        /** 
                         * error message 
                         * tr :hata mesajı
                         */
                        $errsection = "Cari Kullanıcı Güncelenirken Hata ref=>" . $referance;
                        /** 
                         * users model loading
                         * tr :personel kullanıcı modelinin yüklenmesi 
                         */
                        $this->CI->load->model('Kullanicilar_model');
                        $model = $this->CI->Kullanicilar_model;
                        if ($params['cari_kullanici']['id'] != '' && $params['cari_kullanici']['id'] != '0') {
                            /**
                             * set personel user object from database
                             * tr : personel kullanıcı objesi veri tabanından atandı
                             */
                            $obj = (array) $model->Get(['id' => $params['cari_kullanici']['id']])['Data'][0];
                            foreach ($params['cari_kullanici'] as $k => $v) {
                                if (array_key_exists($k, $obj)) {
                                    $obj[$k] = $v;
                                }
                            }
                            $obj['updated_by'] = $params['_wid'];
                            $obj['updated_on'] = date('Y-m-d h:i:s');
                            $obj['kul_tip'] = 'C';
                            $result = $model->Update($obj);
                        } else {
                            /**
                             * set personel user object from database
                             * tr : personel kullanıcı objesi veri tabanından atandı
                             */
                            $obj = $params['cari_kullanici'];
                            $obj['created_by'] =  $params['_wid'];
                            $obj['ref'] = $referance;
                            $obj['kul_tip'] = 'C';
                            unset($obj['id']);
                            $result = $model->Add($obj);
                        }
                    }
                    /**
                     * 10- client bank table update
                     * tr : cari banka tablosu güncellemesi
                     */
                    if (isset($params['cari_banka']) && count($params['cari_banka']) > 0) {
                        //error message
                        $errsection = "Cari Banka Tablosu Güncellenirken Hata ref=>" . $referance;
                        //it will load client caris model
                        $this->CI->load->model('Cari_Banka_model');
                        $model = $this->CI->Cari_Banka_model;
                        foreach ($params['cari_banka'] as $p) {
                            //if record is new then insert
                            if ($p['id'] == '') {
                                unset($p['PBId']);
                                unset($p['id']);
                                unset($p['isUpdated']);
                                $p['ref'] = $referance;
                                $p['created_by'] =  $params['_wid'];
                                $result = $model->Add($p);
                            } else {
                                //or update
                                if ($p['isUpdated'] == 1) {
                                    $obj = (array) $model->Get(['id' => $p['id']])['Data'][0];
                                    foreach ($p as $k => $v) {
                                        if (array_key_exists($k, $obj)) {
                                            $obj[$k] = $v;
                                        }
                                    }
                                    $obj['updated_by'] = $params['_wid'];
                                    $obj['updated_on'] = date('Y-m-d h:i:s');
                                    $result = $model->Update($obj);
                                }
                            }
                        }
                    }
                    if ($result['Result']) {
                        $this->CI->db->trans_commit();
                        _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $params['cari']['cari_kadi'], 'log_tip' => 'cari', 'ref' => 'ref-' . $referance, 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_CariGuncellendi']);
                        return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
                    } else {
                        $errsection = $this->CI->db->error();
                        $this->CI->db->trans_rollback();
                        return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => isset($result['Data']) ? $result['Data'] : 'Data Yok.'];
                    }
                    break;
            }
        } catch (Exception $th) {
            log_message('error', 'message : ' . $th);
            $this->CI->db->trans_rollback();
            $errsection = $this->CI->db->error();
            return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
        }
    }
}
