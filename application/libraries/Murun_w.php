<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Current class is only use for product transactions
 * every return in system is have same pattern
 * it will return 'result' as true or false , and  'data' key for transaction result
 * example return is '['message'=>'Hata Oluştu..','result'=>false]'
 */

class Murun_w
{
    private $CI;
    /**
     * constructor for calling CI 
     */
    public function __construct()
    {
        $this->CI = &get_instance();
    }

    /**
     * this method is for product transactions
     * tr : bu metod ürün işlemleri için kullanılır
     */
    public function urunevents($params)
    {
        $result = false;
        $errsection = "Post datası Hatalı ..";
        $referance = str_replace(".", "", microtime(true));
        try {
            switch ($params['event']) {
                case "getdetail":
                    $obj = array();
                    /**
                     * it will load personel model
                     * tr : personel modelini yükler
                     */
                    $this->CI->load->model('Urun_model');
                    $model = $this->CI->Urun_model;
                    if (isset($params['where'])) {
                        $result = $model->Get($params['where']);
                        /**
                         * if product is exists
                         * tr: ürün mevcut ise
                         */
                        if ($result['Result']) {
                            $obj['urun'] = $result['Data'][0];
                            /**
                             * it will get product parameters
                             * tr: ürün parametrelerinin çekilmesi
                             */
                            $this->CI->load->model('Urun_Parametre_model');
                            $result = $this->CI->Urun_Parametre_model->Get($params['where']);
                            if ($result['Result']) {
                                $obj['parametre'] = $result['Data'][0];
                            }

                            /**
                             * it will get product units
                             * tr: ürün birimlerinin çekilmesi
                             */
                            $this->CI->load->model('Urun_Birimler_model');
                            $this->CI->load->model('Urun_Birim_model');
                            $obj['birimler'] = []; 
                            $result = $this->CI->Urun_Birimler_model->Get($params['where']);
                            
                            if ($result['Result']) {
                                $resb = $this->CI->Urun_Birim_model->Get(["ur_bir_durum"=>1]);
                                foreach($result['Data'] as $u){
                                    $u->birim_dil_key=$resb['Data'][array_search($u->ur_bir_id,array_column($resb['Data'], 'id'))]->baslik_dil_key;
                                    $u->cbirim_dil_key=$resb['Data'][array_search($u->ur_bir_cid,array_column($resb['Data'], 'id'))]->baslik_dil_key;
                                    array_push($obj['birimler'],$u);
                                }
                            }
                            /**
                             * it will get product prices
                             * tr: ürün fiyatlarının çekilmesi
                             */
                            $this->CI->load->model('Urun_Fiyatlar_model');
                            $this->CI->load->model('Sys_Pbirim_model');
                            $this->CI->load->model('Sys_Fiyatlar_model');
                            $this->CI->load->model('Sys_Ulkeler_model');
                            $obj['fiyatlar'] = []; 
                            $result = $this->CI->Urun_Fiyatlar_model->Get($params['where']);
                            $resb = $this->CI->Sys_Pbirim_model->Get(["sys_pbir_durum"=>1]);
                            $resc = $this->CI->Sys_Fiyatlar_model->Get(["sys_fi_durum"=>1]);
                            $resd = $this->CI->Sys_Ulkeler_model->Get(["sys_ul_durum"=>1]);
                            if ($result['Result']) {
                                
                                foreach($result['Data'] as $u){
                                    $u->pbirim = $resb['Data'][array_search($u->ur_fi_pbirim,array_column($resb['Data'], 'id'))]->sys_pbir_icon;
                                    $u->fiyat_dil_key = $resc['Data'][array_search($u->ur_fi_id,array_column($resc['Data'], 'id'))]->baslik_dil_key;
                                    $u->ulke_dil_key = $resd['Data'][array_search($u->ur_fi_ulid,array_column($resd['Data'], 'id'))]->baslik_dil_key;
                                    array_push($obj['fiyatlar'],$u);
                                }
                            }
                            /**
                             * it will get product prices
                             * tr: ürün fiyatlarının çekilmesi
                             */
                            $this->CI->load->model('Urun_Vergiler_model');
                            $obj['vergiler'] = []; 
                            $result = $this->CI->Urun_Vergiler_model->Get($params['where']);
                            if ($result['Result']) {
                                foreach($result['Data'] as $u){
                                    $u->ulke_dil_key = $resd['Data'][array_search($u->ur_fi_ulid,array_column($resd['Data'], 'id'))]->baslik_dil_key;
                                    array_push($obj['vergiler'],$u);
                                }
                            }
                            
                            /**
                             * it will get product categories
                             * tr: ürün kategorilerinin çekilmesi
                             */
                            $this->CI->load->model('Sys_Kat_Baglanti_model');
                            $obj['kategoriler'] = []; 
                            $result = $this->CI->Sys_Kat_Baglanti_model->Get($params['where']);
                            if ($result['Result']) {
                                $obj['kategoriler'] = $result['Data'];
                            }



                            /**
                             * it will get product groups
                             * tr: ürün gruplarının bulunması (UT falan)
                             */
                            $this->CI->load->model('Sys_Grup_Baglanti_model');
                            $this->CI->load->model('Sys_Grup_model');
                            $this->CI->load->model('Sys_Grup_Tip_model');
                            $this->CI->load->model('Personel_model');
                            $this->CI->load->model('Per_Iletisim_model');
                            $obj['gruplar'] = []; 
                            /**
                             * first we will look for product categories
                             * tr: ilk olarak kategorilerin dahil olduğu gruplara bakılır
                             */
                            if(isset($obj['kategoriler']) && count($obj['kategoriler'])>0){
                                foreach($obj['kategoriler'] as $k){
                                    /**
                                     * it will find group connections
                                     * tr: grup bağlantılarını bulur
                                     */
                                    $result = $this->CI->Sys_Grup_Baglanti_model->Get(['gr_bag_tip'=>'U','gr_bag_kat'=>1,'gr_bag_id'=>$k->sys_kat_id]);
                                    if ($result['Result']) {
                                        foreach($result['Data'] as $r){
                                            $obj['gruplar'][$r->ref] = ['ref'=>$r->ref];
                                        }
                                    }
                                }
                            }
                            /**
                             * second we will look for product
                             * tr: ikinci olarak ürünün dahil olduğu gruplara bakılır
                             */
                            $result = $this->CI->Sys_Grup_Baglanti_model->Get(['gr_bag_tip'=>'U','gr_bag_kat'=>0,'gr_bag_id'=>$obj['urun']->id]);
                            //log_message('error',json_encode($result['Data']));
                            if ($result['Result']) {
                                foreach($result['Data'] as $r){
                                    $obj['gruplar'][$r->ref] = ['ref'=>$r->ref];
                                }
                            }
                            //log_message('error',json_encode($obj['gruplar']));
                            /**
                             * last we will look for group details
                             * tr: son olarak grupların detaylarına bakılır
                             */
                            foreach($obj['gruplar'] as $k=>$v){
                                $ref=$v["ref"];
                                /**
                                 * it will find group 
                                 * tr: grubu bulur
                                 */
                                $result = $this->CI->Sys_Grup_model->Get(array('ref'=>$ref));
                                if ($result['Result']) {
                                    $obj['gruplar'][$ref]['gad']=$result['Data'][0]->gr_baslik;
                                    $sorumlu = $result['Data'][0]->gr_sorumlu;
                                    $tip = $result['Data'][0]->gr_tip;
                                    /**
                                     * it will find group type
                                     * tr: grup tipini bulur
                                     */
                                    $result = $this->CI->Sys_Grup_Tip_model->Get(['id'=> $tip ]);
                                    if ($result['Result']) {
                                        $obj['gruplar'][$ref]['gtip'] = $result['Data'][0]->baslik_dil_key;
                                    }
                                    /**
                                     * it will find group supervisor
                                     * tr: grup yöneticisini bulur
                                     */
                                    if(strlen(trim($sorumlu))>0){
                                        $result = $this->CI->Personel_model->Get(['id'=>$sorumlu]);
                                        if ($result['Result']) {
                                            $obj['gruplar'][$ref]['gsref'] =$result['Data'][0]->ref;
                                            $obj['gruplar'][$ref]['gsorumlu'] = $result['Data'][0]->per_ad;
                                            $obj['gruplar'][$ref]['gsimg'] = $result['Data'][0]->per_img;
                                            $result1 = $this->CI->Per_Iletisim_model->Get(['ref'=>$result['Data'][0]->ref]);
                                            if ($result1['Result']) {
                                                $obj['gruplar'][$ref]['gstel'] = $result1['Data'][0]->per_il_ceptel;
                                                $obj['gruplar'][$ref]['gsmail'] = $result1['Data'][0]->per_il_mail;
                                            }
                                        }
                                    }
                                }
                            }






                            return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $obj];
                        } else {
                            return ['message' => 'Referans Numarası Belirtiniz', 'result' => false];
                        }
                    }
                    break;
                case "trash":
                    $referance = $params['ref'];
                    $this->CI->db->trans_begin();
                    /**
                     * 1- product trashing transaction 
                     * tr : personel çöpe atma işlemi
                     */
                    /**
                     * error message 
                     * tr : hata mesajı
                     */
                    $errsection = "Ürün Çöpe Atılırken Hata ref=>" . $referance;
                    /**
                     *  personel model loading
                     * tr :personel modelinin yüklenmesi 
                     */
                    $this->CI->load->model('Urun_model');
                    $model = $this->CI->Urun_model;
                    /**
                     *  get personel data
                     * tr : personel datanın alınması
                     */
                    $obj = (array) $model->Get(['ref' => $referance])['Data'][0];
                    /** 
                     * make status '0'
                     * tr: durumun 0 yapılması
                     */
                    $obj['urun_durum'] = 0;
                    $obj['updated_by'] = $params['_wid'];
                    $obj['updated_on'] = date('Y-m-d h:i:s');
                    /**
                     * model sended for update 
                     * tr : model güncellemeye gönderildi
                     */
                    $result = $model->Update($obj);
                    /**
                     * if there is not any problem
                     * tr: eğer bir sorun yok ise
                     */
                    if ($result['Result']) {
                        /**
                         * accept query
                         * tr : sorguyu kabul et
                         */
                        $this->CI->db->trans_commit();
                        _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $obj['per_ad'], 'log_tip' => 'urun', 'ref' => 'ref-' . $referance, 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_UrCop']);
                        return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
                    } else {
                        log_message('error',  $this->CI->db->last_query());
                        //$errsection = $this->CI->db->error();
                        $this->CI->db->trans_rollback();
                        return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
                    }
                    break;
				case "untrash":
					$referance = $params['ref'];
					$this->CI->db->trans_begin();
					/**
					 * 1- product untrashing transaction
					 * tr : çöpten geri alma işlemi
					 */
					/**
					 * error message
					 * tr : hata mesajı
					 */
					$errsection = "Ürün Çöpten Geri Alınırken Hata ref=>" . $referance;
					/**
					 *  personel model loading
					 * tr :personel modelinin yüklenmesi
					 */
					$this->CI->load->model('Urun_model');
					$model = $this->CI->Urun_model;
					/**
					 *  get personel data
					 * tr : personel datanın alınması
					 */
					$obj = (array) $model->Get(['ref' => $referance])['Data'][0];
					/**
					 * make status '1'
					 * tr: durumun 1 yapılması
					 */
					$obj['urun_durum'] = 1;
					$obj['updated_by'] = $params['_wid'];
					$obj['updated_on'] = date('Y-m-d h:i:s');
					/**
					 * model sended for update
					 * tr : model güncellemeye gönderildi
					 */
					$result = $model->Update($obj);
					/**
					 * if there is not any problem
					 * tr: eğer bir sorun yok ise
					 */
					if ($result['Result']) {
						/**
						 * accept query
						 * tr : sorguyu kabul et
						 */
						$this->CI->db->trans_commit();
						_log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $obj['per_ad'], 'log_tip' => 'urun', 'ref' => 'ref-' . $referance, 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_UrCop']);
						return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
					} else {
						log_message('error',  $this->CI->db->last_query());
						//$errsection = $this->CI->db->error();
						$this->CI->db->trans_rollback();
						return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
					}
					break;
                case "update":
                    /**
                     * get product referance 
                     * tr: urun referansını al
                     */
                    $referance = $params['ref'];
                    $this->CI->db->trans_begin();
                    /**
                     * 1- update main product table
                     * tr : ürün tablosunun güncellenmesi
                     * if product object is received
                     * tr : eğer urun objesi alınmış ise
                     */
                    if (isset($params['urun'])) {
                        /**
                         * error message 
                         * tr :hata mesajı
                         */
                        $errsection = "Urun Tablosu Güncelenirken Hata ref=>" . $referance;
                        /**
                         * Product model loading
                         * tr :urun modelinin yüklenmesi 
                         */
                        $this->CI->load->model('Urun_model');
                        $model = $this->CI->Urun_model;
                        /**
                         * get product object
                         * tr: ürün objesini çek
                         */
                        $obj = (array) $model->Get(['id' => $params['urun']['id']])['Data'][0];
                        /**
                         * update product object
                         * tr: ürün objesini güncelle 
                         */
                        foreach ($params['urun'] as $k => $v) {
                            if (array_key_exists($k, $obj)) {
                                $obj[$k] = $v;
                            }
                        }
                        $obj['urun_durum'] = $params['urun']['urun_durum'] == "" ? 1 : $params['urun']['urun_durum'];
                        $obj['updated_by'] = $params['_wid'];
                        $obj['updated_on'] = date('Y-m-d h:i:s');
                        /**
                         * send object to db for update
                         * tr: urun objesini güncelleme için gönder
                         */
                        $result = $model->Update($obj);
                    }
                    /**
                     * 2- Update product language data
                     * tr: Urun dil dosyaları güncellenir
                     */
                    if (isset($params['language']['urun_ad'])) {
                        $this->CI->load->model('Sys_Ceviriler_model');
                        $model = $this->CI->Sys_Ceviriler_model;
                        /**
                         * remove old translate data
                         * tr: eski çevirileri sil
                         */
                        $model->Remove(array('anahtar' => $params['urun']['baslik_dil_key']));
                        /**
                         * insert language informations 
                         * tr:dil girdilerinin yerleştirilmesi
                         */
                        if (count($params['language']['urun_ad']) > 0) {
                            foreach ($params['language']['urun_ad'] as $l) {
                                $obj =  array(
                                    'anahtar' => $params['urun']['baslik_dil_key'],
                                    'sys_cev_dil_id' => $l['langid'],
                                    'sys_cev_sayfa' => 'urunler',
                                    'sys_cev_ceviri' => $l['value']
                                );
                                $result = $model->Add($obj);
                            }
                        }
                    }
                    /**
                     * 3- product category transactions
                     * tr: ürün kategorilerinin eklenmesi
                     */
                    if (isset($params['urun_kategori']) && count($params['urun_kategori']) > 0) {
                        /**
                         * error message 
                         * tr :hata mesajı
                         */
                        $errsection = "Urun Kategori Tablosuna Eklenirken Hata ref=>" . $referance;
                        /**
                         * product category model loading
                         * tr: ürün kategori modelinin yüklenmesi 
                         */
                        $this->CI->load->model('Sys_Kat_Baglanti_model');
                        $model = $this->CI->Sys_Kat_Baglanti_model;
                        /**
                         * first remove old category connections for referance
                         * tr: ilk olarak eski kategori bağlantıları silinir
                         */
                        $model->Remove(array('ref' => $referance));
                        $cobj = [];
                        /**
                         * each connection will send to database
                         * tr: her bağlantı veri tabanına gönderilir
                         */
                        foreach ($params['urun_kategori'] as $c) {
                            $cobj['sys_kat_id'] = $c;
                            $cobj['sys_kat_tip'] = 'URUN';
                            $cobj['ref'] = $referance;
                            $cobj['created_by'] = $params['_wid'];
                            $result = $model->Add($cobj);
                        }
                    }
                    /**
                     * 4- product tax table update
                     * tr: ürün vergi tablosunun  güncellenmesi
                     */
                    if (isset($params['urun_vergiler']) && count($params['urun_vergiler']) > 0) {
                        //error message
                        $errsection = "Ürün Vergi Tablosu Güncellenirken Hata ref=>" . $referance;
                        //it will load client caris model
                        $this->CI->load->model('Urun_Vergiler_model');
                        $model = $this->CI->Urun_Vergiler_model;
                        foreach ($params['urun_vergiler'] as $c) {
                            //if record is new then insert
                            if ($c['id'] == '' || intval($c['id']) == 0) {
                                unset($c['rowref']);
                                unset($c['id']);
                                unset($c['isUpdated']);
                                $c['ur_ver_durum'] = 1;
                                $c['ref'] = $referance;
                                $c['created_by'] = $params['_wid'];
                                $result = $model->Add($c);
                            } else {
                                //or update
                                if ($c['isUpdated'] == 1) {
                                    $obj = (array) $model->Get(['id' => $c['id']])['Data'][0];
                                    foreach ($c as $k => $v) {
                                        if (array_key_exists($k, $obj)) {
                                            $obj[$k] = $v;
                                        }
                                    }
                                    $obj['updated_by'] = $params['_wid'];
                                    $obj['updated_on'] = date('Y-m-d h:i:s');
                                    $result = $model->Update($obj);
                                }
                            }
                        }
                    }
                    /**
                     * 5- product units table update
                     * tr: ürün birimler tablosunun  güncellenmesi
                     */
                    if (isset($params['urun_birimler']) && count($params['urun_birimler']) > 0) {
                        //error message
                        $errsection = "Ürün Birimler Tablosu Güncellenirken Hata ref=>" . $referance;
                        //it will load units model
                        $this->CI->load->model('Urun_Birimler_model');
                        $model = $this->CI->Urun_Birimler_model;
                        foreach ($params['urun_birimler'] as $c) {
                            //if record is new then insert
                            if ($c['id'] == '' || intval($c['id']) == 0) {
                                unset($c['rowref']);
                                unset($c['id']);
                                unset($c['isUpdated']);
                                $c['ur_bir_durum'] = 1;
                                $c['ref'] = $referance;
                                $c['created_by'] = $params['_wid'];
                                $result = $model->Add($c);
                            } else {
                                //or update
                                if ($c['isUpdated'] == 1) {
                                    $obj = (array) $model->Get(['id' => $c['id']])['Data'][0];
                                    foreach ($c as $k => $v) {
                                        if (array_key_exists($k, $obj)) {
                                            $obj[$k] = $v;
                                        }
                                    }
                                    $obj['updated_by'] = $params['_wid'];
                                    $obj['updated_on'] = date('Y-m-d h:i:s');
                                    $result = $model->Update($obj);
                                }
                            }
                        }
                    }
                    /**
                     * 6- product parameters table update
                     * tr: ürün parametreler tablosunun  güncellenmesi
                     */
                    if (isset($params['urun_parametre'])) {
                        /**
                         * error message 
                         * tr :hata mesajı
                         */
                        $errsection = "Urun  Parametre Tablosu Güncelenirken Hata ref=>" . $referance;
                        /**
                         * Product parameters model loading
                         * tr :urun parametre modelinin yüklenmesi 
                         */
                        $this->CI->load->model('Urun_Parametre_model');
                        $model = $this->CI->Urun_Parametre_model;
                        if ($params['urun_parametre']['id'] != '' && $params['urun_parametre']['id'] != '0') {
                            /**
                             * get product parameter object
                             * tr: ürün objesini çek
                             */
                            $obj = (array) $model->Get(['id' => $params['urun_parametre']['id']])['Data'][0];
                            /**
                             * update product object
                             * tr: ürün objesini güncelle 
                             */
                            foreach ($params['urun_parametre'] as $k => $v) {
                                if (array_key_exists($k, $obj)) {
                                    $obj[$k] = $v;
                                }
                            }
                            $obj['updated_by'] = $params['_wid'];
                            $obj['updated_on'] = date('Y-m-d h:i:s');
                            /**
                             * send object to db for update
                             * tr: urun objesini güncelleme için gönder
                             */
                            $result = $model->Update($obj);
                        } else {
                            /**
                             * set product parameters object 
                             * tr : ürün parametre object
                             */
                            $pobj = $params['urun_parametre'];
                            $pobj['created_by'] =  $params['_wid'];
                            $pobj['ref'] = $referance;
                            /* there is not id created yet so jus unset it
                            tr : henüz id oluşturulmadı o yüzden ne gelirse modelden at
                            */
                            unset($pobj['id']);
                            /* send clean model for insert
                            tr : temiz modeli ekleme için gönder
                            */
                            $result = $model->Add($pobj);
                        }
                    }
                    /**
                     * 7- product units table update
                     * tr: ürün birimler tablosunun  güncellenmesi
                     */
                    
                    if (isset($params['urun_fiyatlar']) && count($params['urun_fiyatlar']) > 0) {
                        //error message
                        $errsection = "Ürün Fiyatlar Tablosu Güncellenirken Hata ref=>" . $referance;
                        //it will load prices model
                        $this->CI->load->model('Urun_Fiyatlar_model');
                        $model = $this->CI->Urun_Fiyatlar_model;
                        foreach ($params['urun_fiyatlar'] as $c) {
                            //if record is new then insert
                            if ($c['id'] == '' || intval($c['id']) == 0) {
                                unset($c['rowref']);
                                unset($c['id']);
                                unset($c['isUpdated']);
                                $c['ur_fi_durum'] = 1;
                                $c['ref'] = $referance;
                                $c['created_by'] = $params['_wid'];
                                $result = $model->Add($c);
                            } else {
                                //or update
                                if ($c['isUpdated'] == 1) {
                                    $obj = (array) $model->Get(['id' => $c['id']])['Data'][0];
                                    foreach ($c as $k => $v) {
                                        if (array_key_exists($k, $obj)) {
                                            $obj[$k] = $v;
                                        }
                                    }
                                    $obj['updated_by'] = $params['_wid'];
                                    $obj['updated_on'] = date('Y-m-d h:i:s');
                                    $result = $model->Update($obj);
                                }
                            }
                        }
                    }
                     /**
                     * 8- product specialities table update
                     * tr: ürün spect tablosunun  güncellenmesi
                     */
                    
                    if (isset($params['urun_spect']) && count($params['urun_spect']) > 0) {
                        //error message
                        $errsection = "Ürün Spect Tablosu Güncellenirken Hata ref=>" . $referance;
                        //it will load prices model
                        $this->CI->load->model('Urun_Spectler_model');
                        $model = $this->CI->Urun_Spectler_model;
                        foreach ($params['urun_spect'] as $c) {
                            //if record is new then insert
                            if ($c['id'] == '' || intval($c['id']) == 0) {
                                unset($c['rowref']);
                                unset($c['id']);
                                unset($c['isUpdated']);
                                $c['urun_spect_durum'] = 1;
                                $c['ref'] = $referance;
                                $c['created_by'] = $params['_wid'];
                                $result = $model->Add($c);
                            } else {
                                //or update
                                if ($c['isUpdated'] == 1) {
                                    $obj = (array) $model->Get(['id' => $c['id']])['Data'][0];
                                    foreach ($c as $k => $v) {
                                        if (array_key_exists($k, $obj)) {
                                            $obj[$k] = $v;
                                        }
                                    }
                                    $obj['updated_by'] = $params['_wid'];
                                    $obj['updated_on'] = date('Y-m-d h:i:s');
                                    $result = $model->Update($obj);
                                }
                            }
                        }
                    }
                    /**
                     * if there is not any problem
                     * tr: eğer bir sorun yok ise
                     */
                    if ($result['Result']) {
                        /**
                         * accept query
                         * tr : sorguyu kabul et
                         */
                        $this->CI->db->trans_commit();
                        _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $params['urun']['urun_kod'], 'log_tip' => 'urun', 'ref' => 'ref-' . $referance, 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_UrunGun']);
                        return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
                    } else {
                        $errsection = $this->CI->db->error();
                        $this->CI->db->trans_rollback();
                        return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => isset($errsection) ? $errsection : 'Data Yok.'];
                    }
                    break;
                case "get":
                    /**
                     * it will load product model
                     * tr : ürün modelini yükler
                     */
                    $this->CI->load->model('Urun_model');
                    $model = $this->CI->Urun_model;
                    if (isset($params['where'])) {
                        $resultp = $model->Get($params['where']);
                        $obj = [];
                        if ($resultp['Result']) {
                            $obj['urun'] = $resultp['Data'][0];
                            /**
                             * it will load language model
                             * tr : dil modelini yükler
                             */
                            $this->CI->load->model('Sys_Ceviriler_model');
                            $model = $this->CI->Sys_Ceviriler_model;
                            $result = $model->Get(['anahtar' => $resultp['Data'][0]->baslik_dil_key]);
                            if ($result['Result']) {
                                $obj['dil'] = $result['Data'];
                            }
                            /**
                             * it will load unit model
                             * tr : birim modelini yükler
                             */
                            $this->CI->load->model('Urun_Birimler_model');
                            $model = $this->CI->Urun_Birimler_model;
                            $result = $model->Get($params['where']);
                            if ($result['Result']) {
                                $obj['birim'] = $result['Data'];
                            }

                            /**
                             * it will load vat model
                             * tr : vergi modelini yükler
                             */
                            $this->CI->load->model('Urun_Vergiler_model');
                            $model = $this->CI->Urun_Vergiler_model;
                            $result = $model->Get($params['where']);
                            if ($result['Result']) {
                                $obj['vergi'] = $result['Data'];
                            }
                            /**
                             * it will load parameters model
                             * tr : parametre modelini yükler
                             */
                            $this->CI->load->model('Urun_Parametre_model');
                            $model = $this->CI->Urun_Parametre_model;
                            $result = $model->Get($params['where']);
                            if ($result['Result']) {
                                $obj['parametre'] = $result['Data'][0];
                            }
                            /**
                             * it will load prices model
                             * tr : fiyatlar modelini yükler
                             */
                            $this->CI->load->model('Urun_Fiyatlar_model');
                            $model = $this->CI->Urun_Fiyatlar_model;
                            $result = $model->Get($params['where']);
                            if ($result['Result']) {
                                $obj['fiyatlar'] = $result['Data'];
                            }
                            /**
                             * it will load specialties model
                             * tr : spectler modelini yükler
                             */
                            $this->CI->load->model('Urun_Spectler_model');
                            $model = $this->CI->Urun_Spectler_model;
                            $result = $model->Get($params['where']);
                            if ($result['Result']) {
                                $obj['spectler'] = $result['Data'];
                            }

                        }

                        return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $obj];
                    } else {
                        return ['message' => 'Referans Numarası Belirtiniz', 'result' => false];
                    }
                    break;
                case "add":
                    $baslik_dil_key = 'd_Urun_' . str_replace(".", "", microtime(true));
                    $this->CI->db->trans_begin();
                    /*1-product main record insert 
                    tr : ana ürün kaydının eklenmesi
                    */
                    /*error message 
                    tr :hata mesajı
                    */
                    $errsection = "Ürün Tablosu Eklenirken Hata ref=>" . $referance;
                    /* product model loading
                    tr :ürün modelinin yüklenmesi 
                    */
                    $this->CI->load->model('Urun_model');
                    $model = $this->CI->Urun_model;
                    /* set product object 
                    tr : ürün objesi atandı
                    */
                    $pobj = $params['urun'];
                    $pobj['created_by'] = $params['_wid'];
                    $pobj['ref'] = $referance;
                    $pobj['baslik_dil_key'] = $baslik_dil_key;
                    $pobj['urun_durum'] = $pobj['urun_durum'] == "" ? 1 : $pobj['urun_durum'];
                    /* there is not id created yet so jus unset it
                    tr : henüz id oluşturulmadı o yüzden ne gelirse modelden at
                   */
                    unset($pobj['id']);
                    /* send clean model for insert
                     tr : temiz modeli ekleme için gönder
                    */
                    $result = $model->Add($pobj);
                    /* if there is not any problem
                    tr : eğer bir problem yok ise
                    */
                    if ($result) {
                        if (isset($params['language'])) {
                            $this->CI->load->model('Sys_Ceviriler_model');
                            $dilmodel = $this->CI->Sys_Ceviriler_model;
                            /*insert language informations 
                            tr:dil girdilerinin yerleştirilmesi
                            */
                            if (count($params['language']['urun_ad']) > 0) {
                                foreach ($params['language']['urun_ad'] as $l) {
                                    $obj =  array(
                                        'anahtar' => $baslik_dil_key,
                                        'sys_cev_dil_id' => $l['langid'],
                                        'sys_cev_sayfa' => 'urunler',
                                        'sys_cev_ceviri' => $l['value']
                                    );
                                    $result = $dilmodel->Add($obj);
                                }
                            }
                        }
                        if (isset($params['urun_kategori']) && count($params['urun_kategori']) > 0) {
                            /*2-product category records insert
                            tr : ürün kategori bilgilerinin eklenmesi
                            */
                            /*error message 
                            tr :hata mesajı
                            */
                            $errsection = "Ürün Kategori Tablosuna Eklenirken Hata ref=>" . $referance;
                            /* product category model loading
                            tr :ürün kategori modelinin yüklenmesi 
                           */
                            $this->CI->load->model('Sys_Kat_Baglanti_model');
                            $model = $this->CI->Sys_Kat_Baglanti_model;
                            $cobj = [];
                            foreach ($params['urun_kategori'] as $c) {
                                $cobj['sys_kat_id'] = $c;
                                $cobj['sys_kat_tip'] = 'URUN';
                                $cobj['ref'] = $referance;
                                $cobj['created_by'] = $params['_wid'];
                                $result = $model->Add($cobj);
                            }
                        }
                        if (isset($params['urun_birimler']) && count($params['urun_birimler']) > 0) {
                            /**
                             * 3-product unit records insert
                             * tr : ürün birim bilgilerinin eklenmesi
                             */
                            /**
                             * error message 
                             * tr :hata mesajı
                             */
                            $errsection = "Ürün Birimler Tablosuna Eklenirken Hata ref=>" . $referance;
                            /**
                             * product unit model loading
                             * tr :ürün birimler modelinin yüklenmesi 
                             */
                            $this->CI->load->model('Urun_Birimler_model');
                            $model = $this->CI->Urun_Birimler_model;
                            foreach ($params['urun_birimler'] as $c) {
                                unset($c['rowref']);
                                unset($c['id']);
                                unset($c['isUpdated']);
                                $c['ur_bir_durum'] = 1;
                                $c['ref'] = $referance;
                                $c['created_by'] = $params['_wid'];
                                $result = $model->Add($c);
                            }
                        }
                        if (isset($params['urun_vergiler']) && count($params['urun_vergiler']) > 0) {
                            /**
                             * 4-product tax records insert
                             * tr : ürün vergi bilgilerinin eklenmesi
                             */
                            /**
                             * error message 
                             * tr :hata mesajı
                             */
                            $errsection = "Ürün Vergiler Tablosuna Eklenirken Hata ref=>" . $referance;
                            /**
                             * product tax model loading
                             * tr :ürün vergiler modelinin yüklenmesi 
                             */
                            $this->CI->load->model('Urun_Vergiler_model');
                            $model = $this->CI->Urun_Vergiler_model;
                            foreach ($params['urun_vergiler'] as $c) {
                                unset($c['rowref']);
                                unset($c['id']);
                                unset($c['isUpdated']);
                                $c['ur_ver_durum'] = 1;
                                $c['ref'] = $referance;
                                $c['created_by'] = $params['_wid'];
                                $result = $model->Add($c);
                            }
                        }
                        if (isset($params['urun_fiyatlar']) && count($params['urun_fiyatlar']) > 0) {
                            /**
                             * 6-product price records insert
                             * tr : ürün fiyat bilgilerinin eklenmesi
                             */
                            /**
                             * error message 
                             * tr :hata mesajı
                             */
                            $errsection = "Ürün Fiyatlar Tablosuna Eklenirken Hata ref=>" . $referance;
                            /**
                             * product price model loading
                             * tr :ürün fiyatlar modelinin yüklenmesi 
                             */
                            $this->CI->load->model('Urun_Fiyatlar_model');
                            $model = $this->CI->Urun_Fiyatlar_model;
                            foreach ($params['urun_fiyatlar'] as $c) {
                                unset($c['rowref']);
                                unset($c['id']);
                                unset($c['isUpdated']);
                                $c['ur_fi_durum'] = 1;
                                $c['ref'] = $referance;
                                $c['created_by'] = $params['_wid'];
                                $result = $model->Add($c);
                            }
                        }
                        if (isset($params['urun_spect']) && count($params['urun_spect']) > 0) {
                            /**
                             * 7-product specialities records insert
                             * tr : ürün özellik bilgilerinin eklenmesi
                             */
                            /**
                             * error message 
                             * tr :hata mesajı
                             */
                            $errsection = "Ürün Spect Tablosuna Eklenirken Hata ref=>" . $referance;
                            /**
                             * product specialities model loading
                             * tr :ürün spect modelinin yüklenmesi 
                             */
                            $this->CI->load->model('Urun_Spectler_model');
                            $model = $this->CI->Urun_Spectler_model;
                            foreach ($params['urun_spect'] as $c) {
                                unset($c['rowref']);
                                unset($c['id']);
                                unset($c['isUpdated']);
                                $c['urun_spect_durum'] = 1;
                                $c['ref'] = $referance;
                                $c['created_by'] = $params['_wid'];
                                $result = $model->Add($c);
                            }
                        }
                        if (isset($params['urun_parametre'])) {
                             /**
                             * 8-product parameters insert
                             * tr : ürün parametre bilgilerinin eklenmesi
                             */
                            /**
                             * error message 
                             * tr :hata mesajı
                             */
                            $errsection = "Ürün Parametre Tablosuna Eklenirken Hata ref=>" . $referance;
                            /**
                             * product parameters model loading
                             * tr: ürün parametre modelinin yüklenmesi 
                             */
                            $this->CI->load->model('Urun_Parametre_model');
                            $model = $this->CI->Urun_Parametre_model;
                            /**
                             * set product parameters object 
                             * tr : ürün parametre object
                             */
                            $pobj = $params['urun_parametre'];
                            $pobj['created_by'] =  $params['_wid'];
                            $pobj['ref'] = $referance;
                            /* there is not id created yet so jus unset it
                            tr : henüz id oluşturulmadı o yüzden ne gelirse modelden at
                            */
                            unset($pobj['id']);
                            /* send clean model for insert
                            tr : temiz modeli ekleme için gönder
                            */
                            $result = $model->Add($pobj);
                        }
                    }
                    /* if there is not any problem
                    tr: eğer bir sorun yok ise
                    */
                    if ($result['Result']) {
                        /* accept query
                        tr : sorguyu kabul et
                        */
                        $this->CI->db->trans_commit();
                        _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $params['urun']['urun_kod'], 'log_tip' => 'urun', 'ref' => 'ref-' . $referance, 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_UrunEklendi']);
                        return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
                    } else {
                        $errsection = $this->CI->db->error();
                        $this->CI->db->trans_rollback();
                        return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => isset($errsection) ? $errsection : 'Data Yok.'];
                    }
                    break;
            }
        } catch (Exception $th) {
            $this->CI->db->trans_rollback();
            log_message('error',  $this->CI->db->last_query());
            log_message('error',  $th);
            $errsection = $this->CI->db->error();
            return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
        }
    }
}
