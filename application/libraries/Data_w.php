<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Current class is only use for personnel transactions
 * every return in system is have same pattern
 * it will return 'result' as true or false , and  'data' key for transaction result
 * example return is '['message'=>'Hata Oluştu..','result'=>false]'
 */

class Data_w
{
    private $CI;
    /**
     * constructor for calling CI 
     */
    public function __construct()
    {
        $this->CI = &get_instance();
    }

    /**
     * this method will get clients by per page and selected order or like parameter
     */
    public function getClients($params=null){
        $startC =  microtime(true);
        try {
            //limit parameters
            $limit = "";
            //where parameters
            $where = "";
            //like parameters
            $like = "";
            //orderby parameters
            $order_by  = "";
            //if order parameters is exist
            if(isset($params['order'])){
                //write order by query
                $order_by  = " order by ". $params['order']['column'] . " " . $params['order']['by']." ";
            }
            //if limit parameter is exist
            if(isset($params['limit'])){
                //write limit query
                 $limit  = "LIMIT " . $params['limit']['limit']  . " OFFSET " . $params['limit']['start']." ";
            }
            //id where parameters is exist
            if(isset($params['where'])){
                //write where query
                $where = " where ";
                $keys = [];
                foreach($params['where'] as $s){
                   array_push($keys, $s['key']."='" . $s['value']."'");
                }
                $where.= join(" and ",$keys);
            }
            //if like parameters is exist
            if(isset($params['like'])){
                //write like query
                //if query not has 'where'
                if (strpos($where, 'where') !== true) {
                    //put 'where' to query
                    $where  = " where ";
                }
                $keys = [];
                foreach($params['like'] as $s){
                   array_push($keys, "lower(".$s['key'].") like '%" . strtolower($s['value'])."%'");
                }
                $where.= join(" or ",$keys);
            }
            //join query 
            $sql = "select cl.id as id , cl.cari_kod as kod,cl.cari_kadi as kadi
                    from cari as cl
                    left join cari_parametre as cp on cp.ref=cl.ref" ;
            $totalCount=$this->CI->db->query($sql)->num_rows();
            $sql.=$where . $like . $order_by;
            $filteredCount=$this->CI->db->query($sql)->num_rows();
            $sql.=$limit;
            //send to db
            $return = $this->CI->db->query($sql)->result_array();
            //return result
            if(!empty($return)){
                return [
                    'Data'=>array(
                        'draw'=>$params['draw'],
                        'data'=>$return,
                        'recordsFiltered'=>$filteredCount,
                        'recordsTotal'=>$totalCount
                    ),
                    'Result'=>true,
                    'Time'=>  (microtime(true)- $startC) . ' second'
                ];
            }
            return ['Result'=>false];
        } catch (\Throwable $th) {
            //if error will happen it will write error
            return ['Result'=>false,'Data'=>$th];
        }
    }
    /**
     * this method will get clients for datatable by per page and selected order or like parameter
     */
    public function getBClients($params=null){
        $startC =  microtime(true);
        try {
            //limit parameters
            $limit = '';
            //where parameters
            $where = '';
            //like parameters
            $like = '';
            //orderby parameters
            $order_by  ='';
            //join parameters
            $join='';
            //if order parameters is exist
            if(isset($params['order'])){
                //write order by query
                $order_by  = ' order by '. $params['order']['column'] . ' ' . $params['order']['by'].' ';
            }
            //if limit parameter is exist
            if(isset($params['limit'])){
                //write limit query
                 $limit  = ' LIMIT ' . $params['limit']['limit']  . ' OFFSET ' . $params['limit']['start'].' ';
            }
            //id where parameters is exist
            if(isset($params['where']) && count($params['where'])>0){
                //write where query
                $where  = ' where ';
                $keys = [];
                foreach($params['where'] as $s){
                    $s['key'] = str_replace("-", ".", $s['key']);
                    if($s['key']=='cp.cari_par_tgunu'){
                        //tur günleri array içinde aranmalı..
                        array_push($keys,"'".$s['value']."' = ANY(".$s['key'].") " );
                    }else{
                        array_push($keys, $s['key']."='" . $s['value']."'");
                    }
                }
                $where.= join(" and ",$keys);
            }
            //if like parameters is exist
            if(isset($params['like'])){
                //write like query
                //if query not has 'where'
                if (strpos($where, 'where') !== false) {
                    $where.= ' and ';
                }else{
                    //put 'where' to query
                    $where  = ' where ';
                }
                $keys = [];
                foreach($params['like'] as $s){
                    $s['key'] = str_replace("-", ".", $s['key']);
                    array_push($keys, "lower(".$s['key'].") like '%" . strtolower($s['value'])."%'");
                }
                $where.= '( '.join(" or ",$keys). ' )';
            }
            //client area query
            if(isset($params['cats'])){
                if (strpos($where, 'where') !== false) {
                    $where.= ' and ';
                }else{
                    //put 'where' to query
                    $where  = ' where ';
                }
                $join='join sys_kat_baglanti as cat on cat.ref=cl.ref';
                $list = [];
                foreach(explode(',',$params['cats']) as $c){
                    array_push($list,'cat.sys_kat_id='.$c);
                }
                $where .= ' ( '.join(' or ',$list).' ) ';
            }
            //
            //client discount area
            if(isset($params['discount'])){
                
                if(strlen($params['discount']['min'])>0){
                    if (strpos($where, 'where') !== false) {
                        $where.= ' and ';
                    }else{
                        //put 'where' to query
                        $where  = ' where ';
                    }
                    $where.= ' cf.cari_fin_indirim > '.$params['discount']['min'].' ';
                }
                if(strlen($params['discount']['max'])>0){
                    if (strpos($where, 'where') !== false) {
                        $where.= ' and ';
                    }else{
                        //put 'where' to query
                        $where  = ' where ';
                    }
                    $where.= ' cf.cari_fin_indirim < '.$params['discount']['max'].' ';
                }
            }
            //

            //join query 
            $sql = "select distinct cl.ref as ref , cl.cari_kod as kod,cl.cari_unvan as unvan,cl.cari_telofis as telofis,0 as borc,0 as alacak , 0 as bakiye,cl.id as id
                    from cari as cl
                    left join cari_finans as cf on cf.ref = cl.ref
                    left join cari_parametre as cp on cp.ref=cl.ref
                    join cari_tipi as ct on ct.id=cl.cari_tipi
                    $join
                    left join cari_durum as cd on cd.id=cl.cari_durum ";
          
            $totalCount=$this->CI->db->query($sql)->num_rows();
            $sql.=$where . $like . $order_by;
            $filteredCount=$this->CI->db->query($sql)->num_rows();
            $sql.=$limit;
            //send to db
            log_message('error', 'Like : '.$like);
            log_message('error', 'Order By : '.$order_by);
            log_message('error', $sql);
            $return = $this->CI->db->query($sql)->result_array();
            //log_message('error',json_encode($return));
            //return result
            if($return!=[] && !empty($return)){
                return [
                    'Data'=>array(
                        'draw'=>$params['draw'],
                        'data'=>$return,
                        'recordsFiltered'=>$filteredCount,
                        'recordsTotal'=>$totalCount
                    ),
                    'Result'=>true,
                    'Time'=>  (microtime(true)- $startC) . ' second'
                ];
            }else{
                return [
                    'Data'=>array(
                        'draw'=>$params['draw'],
                        'data'=>[],
                        'recordsFiltered'=>0,
                        'recordsTotal'=>$totalCount
                    ),
                    'Result'=>false,
                    'Time'=>  (microtime(true)- $startC) . ' second'
                ];
            }
           
        } catch (\Throwable $th) {
            //if error will happen it will write error
            return ['Result'=>false,'Data'=>$th];
        }
    }

    /**
     * this method will get clients by per page and selected order or like parameter
     */
    public function getLangs($params=null){
        $startC =  microtime(true);
        try {
            //limit parameters
            $limit = "";
            //where parameters
            $where = "";
            //like parameters
            $like = "";
            //orderby parameters
            $order_by  = "";
            //if order parameters is exist
            if(isset($params['order'])){
                //write order by query
                $order_by  = " order by ". $params['order']['column'] . " " . $params['order']['by']." ";
            }
            //if limit parameter is exist
            if(isset($params['limit'])){
                //write limit query
                 $limit  = "LIMIT " . $params['limit']['limit']  . " OFFSET " . $params['limit']['start']." ";
            }
            //id where parameters is exist
            if(isset($params['where'])){
                //write where query
                $where = " where ";
                $keys = [];
                foreach($params['where'] as $s){
                   array_push($keys, $s['key']."='" . $s['value']."'");
                }
                $where.= join(" and ",$keys);
            }
            //if like parameters is exist
            if(isset($params['like'])){
                //write like query
                //if query not has 'where'
                if (strpos($where, 'where') !== true) {
                    //put 'where' to query
                    $where  = " where ";
                }
                $keys = [];
                foreach($params['like'] as $s){
                   array_push($keys, "lower(".$s['key'].") like '%" . mb_strtolower($s['value'])."%'");
                }
                $where.= join(" or ",$keys);
            }
            //join query 
            $sql = "select distinct dl.anahtar as key ,dl.sys_cev_sayfa as sayfa
                    from sys_ceviriler as dl" ;
            $totalCount=$this->CI->db->query($sql)->num_rows();
            $sql.=$where . $like . $order_by;
            $filteredCount=$this->CI->db->query($sql)->num_rows();
            $sql.=$limit;
            //send to db
            $return = $this->CI->db->query($sql)->result_array();
            $data = [];
            //$languages = $this->CI->db->query("select id from sys_diller")->result_array();
            //after keys getted we need to take translate
            foreach($return as $d){
                $translates = $this->CI->db->query("select sys_cev_ceviri,sys_cev_dil_id from sys_ceviriler where anahtar='".$d['key']."'")->result_array();
                /*foreach($languages as $l){
                    $d["l_".$l['id']]=$d['key'];
                }*/
                foreach($translates as $l){
                    //take each translate for language id
                    $d["l_".$l['sys_cev_dil_id']]=$l['sys_cev_ceviri'];
                    
                }
                array_push($data,$d);
            }
            //return result
            if(!empty($data)){
                return [
                    'Data'=>array(
                        'draw'=>$params['draw'],
                        'data'=>$data,
                        'recordsFiltered'=>$filteredCount,
                        'recordsTotal'=>$totalCount
                    ),
                    'Result'=>true,
                    'Time'=>  (microtime(true)- $startC) . ' second'
                ];
            }else{
                return [
                    'Data'=>array(
                        'draw'=>$params['draw'],
                        'data'=>[],
                        'recordsFiltered'=>$filteredCount,
                        'recordsTotal'=>$totalCount
                    ),
                    'Result'=>true,
                    'Time'=>  (microtime(true)- $startC) . ' second'
                ];
            }
            return ['Result'=>false];
        } catch (\Throwable $th) {
            //if error will happen it will write error
            return ['Result'=>false,'Data'=>$th];
        }
    }

    /**
     * this method will return user logs from ranged date
     */
    public function getLogs($params=null){
        $startC =  microtime(true);
        try {
            $sql='';
            $where =' where log_kul_id!=1 and sc.sys_cev_dil_id=\''.$params['lid'].'\'';
            $join ='';
            //Person
			if(isset($params['params']['person'])){
				if($params['params']['person'] > 0){
					$where .= ' and per.id=\''.$params['params']['person'].'\'';
				}
			}
            //start date
            if(!isset($params['params']['d1'])){
                $where .= ' and sl.created_on > \''.date('Y-m-d').'\'';
            }else{
                $where .= ' and sl.created_on >= \''.$params['params']['d1'].'\'';
            }
            //end date
            if(isset($params['params']['d2'])){
                $where .= ' and sl.created_on <= \''.$params['params']['d2'].'\'';
            }

            if(isset($params['params']['modul'])){
                $where .= ' and  sl.log_tip =\''.$params['params']['modul'].'\'';
            }
            $sql = 'select sl.id as id,sl.log_kul_id as yapan,sl.log_aciklama as baslik,sl.log_tip as tip,sl.ref as ref,sl.created_on as tarih,sc.sys_cev_ceviri as aksiyon,kl.ref as recordref,kl.kul_tip as tip from sys_log as sl
            join sys_ceviriler as sc on sc.anahtar = sl.log_aksiyon
            join kullanicilar as kl on kl.id=sl.log_kul_id
            join personel as per on kl.ref=per.ref
            '.$where.' order by sl.created_on desc';
//			log_message('error',$sql);
//			log_message('error',$params['params']['person']);

			$data = $this->CI->db->query($sql)->result_array();
            foreach($data as $i => $d){
                switch($d['tip']){
                    case 'P':
                        $return=$this->CI->db->select('per_ad,per_img')
                        ->where(array('ref'=>$d['recordref']))
                        ->get('personel')->result_array();
                        if(count($return)>0){
                            $data[$i]['yapan']=$return[0]['per_ad'];
                            $data[$i]['img']= $return[0]['per_img'];
                        }
                        break;
                    default:
                        break;    
                }
            }
            //return result
			$status = true;
			if($data == null){
				$status = false;
			}
            if(!empty($data)){
                return array(
                    'Data'=>$data,
                    'Result'=>true,
                    'Time'=>  (microtime(true)- $startC) . ' second'
				);
            }

            return array('Data' => array(), 'Result' => $status, 'Time' => (microtime(true) - $startC) . ' second');
        } catch (\Throwable $th) {
            //if error will happen it will write error
            return array('Result'=>false,'Data'=>$th);
        }
    }

    /**
     * this method will get clients for datatable by per page and selected order or like parameter
     */
    public function getBPersonels($params=null){
        $startC =  microtime(true);
        try {
            $join='';
            //limit parameters
            $limit = "";
            //where parameters
            $where = "";
            //like parameters
            $like = "";
            //orderby parameters
            $order_by  = "";
            //if order parameters is exist
            if(isset($params['order'])){
                //write order by query
                $order_by  = " order by ". $params['order']['column'] . " " . $params['order']['by']." ";
            }
            //if limit parameter is exist
            if(isset($params['limit'])){
                //write limit query
                 $limit  = "LIMIT " . $params['limit']['limit']  . " OFFSET " . $params['limit']['start']." ";
            }
            //client category query
            if(isset($params['cats']) && strlen(trim($params['cats']))>0){
                if (strpos($where, 'where') !== false) {
                    $where.= ' and ';
                }else{
                    //put 'where' to query
                    $where  = ' where ';
                }
                $join='join sys_kat_baglanti as cat on cat.ref=pr.ref';
                $list = [];
                foreach(explode(',',$params['cats']) as $c){
                    array_push($list,'cat.sys_kat_id='.$c);
                }
                $where .= ' ( '.join(' or ',$list).' ) ';
            }
            //
            //id where parameters is exist
            if(isset($params['where']) && count($params['where'])>0){
                //write where query
                $keys = [];
                foreach($params['where'] as $s){
                    if(strlen(trim($s['value']))>0){
                        $s['key'] = str_replace("-", ".", $s['key']);
                        array_push($keys, $s['key']."='" . $s['value']."'");
                    }
                }
                if(count($keys)>0){
                    $where = " where ";
                    $where.= join(" and ",$keys);
                }
            }
            //if like parameters is exist
            if(isset($params['like'])){
                //write like query
                $keys = [];
                foreach($params['like'] as $s){
                    if(strlen(trim($s['value']))>0){
                        $s['key'] = str_replace("-", ".", $s['key']);
                        if($s['key']!='pr.per_giris'){
                            array_push($keys, "lower(".$s['key'].") like '%" . strtolower($s['value'])."%'");
                        }else{
                            array_push($keys, "to_char(".$s['key'].",'YYYY-mm-dd') like '%" . strtolower($s['value'])."%'");
                        }
                    }
                }
                if(count($keys)>0){
                    //if query not has 'where'
                    if (strpos($where, 'where') !== false) {
                        $where.= ' and ';
                    }else{
                        //put 'where' to query
                        $where  = " where ";
                    }
                    $where.= '( '.join(" or ",$keys). ' )';
                }
            }
            if(isset($params['range'])){
                //write range query
                //tr : range sorgusunun yazılması
                $keys = [];
                foreach($params['range'] as $k => $v){
                    if(strlen(trim($v))>0){
                        $k = str_replace("-", ".", $k);
                        $v = explode(' - ',$v);
                        array_push($keys, $k.' >= \''.$v[0].'\' and '.$k.' <= \''.$v[1].'\' ');
                    }
                }
                if(count($keys)>0){
                    //if query not has 'where'
                    //tr : eğer sorguda 'where' yoksa
                    if (strpos($where, 'where') !== false) {
                        $where.= ' and ';
                    }else{
                        //put 'where' to query
                        $where  = " where ";
                    }
                    $where.= '( '.join(" and ",$keys). ' )';
                }
            }
           

            //join query 
            $sql = "select distinct pr.ref as ref , pr.per_ad as ad,pr.per_img as img,pp.per_poz_baslik as pozisyon,pr.per_giris as giris,0 as like ,0 as dislike,0 as bgorev,0 as tgorev,0 as dgorev,0 as kizin,0 as tizin,0 as kalizin,pr.id as id
                    from personel as pr
                    left join per_pozisyon as pp on pp.id=pr.per_pozisyon
                    $join ";
            
            $totalCount=$this->CI->db->query($sql)->num_rows();
            $sql.=$where . $like . $order_by;
            $filteredCount=$this->CI->db->query($sql)->num_rows();
            $sql.=$limit;
            log_message('error',$sql);
            //send to db
            //log_message('error', $sql);
            $return = $this->CI->db->query($sql)->result_array();
            //log_message('error',json_encode($return));
            //return result
            if($return!=[] && !empty($return)){
                return [
                    'Data'=>array(
                        'draw'=>$params['draw'],
                        'data'=>$return,
                        'recordsFiltered'=>$filteredCount,
                        'recordsTotal'=>$totalCount
                    ),
                    'Result'=>true,
                    'Time'=>  (microtime(true)- $startC) . ' second'
                ];
            }else{
                return [
                    'Data'=>array(
                        'draw'=>$params['draw'],
                        'data'=>[],
                        'recordsFiltered'=>0,
                        'recordsTotal'=>$totalCount
                    ),
                    'Result'=>false,
                    'Time'=>  (microtime(true)- $startC) . ' second'
                ];
            }
           
        } catch (\Throwable $th) {
            //if error will happen it will write error
            return ['Result'=>false,'Data'=>$th];
        }
    }
    /**
     * this method will get clients by per page and selected order or like parameter for component
     */
    public function getPersonels($params=null){
        $startC =  microtime(true);
        try {
            //limit parameters
            $limit = "";
            //where parameters
            $where = "";
            //like parameters
            $like = "";
            //orderby parameters
            $order_by  = "";
            //if order parameters is exist
            if(isset($params['order'])){
                //write order by query
                $order_by  = " order by ". $params['order']['column'] . " " . $params['order']['by']." ";
            }
            //if limit parameter is exist
            if(isset($params['limit'])){
                //write limit query
                 $limit  = "LIMIT " . $params['limit']['limit']  . " OFFSET " . $params['limit']['start']." ";
            }
            //id where parameters is exist
            if(isset($params['where'])){
                //write where query
                $where = " where ";
                $keys = [];
                foreach($params['where'] as $s){
                   array_push($keys, $s['key']."='" . $s['value']."'");
                }
                $where.= join(" and ",$keys);
            }
            //if like parameters is exist
            if(isset($params['like'])){
                //write like query
                //if query not has 'where'
                if (strpos($where, 'where') !== true) {
                    //put 'where' to query
                    $where  = " where ";
                }
                $keys = [];
                foreach($params['like'] as $s){
                   array_push($keys, "lower(".$s['key'].") like '%" . strtolower($s['value'])."%'");
                }
                $where.= join(" or ",$keys);
            }
            //join query 
            $sql = "select pr.ref as ref ,pr.id as id, pr.per_ad as text,pr.per_img as img,pp.per_poz_baslik as pozisyon
                    from personel as pr
                    left join per_pozisyon as pp on pp.id=pr.per_pozisyon";
            log_message('error',$sql);       
            $totalCount=$this->CI->db->query($sql)->num_rows();
            $sql.=$where . $like . $order_by;
            $filteredCount=$this->CI->db->query($sql)->num_rows();
            $sql.=$limit;
            //send to db
            $return = $this->CI->db->query($sql)->result_array();
            //return result
            if(!empty($return)){
                return [
                    'Data'=>array(
                        'draw'=>$params['draw'],
                        'data'=>$return,
                        'recordsFiltered'=>$filteredCount,
                        'recordsTotal'=>$totalCount
                    ),
                    'Result'=>true,
                    'Time'=>  (microtime(true)- $startC) . ' second'
                ];
            }
            return ['Result'=>false];
        } catch (\Throwable $th) {
            //if error will happen it will write error
            return ['Result'=>false,'Data'=>$th];
        }
    }
    /**
     * this method will get products by per page and selected order or like parameter for component
     */
    public function getProducts($params=null){
        $startC =  microtime(true);
        try {
            //limit parameters
            $limit = "";
            //where parameters
            $where = "";
            //like parameters
            $like = "";
            //orderby parameters
            $order_by  = "";
            //if order parameters is exist
            if(isset($params['order'])){
                //write order by query
                $order_by  = " order by ". $params['order']['column'] . " " . $params['order']['by']." ";
            }
            //if limit parameter is exist
            if(isset($params['limit'])){
                //write limit query
                 $limit  = "LIMIT " . $params['limit']['limit']  . " OFFSET " . $params['limit']['start']." ";
            }
            //id where parameters is exist
            if(isset($params['where'])){
                //write where query
                $where = " where ";
                $keys = [];
                foreach($params['where'] as $s){
                   array_push($keys, $s['key']."='" . $s['value']."'");
                }
                $where.= join(" and ",$keys);
            }
            //if like parameters is exist
            if(isset($params['like'])){
                //write like query
                //if query not has 'where'
                if (strpos($where, 'where') !== true) {
                    //put 'where' to query
                    $where  = " where ";
                }
                $keys = [];
                foreach($params['like'] as $s){
                   array_push($keys, "lower(".$s['key'].") like '%" . strtolower($s['value'])."%'");
                }
                $where.= join(" or ",$keys);
            }
            //join query 
            $sql = "select ur.urun_Ad as ad,ur.urun_kod as kod,ur.id as id
                    from urun as ur";
            log_message('error',$sql);       
            $totalCount=$this->CI->db->query($sql)->num_rows();
            $sql.=$where . $like . $order_by;
            $filteredCount=$this->CI->db->query($sql)->num_rows();
            $sql.=$limit;
            //send to db
            $return = $this->CI->db->query($sql)->result_array();
            //return result
            if(!empty($return)){
                return [
                    'Data'=>array(
                        'draw'=>$params['draw'],
                        'data'=>$return,
                        'recordsFiltered'=>$filteredCount,
                        'recordsTotal'=>$totalCount
                    ),
                    'Result'=>true,
                    'Time'=>  (microtime(true)- $startC) . ' second'
                ];
            }
            return ['Result'=>false];
        } catch (\Throwable $th) {
            //if error will happen it will write error
            return ['Result'=>false,'Data'=>$th];
        }
    }
    /**
     * this method will get clients for datatable by per page and selected order or like parameter
     */
    public function getBProducts($params=null){
		$startC =  microtime(true);
        try {
            $join='';
            //limit parameters
            $limit = "";
            //where parameters
            $where = "";
            //like parameters
            $like = "";
            //orderby parameters
            $order_by  = "";
            //if order parameters is exist
            if(isset($params['order'])){
                //write order by query
                $order_by  = " order by ". $params['order']['column'] . " " . $params['order']['by']." ";
            }
            //if limit parameter is exist
            if(isset($params['limit'])){
                //write limit query
                 $limit  = "LIMIT " . $params['limit']['limit']  . " OFFSET " . $params['limit']['start']." ";
            }
            //id where parameters is exist
            if(isset($params['where']) && count($params['where'])>0){
                //write where query
                $keys = [];
                foreach($params['where'] as $s){
                    if(strlen(trim($s['value']))>0){
                        $s['key'] = str_replace("-", ".", $s['key']);
                        array_push($keys, $s['key']."='" . $s['value']."'");
                    }
                }
                if(count($keys)>0){
                    $where = " where ";
                    $where.= join(" and ",$keys);
                }
            }
            //if like parameters is exist
            if(isset($params['like'])){
                //write like query
                $keys = [];
                foreach($params['like'] as $s){
                    if(strlen(trim($s['value']))>0){
                        $s['key'] = str_replace("-", ".", $s['key']);
                        array_push($keys, "lower(".$s['key'].") like '%" . strtolower($s['value'])."%'");
                    }
                }
                if(count($keys)>0){
                    //if query not has 'where'
                    if (strpos($where, 'where') !== false) {
                        $where.= ' and ';
                    }else{
                        //put 'where' to query
                        $where  = " where ";
                    }
                    $where.= '( '.join(" or ",$keys). ' )';
                }
            }
            if(isset($params['range'])){
                //write range query
                //tr : range sorgusunun yazılması
                $keys = [];
                foreach($params['range'] as $k => $v){
                    if(strlen(trim($v))>0){
                        $k = str_replace("-", ".", $k);
                        $v = explode(' - ',$v);
                        array_push($keys, $k.' >= \''.$v[0].'\' and '.$k.' <= \''.$v[1].'\' ');
                    }
                }
                if(count($keys)>0){
                    //if query not has 'where'
                    //tr : eğer sorguda 'where' yoksa
                    if (strpos($where, 'where') !== false) {
                        $where.= ' and ';
                    }else{
                        //put 'where' to query
                        $where  = " where ";
                    }
                    $where.= '( '.join(" and ",$keys). ' )';
                }
            }
             //product category query
             if(isset($params['cats']) && strlen(trim($params['cats']))>0){
                if (strpos($where, 'where') !== false) {
                    $where.= ' and ';
                }else{
                    //put 'where' to query
                    $where  = ' where ';
                }
                $join='join sys_kat_baglanti as cat on cat.ref=pr.ref';
                $list = [];
                foreach(explode(',',$params['cats']) as $c){
                    array_push($list,'cat.sys_kat_id='.$c);
                }
                $where .= ' ( '.join(' or ',$list).' ) ';
            }
            //

            //join query 
            $sql = "select distinct pr.id as id,pr.urun_ad as ad ,pr.urun_kod as kod,pr.urun_img as img,pr.ref as ref,pr.urun_durum as durum from urun as pr 
            $join";
            
            $totalCount=$this->CI->db->query($sql)->num_rows();
            $sql.=$where . $like . $order_by;
            $filteredCount=$this->CI->db->query($sql)->num_rows();
            $sql.=$limit;
            //send to db
            //log_message('error', $sql);
            $return = $this->CI->db->query($sql)->result_array();
            // log_message('error',json_encode($return));
            $returndata = array();
//				log_message('error',print_r(GetUrun()));

            foreach($return as $value){
				// log_message('error',json_encode($value));
                $ulkeid = array();
                //Ülkelere göre fiyatları almak için ülke idlerini ürünün içine gömüyoruz.
                $insql = "select ur_fi_ulid,count(ur_fi_fiyat) from urun_fiyatlar where ref = '".$value['ref']."' group by 1";
                $inreturn = $this->CI->db->query($insql)->result_array();
                foreach($inreturn as $val){
                    if($val['ur_fi_ulid'] != null || $val['count'] != null){
                        array_push($ulkeid, array(
                            'ulid' => $val['ur_fi_ulid'],
                            'count' => $val['count'],
                        ));
                    }else {
                        array_push($ulkeid, array(
                            'ulid' => "0",
                            'count' => "0",
                        ));
                    }
                   
                }


//				log_message('error',$value['ref']);


				array_push($returndata, array(
                    'id' => $value['id'],
                    'ad' => $value['ad'],
                    'kod' => $value['kod'],
                    'img' => $value['img'],
                    'ref' => $value['ref'],
                    'durum' => $value['durum'],
                    'ulkeler' => $ulkeid,
					'grup' => GetUrun($value['ref']),
				));
             }

			//#region ürüne ait sorumlular
				// log_message('error',json_encode($returndata));
			//#endregion

            //return result
            if($return!= array() && !empty($return)){
                return array(
                    'Data'=>array(
                        'draw'=>$params['draw'],
                        'data'=>$returndata,
                        // 'data'=>$return,
                        'recordsFiltered'=>$filteredCount,
                        'recordsTotal'=>$totalCount
                    ),
                    'Result'=>true,
                    'Time'=>  (microtime(true)- $startC) . ' second'
				);
            }else{
                return array(
                    'Data'=>array(
                        'draw'=>$params['draw'],
                        'data'=> array(),
                        'recordsFiltered'=>0,
                        'recordsTotal'=>$totalCount
                    ),
                    'Result'=>false,
                    'Time'=>  (microtime(true)- $startC) . ' second'
				);
            }
           
        } catch (\Throwable $th) {
            //if error will happen it will write error
            return array('Result'=>false,'Data'=>$th);
        }
    }
    /**
     * this method will get clients for datatable by per page and selected order or like parameter
     */
    public function getBGroups($params=null){
        $startC =  microtime(true);
        try {
            //limit parameters
            $limit = "";
            //where parameters
            $where = "";
            //like parameters
            $like = "";
            //orderby parameters
            $order_by  = "";
            //if order parameters is exist
            if(isset($params['order'])){
                //write order by query
                $order_by  = " order by ". $params['order']['column'] . " " . $params['order']['by']." ";
            }
            //if limit parameter is exist
            if(isset($params['limit'])){
                //write limit query
                 $limit  = "LIMIT " . $params['limit']['limit']  . " OFFSET " . $params['limit']['start']." ";
            }
            //id where parameters is exist
            if(isset($params['where']) && count($params['where'])>0){
                //write where query
                $keys = [];
                foreach($params['where'] as $s){
                    if(strlen(trim($s['value']))>0){
                        $s['key'] = str_replace("-", ".", $s['key']);
                        array_push($keys, $s['key']."='" . $s['value']."'");
                    }
                }
                if(count($keys)>0){
                    $where = " where ";
                    $where.= join(" and ",$keys);
                }
            }
            //if like parameters is exist
            if(isset($params['like'])){
                //write like query
                $keys = [];
                foreach($params['like'] as $s){
                    if(strlen(trim($s['value']))>0){
                        $s['key'] = str_replace("-", ".", $s['key']);
                        array_push($keys, "lower(".$s['key'].") like '%" . strtolower($s['value'])."%'");
                    }
                }
                if(count($keys)>0){
                    //if query not has 'where'
                    if (strpos($where, 'where') !== false) {
                        $where.= ' and ';
                    }else{
                        //put 'where' to query
                        $where  = " where ";
                    }
                    $where.= '( '.join(" or ",$keys). ' )';
                }
            }
            if(isset($params['range'])){
                //write range query
                //tr : range sorgusunun yazılması
                $keys = [];
                foreach($params['range'] as $k => $v){
                    if(strlen(trim($v))>0){
                        $k = str_replace("-", ".", $k);
                        $v = explode(' - ',$v);
                        array_push($keys, $k.' >= \''.$v[0].'\' and '.$k.' <= \''.$v[1].'\' ');
                    }
                }
                if(count($keys)>0){
                    //if query not has 'where'
                    //tr : eğer sorguda 'where' yoksa
                    if (strpos($where, 'where') !== false) {
                        $where.= ' and ';
                    }else{
                        //put 'where' to query
                        $where  = " where ";
                    }
                    $where.= '( '.join(" and ",$keys). ' )';
                }
            }
           

            //join query 
            $sql = "select gr.ref as ref,gr.gr_baslik as baslik,gt.gr_tip_baslik as tip , pr.per_ad as ad,gr.id as id from sys_grup as gr
                    left join sys_grup_tip as gt on gt.id=gr.gr_tip
                    left join personel as pr on pr.id=gr.gr_sorumlu";
            
            $totalCount=$this->CI->db->query($sql)->num_rows();
            $sql.=$where . $like . $order_by;
            $filteredCount=$this->CI->db->query($sql)->num_rows();
            $sql.=$limit;
            //send to db
            //log_message('error', $sql);
            $return = $this->CI->db->query($sql)->result_array();
            //log_message('error',json_encode($return));
            //return result
            if($return!=[] && !empty($return)){
                return [
                    'Data'=>array(
                        'draw'=>$params['draw'],
                        'data'=>$return,
                        'recordsFiltered'=>$filteredCount,
                        'recordsTotal'=>$totalCount
                    ),
                    'Result'=>true,
                    'Time'=>  (microtime(true)- $startC) . ' second'
                ];
            }else{
                return [
                    'Data'=>array(
                        'draw'=>$params['draw'],
                        'data'=>[],
                        'recordsFiltered'=>0,
                        'recordsTotal'=>$totalCount
                    ),
                    'Result'=>false,
                    'Time'=>  (microtime(true)- $startC) . ' second'
                ];
            }
           
        } catch (\Throwable $th) {
            //if error will happen it will write error
            return ['Result'=>false,'Data'=>$th];
        }
    }
        /**
     * this method will return money formats per given country.If you dont given the ulid this paramter gives you all of the Country money formats.
     */
    public function getCountryMoney($params=null){
        $startC =  microtime(true);
        try {
            $ref='';
            $ulid='';
            $langkey='1';

            if(isset($params['ref'])){
                $ref="'".$params['ref']."'";
            }
            if(isset($params['ulid']) && $params['ulid'] > 0 ){
                $ulid= "and ur_fi_ulid=".$params['ulid'];
            }
            if(isset($params['lang'])){
                $langkey=$params['lang'];
            }
            $fiyattipleri = [];
            $tipsql = 'SELECT sf.id,cv.sys_cev_ceviri FROM public.sys_fiyatlar as sf
            join sys_ceviriler as cv on cv.anahtar=sf.baslik_dil_key
            where sf."sys_fi_durum"=1 and cv.sys_cev_dil_id='.$langkey;
            $tipdata = $this->CI->db->query($tipsql)->result_array();
            foreach($tipdata as $i => $d){
                $fiyattipleri[$d['id']]['id'] = $d['id'];
                $fiyattipleri[$d['id']]['baslik'] = $d['sys_cev_ceviri'];
            }

            $fiyatlar = [];
            $sql = 'SELECT uf.ref,uf.ur_fi_id,uf.ur_fi_fiyat,uf.ur_fi_ulid,uf.ur_fi_vat,pb.sys_pbir_icon FROM urun_fiyatlar as uf join sys_pbirim as pb on pb.id=uf.ur_fi_pbirim where ref='.$ref.' '.$ulid;
//             log_message('error',json_encode($sql ));

            $data = $this->CI->db->query($sql)->result_array();
            foreach($data as $i => $d){
                $baslik = "";
                if(isset($fiyattipleri[$d['ur_fi_id']])){
                    $baslik = $fiyattipleri[$d['ur_fi_id']]['baslik'];
                }
				if(isset($params['ulid']) && $params['ulid'] > 0 ){
					array_push($fiyatlar,array(
						"id"=>$d['ur_fi_id'],
						"fiyat"=>$d['ur_fi_fiyat'],
						"vat"=>$d['ur_fi_vat'],
						"birim"=>$d['sys_pbir_icon'],
						"baslik"=>$baslik,
						"ulid"=>$d['ur_fi_ulid'],
					));
				}else {
					if(isset($fiyatlar[$d['ur_fi_ulid']])){
						$name = "false";
						switch ($d['ur_fi_id']) {
							case 7:
								$name = "maliyet";
								break;
							case 8:
								$name = "liste";
								break;
							case 19:
								$name = "bayi";
								break;
							case 17:
								$name = "zincir";
								break;
							case 18:
								$name = "toptanci";
								break;
						}

						$fiyatlar[$d['ur_fi_ulid']][$name]=$d['ur_fi_fiyat'];
					}
					$fiyatlar[$d['ur_fi_ulid']]['id']=$d['ur_fi_id'];
//					$fiyatlar[$d['ur_fi_ulid']]['fiyat']=$d['ur_fi_fiyat'];
//					$fiyatlar[$d['ur_fi_ulid']]['vat']=$d['ur_fi_vat'];
					$fiyatlar[$d['ur_fi_ulid']]['birim']=$d['sys_pbir_icon'];
//					$fiyatlar[$d['ur_fi_ulid']]['baslik']=$baslik;
					$fiyatlar[$d['ur_fi_ulid']]['ulid']=$d['ur_fi_ulid'];
				}
            }
            //return result
            if(!empty($fiyatlar)){
                return [
                    'Data'=>$fiyatlar,
                    'Result'=>true,
                    'Time'=>  (microtime(true)- $startC) . ' second'
                ];
            }
            return [
                'Data'=>[],
                'Result'=>true,
                'Time'=>  (microtime(true)- $startC) . ' second'
            ];
        } catch (\Throwable $th) {
            //if error will happen it will write error
            return ['Result'=>false,'Data'=>$th];
        }
    }
    
}
