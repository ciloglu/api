<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Current class is only use for personnel transactions
 * every return in system is have same pattern
 * it will return 'result' as true or false , and  'data' key for transaction result
 * example return is '['message'=>'Hata Oluştu..','result'=>false]'
 */

class Mpersonel_w
{
    private $CI;
    //user hash key
    private $Hash;
    /**
     * constructor for calling CI 
     */
    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->model('Kullanicilar_model');
        $this->CI->load->model('Kul_Key_model');
    }

    /**
     * this method will extend expire time of key
     * if user will do a transaction,this method will extend api token's timeout
     *
     */
    public function extendkey($params)
    {
        if (isset($params['_ip']) && isset($params['_ua'])) {
            $this->hash = md5($params['_ip'] . $params['_ua']);
            //if token is exists
            $Token = $this->CI->Kul_Key_model->Get(array('kul_key_refid' => $params['_wid'], 'kul_key_hash' => $this->hash));
            if ($Token['Result']) {
                $obj =  array(
                    'id' => $Token['Data'][0]->id,
                    'kul_key_expire' => date("Y-m-d H:i", strtotime('+30 minutes', strtotime(date("Y-m-d H:i")))),
                    'kul_key_status' => '1',
                );
                $Token = $this->CI->Kul_Key_model->Update($obj);
                //if transaction is successed return info
                if ($Token['Result']) {
                    //send updated token
                    return ['message' => 'Kullanıcı Süresi Uzatıldı..', 'result' => true];
                } else {
                    return ['message' => 'Hata Oluştu..', 'result' => false, 'Error' => $Token['Data']];
                }
            } else {
                return ['message' => 'Giriş Yapınız...', 'result' => false, 'Error' => $Token['Data']];
            }
        } else {
            return ['message' => 'Kimliğiniz Belirlenememektedir.', 'result' => false];
        }
    }

    /**
     * this method will do login transaction
     * if login is success it will return api token and user referance code for user
     */
    public function login($params)
    {
        try {
            $this->hash = md5($params['_ip'] . $params['_ua']);
            /**
             * it will check if parameters all sended
             * tr : gerekli parametrelerin gönderilip gönderilmediğini kontrol eder
             */
            if (isset($params['Username']) && isset($params['Password']) && isset($params['_ip']) && isset($params['_ua']) && isset($params['Location'])) {
                /**
                 * if all parameters is sended it will ask to db for user info
                 * tr : eğer tüm parametreler gönderilmiş ise kullanıcıyı veri tabanından çeker
                 */
                $User = $this->CI->Kullanicilar_model->Get(['kul_kadi' => $params['Username'], 'kul_parola' => $params['Password']]);

                /*if user is exist it will create login token informations
                tr : eğer kullanıcı mevcut ise token işlemlerine geçer
                */
                if ($User['Result']) {
                    log_message('error', 'kullanıcı tamam');
                    /* in this step it will collect user data  
                    tr :bu aşamada kullanıcının veri bilgilerini var ise toplar  
                    */
                    $Data = [];
                    if (strlen(trim($User['Data'][0]->kul_tip)) > 0) {
                        $Data['Type'] = $User['Data'][0]->kul_tip;
                        switch ($User['Data'][0]->kul_tip) {
                            case 'P':
                                $this->CI->load->model('Personel_model');
                                $worker = $this->CI->Personel_model->Get(['ref' => $User['Data'][0]->ref]);
                                $Data['Name'] = $worker['Data'][0]->per_ad;
                                $Data['Img'] = $worker['Data'][0]->per_img;
                                $Data['RefId'] = $worker['Data'][0]->ref;
                                if($worker['Data'][0]->ref){
                                    $sirketler = [];
                                    $workerref = $worker['Data'][0]->ref;
                                    $sql = "SELECT kb.sys_kat_id,kat.sys_kat_baslik FROM personel as per
                                    join sys_kat_baglanti as kb on kb.ref=per.ref 
                                    join sys_kategoriler as kat on kat.id=kb.sys_kat_id
                                    where kb.ref='$workerref' and kat.sys_kat_ust=0";
                                    $data = $this->CI->db->query($sql)->result_array();
                                    foreach($data as $i => $d){
                                        array_push($sirketler,array(
                                            "id"=>$d['sys_kat_id'],
                                            "sirket"=>$d['sys_kat_baslik'],
                                        ));
                                    }  
                                    $Data['Company'] = $sirketler;
                                }
                                log_message('error', 'çalışan tamam');
                                break;
                            case 'C':
                                $this->CI->load->model('Cari_model');
                                $client = $this->CI->Cari_model->Get(['ref' => $User['Data'][0]->ref]);
                                $Data['Name'] = $client['Data'][0]->cari_ladi;
                                $Data['Img'] = $worker['Data'][0]->cari_img;
                                log_message('error', 'cari tamam');
                                break;
                        }
                    }
                    /*it will check if user already logined from same hash and has key 
                    tr: daha önceden keyi varmı diye kontrol eder
                    */
                    $Token = $this->CI->Kul_Key_model->Get(array('kul_key_refid' => $User['Data'][0]->id, 'kul_key_hash' => $this->hash));

                    if ($Token['Result']) {
                        /*if user is already logined from same hash code and token is not expired it will return same token
                        tr:eğer kullanıcının keyi var ise ve süresi geçmemiş ise aynı keyi döndürür
                        */
                        if (date("Y-m-d H:i") > $Token['Data'][0]->kul_key_expire) {
                            /*if token is old update with new token and send to user
                            tr: token eski ise yenisi ile güncelle
                            */
                            $obj =  array(
                                'id' => $Token['Data'][0]->id,
                                'kul_key_token' => _crypt($User['Data'][0]->id . $this->hash),
                                'kul_key_expire' => date("Y-m-d H:i", strtotime('+30 minutes', strtotime(date("Y-m-d H:i")))),
                                'kul_key_status' => '1',
                            );
                            $Token = $this->CI->Kul_Key_model->Update($obj);

                            /*if token update is success
                            tr: eğer key güncellenir ise yeni keyi gönder
                            */
                            if ($Token['Result']) {
                                _log(['log_kul_id' => $User['Data'][0]->id, 'log_ip' => $params['_ip'], 'log_aciklama' => $User['Data'][0]->id, 'log_tip' => 'system', 'ref' => '', 'created_by' => $User['Data'][0]->id, 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_GirisYapildi']);
                                return ['message' => 'Kullanıcı Girişi Sağlandı..', 'result' => true, 'data' => ['token' => $obj['kul_key_token'], 'workerreferance' => $User['Data'][0]->id], 'info' => $Data];
                            } else {
                                return ['message' => 'Hata Oluştu..', 'result' => false, 'Error' => $Token['Data']];
                            }
                        } else {
                            _log(['log_kul_id' => $User['Data'][0]->id, 'log_ip' => $params['_ip'], 'log_aciklama' => $User['Data'][0]->id, 'log_tip' => 'system', 'ref' => '', 'created_by' => $User['Data'][0]->id, 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_GirisYapildi']);
                            /*return current token
                            tr: key hala geçerli o yüzden eskisini gönder
                            */
                            return [
                                'message' => 'Kullanıcı Girişi Sağlandı..',
                                'result' => true,
                                'data' => [
                                    'token' => $Token['Data'][0]->kul_key_token,
                                    'workerreferance' => $User['Data'][0]->id
                                ],
                                'info' => $Data
                            ];
                        }
                    } else {
                        /*it will create new token for current hash of user
                        tr:yeni key oluştur ve gönder
                        */
                        $obj =  array(
                            'kul_key_refid' => $User['Data'][0]->id,
                            'kul_key_token' => _crypt($User['Data'][0]->id . $this->hash),
                            'kul_key_expire' => date("Y-m-d H:i", strtotime('+30 minutes', strtotime(date("Y-m-d H:i")))),
                            'kul_key_status' => '1',
                            'kul_key_hash' => $this->hash
                        );

                        /*it will add token to db and return true or false
                        tr : yeni keyi veri tabanına ekle
                        */
                        $Token = $this->CI->Kul_Key_model->Add($obj);

                        /*if token is created it will return to user his token and worker referance code
                        tr : herşey tamam ise bilgileri gönder
                        */
                        if ($Token['Result']) {
                            _log(['log_kul_id' => $User['Data'][0]->id, 'log_ip' => $params['_ip'], 'log_aciklama' => $User['Data'][0]->id, 'log_tip' => 'system', 'ref' => '', 'created_by' => $User['Data'][0]->id, 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_GirisYapildi']);
                            return ['message' => 'Kullanıcı Girişi Sağlandı..', 'result' => true, 'data' => ['token' => $obj['kul_key_token'], 'workerreferance' => $User['Data'][0]->id], 'info' => $Data];
                        } else {
                            return ['message' => 'Hata Oluştu..', 'result' => false, 'Error' => $Token['Data']];
                        }
                    }
                } else {
                    return ['message' => 'Kullanıcı Bulunamadı..', 'result' => false, 'Error' => $User['Data']];
                }
            } else {
                return ['message' => 'Gerekli veriler eksik gönderildi..', 'result' => false];
            }
        } catch (Exception $th) {
            log_message('error',  $this->CI->db->last_query());
            $errsection = $this->CI->db->error();
            return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
        }
    }

    /**
     * this method will check users key is valid or not
     * tr: bu metod kullanıcının keyinin geçerliliğini sorgular
     */
    public function checkkey($params)
    {
        /**
         * if necessary parameters is sended
         * tr:gerekli parametreler gönderilmiş ise
         */
        if (isset($params['_ip']) && isset($params['_ua'])) {
            $this->hash = md5($params['_ip'] . $params['_ua']);
            /**
             * check yoken
             */
            $Token = $this->CI->Kul_Key_model->Get(array('kul_key_refid' => $params['_wid'], 'kul_key_hash' => $this->hash));
            if ($Token['Result']) {
                return ['message' => 'Kullanıcı Keyi Geçerli..', 'result' => true];
            } else {
                return ['message' => 'Giriş Yapınız...', 'result' => false, 'Error' => $Token['Data']];
            }
        }
    }


    /**
     * this method will do forgot password transactions
     * if user mail is exist in system this method will push mail to user mail adress about user information
     */
    public function forgot($params)
    {
        //it will check if parameters all sended
        if (isset($params['Email'])) {

            //it will find worker info from email
            $User = $this->CI->Kullanicilar_model->Get(['kul_email' => $params['Email']]);

            if ($User['Result']) {

                //çalışana mail gönder.
                $result = _sendMail($User['Data'][0]->kul_email, ['Username' => $User['Data'][0]->kul_kadi, 'Password' => $User['Data'][0]->kul_parola], 'forgot_mail.html');
                if ($result['Result']) {
                    return ['message' => 'Bilgi Maili Gönderildi..', 'result' => true];
                } else {
                    return ['message' => 'Hata Oluştu', 'result' => false, 'Error' => $result['Data']];
                }
            }
            return ['message' => 'Kullanıcı Bulunamadı..', 'result' => false, 'Error' => $User['Data']];
        }
    }

    /**
     * this method will do client  transaction
     * if transactions is success it will return true
     * TR : Cari işlemlerinin gerçekleştiği bölgedir.
     */
    public function personelevents($params)
    {
        $result = false;
        $errsection = "Post datası Hatalı ..";
        $referance = str_replace(".", "", microtime(true));
        try {
            switch ($params['event']) {
                case "getdetail":
                    $data = [];
                    /**
                     * it will load personel model
                     * tr : personel modelini yükler
                     */
                    $this->CI->load->model('Personel_model');
                     /**
                     * it will return personel
                     * tr : personeli döner
                     */
                    $result = $this->CI->Personel_model->Get($params['where']);
                    if ($result['Result']) {
                        /**
                         * it will set personel data
                         * tr : personel datasını değişkene atar
                         */
                        $data['personel'] = $result['Data'][0];

                        /**
                         * it will load personel contact model
                         * tr : personel iletişim modelini yükler
                         */
                        $this->CI->load->model('Per_Iletisim_model');
                        /**
                         * it will return personel contact info
                         * tr : personeli iletişim bilgilerini döner
                         */
                        $result = $this->CI->Per_Iletisim_model->Get($params['where']);
                        if ($result['Result']) {
                            /**
                             * it will set personel contact data
                             * tr : personel iletişim datasını değişkene atar
                             */
                            $data['iletisim'] = $result['Data'];
                        }
                        /**
                         * it will take position info
                         * tr : pozisyon bilgilerini getirir
                         */
                        if (isset($data['personel']->per_pozisyon)) {
                            $this->CI->load->model('Per_Pozisyon_model');
                            $result = $this->CI->Per_Pozisyon_model->Get(['per_pozisyon.id' => $data['personel']->per_pozisyon]);
                            if ($result['Result']) {
                                $data['pozisyon'] =  $result['Data'];
                            }
                        }

                        /**
                         * it will get personnel categories
                         * tr: ürün kategorilerinin çekilmesi
                         */
                        $this->CI->load->model('Sys_Kat_Baglanti_model');
                        $data['kategoriler'] = []; 
                        $result = $this->CI->Sys_Kat_Baglanti_model->Get($params['where']);
                        if ($result['Result']) {
                            $data['kategoriler'] = $result['Data'];
                        }


                        /**
                         * it will take supervisor info
                         * tr : sorumlu kişi bilgilerini getirir
                         */
                        if (isset($data['personel']->per_sorumlu)) {
                            $this->CI->load->model('Personel_model');
                            $model = $this->CI->Personel_model;
                            $data['sorumlu'] = $model->Get(['id' => $data['personel']->per_sorumlu])['Data'][0];
                            /**
                             * it will take position info
                             * tr : pozisyon bilgilerini getirir
                             */
                            if (isset($data['sorumlu']->per_pozisyon)) {
                                $this->CI->load->model('Per_Pozisyon_model');
                                $model = $this->CI->Per_Pozisyon_model;
                                $data['sorumlu']->pozisyon = $model->Get(['per_pozisyon.id' => $data['sorumlu']->per_pozisyon])['Data'][0];
                            }
                        }

                    }
                    return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $data];
                    break;
                case "getjust":
                    $this->CI->load->model('Personel_model');
                    $model = $this->CI->Personel_model;
                    if (isset($params['where'])) {
                        $result = $model->Get($params['where']);
                    } else {
                        $result = $model->Get();
                    }
                    if ($result['Result']) {
                        return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
                    } else {
                        $errsection = $this->CI->db->error();
                        return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => isset($errsection) ? $errsection : 'Data Yok.'];
                    }
                    break;
                case "add":
                    $this->CI->db->trans_begin();
                    /*1-personel main record insert 
                    tr : ana personel kaydının eklenmesi
                    */
                    /*error message 
                    tr :hata mesajı
                    */
                    $errsection = "Personel Tablosu Eklenirken Hata ref=>" . $referance;
                    /* personel model loading
                    tr :personel modelinin yüklenmesi 
                    */
                    $this->CI->load->model('Personel_model');
                    $model = $this->CI->Personel_model;
                    /* set personel object 
                    tr : personel objesi atandı
                    */
                    $pobj = $params['personel'];
                    $pobj['created_by'] = $params['_wid'];
                    $pobj['ref'] = $referance;
                    $pobj['per_durum'] = $pobj['per_durum'] == "" ? 1 : $pobj['per_durum'];
                    /* there is not id created yet so jus unset it
                    tr : henüz id oluşturulmadı o yüzden ne gelirse modelden at
                    */
                    unset($pobj['id']);
                    /* send clean model for insert
                    tr : temiz modeli ekleme için gönder
                    */
                    $result = $model->Add($pobj);
                    /* if there is not any problem
                    tr : eğer bir problem yok ise
                    */
                    if ($result) {
                        if (isset($params['per_iletisim'])) {
                            /*1-personel contact record insert
                            tr : personel iletişim bilgilerinin eklenmesi
                            */
                            /*error message 
                            tr :hata mesajı
                            */
                            $errsection = "Personel İletişim Tablosuna Eklenirken Hata ref=>" . $referance;
                            /* personel contact model loading
                            tr :personel iletişim modelinin yüklenmesi 
                            */
                            $this->CI->load->model('Per_Iletisim_model');
                            $model = $this->CI->Per_Iletisim_model;
                            /* set personel contact object 
                            tr : personel iletişim objesi atandı
                            */
                            $pobj = $params['per_iletisim'];
                            $pobj['created_by'] =  $params['_wid'];
                            $pobj['ref'] = $referance;
                            /* there is not id created yet so jus unset it
                            tr : henüz id oluşturulmadı o yüzden ne gelirse modelden at
                            */
                            unset($pobj['id']);
                            /* send clean model for insert
                            tr : temiz modeli ekleme için gönder
                            */
                            $result = $model->Add($pobj);
                        }
                        if (isset($params['kullanicilar'])) {
                            //3- kullanıcılar tablo inserti
                            /*error message 
                            tr :hata mesajı
                            */
                            $errsection = "Personel Kullanılar Tablosuna Eklenirken Hata ref=>" . $referance;
                            /* personel user model loading
                            tr :personel kullanıcı modelinin yüklenmesi 
                            */
                            $this->CI->load->model('Kullanicilar_model');
                            $model = $this->CI->Kullanicilar_model;
                            /* set personel user object 
                            tr : personel kullanıcı objesi atandı
                            */
                            $pobj = $params['kullanicilar'];
                            $pobj['created_by'] =  $params['_wid'];
                            $pobj['kul_tip'] =  'P';
                            $pobj['ref'] = $referance;
                            /* there is not id created yet so jus unset it
                            tr : henüz id oluşturulmadı o yüzden ne gelirse modelden at
                            */
                            unset($pobj['id']);
                            /* send clean model for insert
                            tr : temiz modeli ekleme için gönder
                            */
                            $result = $model->Add($pobj);
                        }
                        if (isset($params['per_kategori']) && count($params['per_kategori']) > 0) {
                            /*4- personel category transactions
                            tr : personel kategorilerinin eklenmesi
                            */
                            /*error message 
                            tr :hata mesajı
                            */
                            $errsection = "Personel Kategori Tablosuna Eklenirken Hata ref=>" . $referance;
                            /* personel category model loading
                            tr :personel kategori modelinin yüklenmesi 
                           */
                            $this->CI->load->model('Sys_Kat_Baglanti_model');
                            $model = $this->CI->Sys_Kat_Baglanti_model;
                            $cobj = [];
                            foreach ($params['per_kategori'] as $c) {
                                $cobj['sys_kat_id'] = $c;
                                $cobj['sys_kat_tip'] = 'PERS';
                                $cobj['ref'] = $referance;
                                $cobj['created_by'] = $params['_wid'];
                                $result = $model->Add($cobj);
                            }
                        }
                    }
                    /* if there is not any problem
                    tr: eğer bir sorun yok ise
                    */
                    if ($result['Result']) {
                        /* accept query
                        tr : sorguyu kabul et
                        */
                        $this->CI->db->trans_commit();
                        _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $params['personel']['per_ad'], 'log_tip' => 'personel', 'ref' => 'ref-' . $referance, 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_PersonelEklendi']);
                        return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
                    } else {
                        $errsection = $this->CI->db->error();
                        $this->CI->db->trans_rollback();
                        return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => isset($errsection) ? $errsection : 'Data Yok.'];
                    }
                    break;
                case "get":
                    /**
                     * it will load personel model
                     * tr : personel modelini yükler
                     */
                    $this->CI->load->model('Personel_model');
                    //personel iletişim modelini yükler
                    $this->CI->load->model('Per_Iletisim_model');
                    //kullanıcı modelini yükler
                    $this->CI->load->model('Kullanicilar_model');
                    $personel = $this->CI->Personel_model;
                    $contact = $this->CI->Per_Iletisim_model;
                    $user = $this->CI->Kullanicilar_model;
                    if (isset($params['where'])) {
                        $resultp = $personel->Get($params['where']);
                        $resultc = $contact->Get($params['where']);
                        $resultu = $user->Get($params['where']);

                        $obj = [
                            'personel' => isset($resultp['Data']) ? $resultp['Data'][0] : [],
                            'per_iletisim' => isset($resultc['Data']) ? $resultc['Data'][0] : [],
                            'kullanici' => isset($resultu['Data']) ? $resultu['Data'][0] : [],
                        ];
                        return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $obj];
                    } else {
                        return ['message' => 'Referans Numarası Belirtiniz', 'result' => false];
                    }
                    break;
                case "update":
                    /**
                     * get personnel referance 
                     * tr: personel referansını al
                     */
                    $referance = $params['ref'];
                    $this->CI->db->trans_begin();
                    /**
                     * 1- update main personnel table
                     * tr : personel tablosunun güncellenmesi
                     * if personnel object is received
                     * tr : eğer personel objesi alınmış ise
                     */
                    if (isset($params['personel'])) {
                        /*error message 
                        tr :hata mesajı
                        */
                        $errsection = "Personel Tablosu Güncelenirken Hata ref=>" . $referance;
                        /* personnel model loading
                        tr :personel modelinin yüklenmesi 
                        */
                        $this->CI->load->model('Personel_model');
                        $model = $this->CI->Personel_model;
                        /**
                         *  get personnel object
                         * tr :personel objesini çek
                         */
                        $obj = (array) $model->Get(['id' => $params['personel']['id']])['Data'][0];
                        /**
                         * update personnel object
                         * tr :personel objesini güncelle 
                         */
                        foreach ($params['personel'] as $k => $v) {
                            if (array_key_exists($k, $obj)) {
                                $obj[$k] = $v;
                            }
                        }
                        $obj['per_durum'] = $params['personel']['per_durum'] == "" ? 1 : $params['personel']['per_durum'];
                        $obj['updated_by'] = $params['_wid'];
                        $obj['updated_on'] = date('Y-m-d h:i:s');
                        /* send object to db for update
                        tr :personel objesini güncelleme için gönder
                        */
                        $result = $model->Update($obj);
                    }
                    /**
                     * 2- update personnel contact teble
                     * tr: personel iletişim tablosunun güncellenmesi
                     */
                    if (isset($params['per_iletisim'])) {
                        /*error message 
                        tr :hata mesajı
                        */
                        $errsection = "Personel Iletişim Tablosu Güncellenirken Hata ref=>" . $referance;
                        /* personel model loading
                        tr :personel modelinin yüklenmesi 
                        */
                        $this->CI->load->model('Per_Iletisim_model');
                        $model = $this->CI->Per_Iletisim_model;
                        /* if object has id then update
                        tr :eğer objenin id si var ise güncelle
                        */
                        if ($params['per_iletisim']['id'] != '' && $params['per_iletisim']['id'] != '0') {
                            /* get old object
                            tr :eski objeyi çek
                            */
                            $obj = (array) $model->Get(['id' => $params['per_iletisim']['id']])['Data'][0];
                            /*update object
                            tr :objeyi güncelle
                            */
                            foreach ($params['per_iletisim'] as $k => $v) {
                                if (array_key_exists($k, $obj)) {
                                    $obj[$k] = $v;
                                }
                            }
                            $obj['updated_by'] = $params['_wid'];
                            $obj['updated_on'] = date('Y-m-d h:i:s');
                            /* send object to update
                            tr :objeyi güncellemeye gönder
                            */
                            $result = $model->Update($obj);
                        } else {
                            /* set object 
                            tr :objeyi set et
                            */
                            $crobj = $params['per_iletisim'];
                            $crobj['created_by'] =  $params['_wid'];
                            $crobj['ref'] = $referance;
                            /* is is not created yet so just unset it
                            tr :henüz id oluşmadı bu yüzden gelen nevar ise yok et
                            */
                            unset($crobj['id']);
                            /* send object to insert
                            tr :objeyi eklemeye gönder
                            */
                            $result = $model->Add($crobj);
                        }
                    }
                    /**
                     * 3- update users teble
                     * tr : personel kullanıcı tablosunun güncellenmesi
                     */
                    if (isset($params['kullanicilar'])) {
                        /*error message 
                        tr :hata mesajı
                        */
                        $errsection = "Kullanıcılar Güncelenirken Hata ref=>" . $referance;
                        /* personel user loading
                        tr :personel kullanıcı yüklenmesi 
                        */
                        $this->CI->load->model('Kullanicilar_model');
                        $model = $this->CI->Kullanicilar_model;
                        /* if object has id then update
                        tr :eğer objenin id si var ise güncelle
                        */
                        if ($params['kullanicilar']['id'] != '' && $params['kullanicilar']['id'] != '0') {
                            /* get old object
                            tr :eski objeyi çek
                            */
                            $obj = (array) $model->Get(['id' => $params['kullanicilar']['id']])['Data'][0];
                            /*update object
                            tr :objeyi güncelle
                            */
                            foreach ($params['kullanicilar'] as $k => $v) {
                                if (array_key_exists($k, $obj)) {
                                    $obj[$k] = $v;
                                }
                            }
                            $obj['kul_tip'] =  'P';
                            $obj['updated_by'] = $params['_wid'];
                            $obj['updated_on'] = date('Y-m-d h:i:s');
                            /* send object to update
                            tr :objeyi güncellemeye gönder
                            */
                            $result = $model->Update($obj);
                        } else {
                            /* set object 
                            tr :objeyi set et
                            */
                            $fobj = $params['kullanicilar'];
                            $fobj['created_by'] =  $params['_wid'];
                            $fobj['kul_tip'] =  'P';
                            $fobj['ref'] = $referance;
                            /* is is not created yet so just unset it
                            tr :henüz id oluşmadı bu yüzden gelen nevar ise yok et
                            */
                            unset($fobj['id']);
                            /* send object to insert
                            tr :objeyi eklemeye gönder
                            */
                            $result = $model->Add($fobj);
                        }
                    }
                    /**
                     * 4- personnel category transactions
                     * tr : personel kategorilerinin eklenmesi
                     */
                    if (isset($params['per_kategori']) && count($params['per_kategori']) > 0) {
                        /**
                         * error message 
                         * tr :hata mesajı
                         */
                        $errsection = "Personel Kategori Tablosuna Eklenirken Hata ref=>" . $referance;
                        /**
                         * personnel category model loading
                         * tr :personel kategori modelinin yüklenmesi 
                         */
                        $this->CI->load->model('Sys_Kat_Baglanti_model');
                        $model = $this->CI->Sys_Kat_Baglanti_model;
                        /**
                         * first remove old category connections for referance
                         * tr : ilk olarak eski kategori bağlantıları silinir
                         */
                        $model->Remove(array('ref' => $referance));
                        $cobj = [];
                        /**
                         * each connection will send to database
                         * tr : her bağlantı veri tabanına gönderilir
                         */
                        foreach ($params['per_kategori'] as $c) {
                            $cobj['sys_kat_id'] = $c;
                            $cobj['sys_kat_tip'] = 'PERS';
                            $cobj['ref'] = $referance;
                            $cobj['created_by'] = $params['_wid'];
                            $result = $model->Add($cobj);
                        }
                    }
                    /* if there is not any problem
                    tr: eğer bir sorun yok ise
                    */
                    if ($result['Result']) {
                        /* accept query
                        tr : sorguyu kabul et
                        */
                        $this->CI->db->trans_commit();
                        _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $params['personel']['per_ad'], 'log_tip' => 'personel', 'ref' => 'ref-' . $referance, 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_PerGuncellendi']);
                        return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
                    } else {
                        log_message('error',  $this->CI->db->last_query());
                        //$errsection = $this->CI->db->error();
                        $this->CI->db->trans_rollback();
                        return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
                    }
                    break;
                case "trash":
                    $referance = $params['ref'];
                    $this->CI->db->trans_begin();
                    /*1- personel trashing transaction 
                    tr : personel çöpe atma işlemi*/
                    /*error message 
                    tr : hata mesajı
                    */
                    $errsection = "Personel Çöpe Atılırken Hata ref=>" . $referance;
                    /* personel model loading
                    tr :personel modelinin yüklenmesi 
                    */
                    $this->CI->load->model('Personel_model');
                    $model = $this->CI->Personel_model;
                    /* get personel data
                    tr : personel datanın alınması
                    */
                    $obj = (array) $model->Get(['ref' => $referance])['Data'][0];
                    /*make status '0'
                    tr : durumun 0 yapılması
                    */
                    $obj['per_durum'] = 0;
                    $obj['updated_by'] = $params['_wid'];
                    $obj['updated_on'] = date('Y-m-d h:i:s');
                    /*model sended for update 
                    tr : model güncellemeye gönderildi
                    */
                    $result = $model->Update($obj);
                    /* if there is not any problem
                    tr: eğer bir sorun yok ise
                    */
                    if ($result['Result']) {
                        /* accept query
                        tr : sorguyu kabul et
                        */
                        $this->CI->db->trans_commit();
                        _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $obj['per_ad'], 'log_tip' => 'personel', 'ref' => 'ref-' . $referance, 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_PerCop']);
                        return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
                    } else {
                        log_message('error',  $this->CI->db->last_query());
                        //$errsection = $this->CI->db->error();
                        $this->CI->db->trans_rollback();
                        return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
                    }
                    break;
            }
        } catch (Exception $th) {
            $this->CI->db->trans_rollback();
            log_message('error',  $this->CI->db->last_query());
            log_message('error',  $th);
            $errsection = $this->CI->db->error();
            return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
        }
    }

    /**
     * this method will do client personel position transactions
     * if transactions is success it will return true
     * TR : Cari personel pozisyon işlemlerinin gerçekleştiği bölgedir.
     */
    public function ppozisyonevents($params)
    {
        try {
            //it will load model
            $this->CI->load->model('Sys_Ceviriler_model');
            $this->CI->load->model('Per_Pozisyon_model');
            $model =  $this->CI->Per_Pozisyon_model;
            $dilmodel = $this->CI->Sys_Ceviriler_model;
            $result = false;
            //start query transaction 
            $this->CI->db->trans_begin();
            //it will decide to transaction from 'event' parameter
            switch ($params['event']) {
                case "add":
                    $baslik_dil_key = 'd_Per_Pozisyon_' . str_replace(".", "", microtime(true));
                    //it will create object 
                    $bobj =  array(
                        'per_poz_baslik' => $params['per_poz_baslik'],
                        'baslik_dil_key' => $baslik_dil_key,
                        'created_by' => $params['_wid'],
                        'per_poz_durum' => 1
                    );
                    //it will turn result 
                    $result =  $model->Add($bobj);
                    //insert language informations 
                    if (count($params['language']['per_poz_baslik']) > 0) {
                        foreach ($params['language']['per_poz_baslik'] as $l) {
                            $obj =  array(
                                'anahtar' => $baslik_dil_key,
                                'sys_cev_dil_id' => $l['langid'],
                                'sys_cev_sayfa' => 'per_poz',
                                'sys_cev_ceviri' => $l['value']
                            );
                            $result = $dilmodel->Add($obj);
                        }
                    }
                    //
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $bobj['per_poz_baslik'], 'log_tip' => 'master', 'ref' => '', 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_PPozEkle']);
                    break;
                case "update":
                    //it will create object  
                    $obj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    $oldObj = $obj;
                    foreach ($params as $k => $v) {
                        if (array_key_exists($k, $obj)) {
                            $obj[$k] = $v;
                        }
                    }
                    $obj['updated_by'] = $params['_wid'];
                    $obj['updated_on'] = date('Y-m-d h:i:s');
                    //it will turn result 
                    $result = $model->Update($obj);
                    //update language rows
                    if (count($params['language']['per_poz_baslik']) > 0) {
                        foreach ($params['language']['per_poz_baslik'] as $l) {
                            //create array (id must be come with posted data)
                            $obj =  array(
                                'id' => $l['id'],
                                'sys_cev_ceviri' => $l['value'],
                                'updated_by' => $params['_wid'],
                                'updated_on' => date('Y-m-d h:i:s')
                            );
                            //update record
                            $result = $dilmodel->Update($obj);
                        }
                    }
                    ///
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['per_poz_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_PPozGuncel']);
                    break;
                case "trash":
                    $oldObj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    //it will create object 
                    $obj =  array(
                        'id' => $params['id'],
                        'per_poz_durum' => 0,
                        'deleted_by' => $params['_wid'],
                        'deleted_on' => date('Y-m-d h:i:s')
                    );
                    //it will turn result 
                    $result = $model->Update($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['per_poz_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_PPozCop']);
                    break;
                case "get":
                    if (isset($params['where'])) {
                        $result = $model->Get($params['where']);
                    } else {
                        $result = $model->Get();
                    }
                    break;
            }

            //if result is true it will return true
            if ($result['Result']) {
                //commit query if everything is okey
                $this->CI->db->trans_commit();
                return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
            } else {
                //rollback query on error
                $this->CI->db->trans_rollback();
                return ['message' => 'Hata Oluştu..', 'result' => false, 'Error' => isset($result['Data']) ? $result['Data'] : 'Data Yok'];
            }
        } catch (Exception $th) {
            $errsection = $th;
            //rollback query on error
            $this->CI->db->trans_rollback();
            if (strlen($this->CI->db->error()['message']) > 1) {
                $errsection = $this->CI->db->error();
            }
            return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
        }
    }
}
