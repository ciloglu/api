<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Current class is only use for personnel transactions
 * every return in system is have same pattern
 * it will return 'result' as true or false , and  'data' key for transaction result
 * example return is '['message'=>'Hata Oluştu..','result'=>false]'
 */

class System_w
{
    private $CI;
    /**
     * constructor for calling CI 
     */
    public function __construct()
    {
        $this->CI = &get_instance();
    }



    /**
     * this method will do client type transaction
     * if transactions is success it will return true
     * TR : Cari tipi işlemlerinin gerçekleştiği bölgedir.
     */
    public function ctipevents($params)
    {
        try {
            //it will load model
            $this->CI->load->model('Cari_Tipi_model');
            $this->CI->load->model('Sys_Ceviriler_model');
            $model =  $this->CI->Cari_Tipi_model;
            $dilmodel = $this->CI->Sys_Ceviriler_model;
            $result = false;
            //start query transaction 
            $this->CI->db->trans_begin();
            //it will decide to transaction from 'event' parameter
            switch ($params['event']) {
                case "add":
                    $baslik_dil_key = 'd_Cari_Tip_' . str_replace(".", "", microtime(true));
                    //it will create object 
                    $bobj =  array(
                        'cari_tip_baslik' => $params['cari_tip_baslik'],
                        'cari_tip_aciklama' =>  $params['cari_tip_aciklama'],
                        'created_by' => $params['_wid'],
                        'cari_tip_durum' => 1,
                        'baslik_dil_key' => $baslik_dil_key
                    );
                    //it will turn result 
                    $result = $model->Add($bobj);
                    //insert language informations 
                    if (count($params['language']['cari_tip_baslik']) > 0) {
                        foreach ($params['language']['cari_tip_baslik'] as $l) {
                            $obj =  array(
                                'anahtar' => $baslik_dil_key,
                                'sys_cev_dil_id' => $l['langid'],
                                'sys_cev_sayfa' => 'cari_tip',
                                'sys_cev_ceviri' => $l['value']
                            );
                            $result = $dilmodel->Add($obj);
                        }
                    }
                    //
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $bobj['cari_tip_baslik'], 'log_tip' => 'master', 'ref' => '', 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_CTipEklendi']);
                    break;
                case "update":
                    //it will create object    
                    $obj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    $oldObj = $obj;
                    foreach ($params as $k => $v) {
                        if (array_key_exists($k, $obj)) {
                            $obj[$k] = $v;
                        }
                    }
                    $obj['updated_by'] = $params['_wid'];
                    $obj['updated_on'] = date('Y-m-d h:i:s');
                    //it will turn result 
                    $result = $model->Update($obj);
                    //update language rows
                    if (count($params['language']['cari_tip_baslik']) > 0) {
                        foreach ($params['language']['cari_tip_baslik'] as $l) {
                            //create array (id must be come with posted data)
                            $obj =  array(
                                'id' => $l['id'],
                                'sys_cev_ceviri' => $l['value'],
                                'updated_by' => $params['_wid'],
                                'updated_on' => date('Y-m-d h:i:s')
                            );
                            //update record
                            $result = $dilmodel->Update($obj);
                        }
                    }
                    ///
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['cari_tip_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_CTipGuncellendi']);
                    break;
                case "trash":
                    $oldObj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    //it will create object 
                    $obj =  array(
                        'id' => $params['id'],
                        'cari_tip_durum' => 0,
                        'deleted_by' => $params['_wid'],
                        'deleted_on' => date('Y-m-d h:i:s')
                    );
                    //it will turn result 
                    $result =   $model->Update($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['cari_tip_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_CTipCop']);
                    break;
                case "get":
                    if (isset($params['where'])) {
                        $result = $model->Get($params['where']);
                    } else {
                        $result = $model->Get();
                    }
                    break;
            }
            //if result is true it will return true
            if ($result['Result']) {
                //commit query if everything is okey
                $this->CI->db->trans_commit();
                return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
            } else {
                //rollback query on error
                $this->CI->db->trans_rollback();
                return ['message' => 'Hata Oluştu..', 'result' => false, 'Error' => isset($result['Data']) ? $result['Data'] : 'Data Yok'];
            }
        } catch (Exception $th) {
            $errsection = $th;
            //rollback query on error
            $this->CI->db->trans_rollback();
            if (strlen($this->CI->db->error()['message']) > 1) {
                $errsection = $this->CI->db->error();
            }
            return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
        }
    }

    /**
     * this method will do client status transaction
     * if transactions is success it will return true
     * TR : Cari durumu işlemlerinin gerçekleştiği bölgedir.
     */
    public function cdurumevents($params)
    {
        try {
            //it will load model
            $this->CI->load->model('Sys_Ceviriler_model');
            $this->CI->load->model('Cari_Durum_model');
            $model =  $this->CI->Cari_Durum_model;
            $dilmodel = $this->CI->Sys_Ceviriler_model;
            $result = false;
            //start query transaction 
            $this->CI->db->trans_begin();
            //it will decide to transaction from 'event' parameter
            switch ($params['event']) {
                case "add":
                    $baslik_dil_key = 'd_Cari_Durum_' . str_replace(".", "", microtime(true));
                    //it will create object 
                    $bobj =  array(
                        'cari_dur_baslik' => $params['cari_dur_baslik'],
                        'cari_dur_aciklama' => $params['cari_dur_aciklama'],
                        'baslik_dil_key' => $baslik_dil_key,
                        'created_by' => $params['_wid'],
                        'cari_dur_durum' => 1
                    );
                    //it will turn result 
                    $result =  $model->Add($bobj);
                    //insert language informations 
                    if (count($params['language']['cari_dur_baslik']) > 0) {
                        foreach ($params['language']['cari_dur_baslik'] as $l) {
                            $obj =  array(
                                'anahtar' => $baslik_dil_key,
                                'sys_cev_dil_id' => $l['langid'],
                                'sys_cev_sayfa' => 'cari_durum',
                                'sys_cev_ceviri' => $l['value']
                            );
                            $result = $dilmodel->Add($obj);
                        }
                    }
                    //
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $bobj['cari_dur_baslik'], 'log_tip' => 'master', 'ref' => '', 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_CDurumEkle']);
                    break;
                case "update":
                    //it will create object  
                    $obj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    $oldObj = $obj;
                    foreach ($params as $k => $v) {
                        if (array_key_exists($k, $obj)) {
                            $obj[$k] = $v;
                        }
                    }
                    $obj['updated_by'] = $params['_wid'];
                    $obj['updated_on'] = date('Y-m-d h:i:s');
                    //it will turn result 
                    $result = $model->Update($obj);
                    //update language rows
                    if (count($params['language']['cari_dur_baslik']) > 0) {
                        foreach ($params['language']['cari_dur_baslik'] as $l) {
                            //create array (id must be come with posted data)
                            $obj =  array(
                                'id' => $l['id'],
                                'sys_cev_ceviri' => $l['value'],
                                'updated_by' => $params['_wid'],
                                'updated_on' => date('Y-m-d h:i:s')
                            );
                            //update record
                            $result = $dilmodel->Update($obj);
                        }
                    }
                    ///
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['cari_dur_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_CDurumGuncel']);
                    break;
                case "trash":
                    $oldObj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    //it will create object 
                    $obj =  array(
                        'id' => $params['id'],
                        'cari_dur_durum' => 0,
                        'deleted_by' => $params['_wid'],
                        'deleted_on' => date('Y-m-d h:i:s')
                    );
                    //it will turn result 
                    $result = $model->Update($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['cari_dur_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_CDurCop']);
                    break;
                case "get":
                    if (isset($params['where'])) {
                        $result = $model->Get($params['where']);
                    } else {
                        $result = $model->Get();
                    }
                    break;
            }

            //if result is true it will return true
            if ($result['Result']) {
                //commit query if everything is okey
                $this->CI->db->trans_commit();
                return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
            } else {
                //rollback query on error
                $this->CI->db->trans_rollback();
                return ['message' => 'Hata Oluştu..', 'result' => false, 'Error' => isset($result['Data']) ? $result['Data'] : 'Data Yok'];
            }
        } catch (Exception $th) {
            $errsection = $th;
            //rollback query on error
            $this->CI->db->trans_rollback();
            if (strlen($this->CI->db->error()['message']) > 1) {
                $errsection = $this->CI->db->error();
            }
            return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
        }
    }

    /**
     * this method will do client personel position transactions
     * if transactions is success it will return true
     * TR : Cari personel pozisyon işlemlerinin gerçekleştiği bölgedir.
     */
    public function cpozisyonevents($params)
    {
        try {
            //it will load model
            $this->CI->load->model('Sys_Ceviriler_model');
            $this->CI->load->model('Cari_Pozisyon_model');
            $model =  $this->CI->Cari_Pozisyon_model;
            $dilmodel = $this->CI->Sys_Ceviriler_model;
            $result = false;
            //start query transaction 
            $this->CI->db->trans_begin();
            //it will decide to transaction from 'event' parameter
            switch ($params['event']) {
                case "add":
                    $baslik_dil_key = 'd_Cari_Pozisyon_' . str_replace(".", "", microtime(true));
                    //it will create object 
                    $bobj =  array(
                        'cari_poz_baslik' => $params['cari_poz_baslik'],
                        'baslik_dil_key' => $baslik_dil_key,
                        'created_by' => $params['_wid'],
                        'cari_poz_durum' => 1
                    );
                    //it will turn result 
                    $result =  $model->Add($bobj);
                    //insert language informations 
                    if (count($params['language']['cari_poz_baslik']) > 0) {
                        foreach ($params['language']['cari_poz_baslik'] as $l) {
                            $obj =  array(
                                'anahtar' => $baslik_dil_key,
                                'sys_cev_dil_id' => $l['langid'],
                                'sys_cev_sayfa' => 'cari_durum',
                                'sys_cev_ceviri' => $l['value']
                            );
                            $result = $dilmodel->Add($obj);
                        }
                    }
                    //
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $bobj['cari_poz_baslik'], 'log_tip' => 'master', 'ref' => '', 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_CPozEkle']);
                    break;
                case "update":
                    //it will create object  
                    $obj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    $oldObj = $obj;
                    foreach ($params as $k => $v) {
                        if (array_key_exists($k, $obj)) {
                            $obj[$k] = $v;
                        }
                    }
                    $obj['updated_by'] = $params['_wid'];
                    $obj['updated_on'] = date('Y-m-d h:i:s');
                    //it will turn result 
                    $result = $model->Update($obj);
                    //update language rows
                    if (count($params['language']['cari_poz_baslik']) > 0) {
                        foreach ($params['language']['cari_poz_baslik'] as $l) {
                            //create array (id must be come with posted data)
                            $obj =  array(
                                'id' => $l['id'],
                                'sys_cev_ceviri' => $l['value'],
                                'updated_by' => $params['_wid'],
                                'updated_on' => date('Y-m-d h:i:s')
                            );
                            //update record
                            $result = $dilmodel->Update($obj);
                        }
                    }
                    ///
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['cari_poz_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_CPozGuncel']);
                    break;
                case "trash":
                    $oldObj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    //it will create object 
                    $obj =  array(
                        'id' => $params['id'],
                        'cari_poz_durum' => 0,
                        'deleted_by' => $params['_wid'],
                        'deleted_on' => date('Y-m-d h:i:s')
                    );
                    //it will turn result 
                    $result = $model->Update($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['cari_poz_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_CPozCop']);
                    break;
                case "get":
                    if (isset($params['where'])) {
                        $result = $model->Get($params['where']);
                    } else {
                        $result = $model->Get();
                    }
                    break;
            }

            //if result is true it will return true
            if ($result['Result']) {
                //commit query if everything is okey
                $this->CI->db->trans_commit();
                return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
            } else {
                //rollback query on error
                $this->CI->db->trans_rollback();
                return ['message' => 'Hata Oluştu..', 'result' => false, 'Error' => isset($result['Data']) ? $result['Data'] : 'Data Yok'];
            }
        } catch (Exception $th) {
            $errsection = $th;
            //rollback query on error
            $this->CI->db->trans_rollback();
            if (strlen($this->CI->db->error()['message']) > 1) {
                $errsection = $this->CI->db->error();
            }
            return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
        }
    }

    /**
     * this method will do system language transactions
     * if transactions is success it will return true
     * TR : Sistem dil işlemlerinin yapıldığı metotdur
     */
    public function sysdilevents($params)
    {
        try {
            $this->CI->load->model('Sys_Diller_model');
            $model = $this->CI->Sys_Diller_model;
            $result = false;
            //start query transaction 
            $this->CI->db->trans_begin();
            switch ($params['event']) {
                case "get":
                    if (isset($params['where'])) {
                        $result =  $model->Get($params['where']);
                    } else {
                        $result =  $model->Get();
                    }
                    break;
                case "add":
                    //it will create object 
                    $obj =  array(
                        'sys_dil_adi' => isset($params['sys_dil_adi']) ? $params['sys_dil_adi'] : 'Girilmedi.',
                        'sys_dil_bayrak' => isset($params['sys_dil_bayrak']) ? $params['sys_dil_bayrak'] : 'Girilmedi.',
                        'sys_dil_global' => isset($params['sys_dil_global']) ? $params['sys_dil_global'] : 'Girilmedi.',
                        'sys_dil_yetkili' => isset($params['sys_dil_yetkili']) ? $params['sys_dil_yetkili'] : '0',
                        'sys_dil_kod' => isset($params['sys_dil_kod']) ? $params['sys_dil_kod'] : 'Girilmedi.',
                        'sys_dil_durum' => 1,
                        'created_by' => $params['_wid'],
                    );
                    //it will turn result 
                    $result =   $model->Add($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $obj['sys_dil_adi'], 'log_tip' => 'master', 'ref' => '', 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_SDilEkle']);
                    break;
                case "update":
                    //it will create object   
                    $obj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    $oldObj = $obj;
                    foreach ($params as $k => $v) {
                        if (array_key_exists($k, $obj)) {
                            $obj[$k] = $v;
                        }
                    }
                    $obj['updated_by'] = $params['_wid'];
                    $obj['updated_on'] = date('Y-m-d h:i:s');
                    //it will turn result 
                    $result =   $model->Update($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['sys_dil_adi'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_SDilGuncel']);
                    break;
                case "trash":
                    //it will create object 
                    $oldObj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    $obj =  array(
                        'id' => $params['id'],
                        'sys_dil_durum' => 0,
                        'deleted_by' => $params['_wid'],
                        'deleted_on' => date('Y-m-d h:i:s')
                    );
                    //it will turn result 
                    $result =   $model->Update($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['sys_dil_adi'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_SDilCop']);
                    break;
            }

            //if result is true it will return true
            if ($result['Result']) {
                //commit query if everything is okey
                $this->CI->db->trans_commit();
                return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
            } else {
                //rollback query on error
                $this->CI->db->trans_rollback();
                return ['message' => 'Hata Oluştu..', 'result' => false, 'Error' => isset($result['Data']) ? $result['Data'] : 'Data Yok'];
            }
        } catch (Exception $th) {
            $errsection = $th;
            //rollback query on error
            $this->CI->db->trans_rollback();
            if (strlen($this->CI->db->error()['message']) > 1) {
                $errsection = $this->CI->db->error();
            }
            return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
        }
    }


    /**
     * this method will do system language translation  transactions
     * if transactions is success it will return true
     * TR : Sistem dil çeviri işlemlerinin yapıldığı metotdur
     */
    public function langevents($params)
    {
        try {
            $this->CI->load->model('Sys_Ceviriler_model');
            $model = $this->CI->Sys_Ceviriler_model;
            $result = false;
            //start query transaction 
            $this->CI->db->trans_begin();
            switch ($params['event']) {
                case "get":
                    if (isset($params['where'])) {
                        $result = $model->Get($params['where']);
                    } else {
                        $result = $model->Get();
                    }
                    break;
                case "add":
                    //if languages is sended
                    if (isset($params['language']) && count($params['language']) > 0) {
                        //foreach language value
                        foreach ($params['language'] as $l) {
                            //create object
                            $obj =  array(
                                'anahtar' => $params['anahtar'],
                                'sys_cev_sayfa' => isset($params['sys_cev_sayfa']) ? $params['sys_cev_sayfa'] : 'Girilmedi',
                                'sys_cev_ceviri' => $l['value'] != '-' ? $l['value'] : $params['anahtar'],
                                'sys_cev_dil_id' => $l['id'],
                                'created_by' => $params['_wid'],
                            );
                            //insert to database "sys_ceviriler" 
                            //it will turn result 
                            $result = $model->Add($obj);
                        }
                        _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $params['anahtar'], 'log_tip' => 'master', 'ref' => '', 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_DilCeviriEk']);
                    }
                    break;
                case "update":
                    //it will create object   
                    if (isset($params['language']) && count($params['language']) > 0) {
                        //take old data 
                        $oldObj = (array) $model->Get(array('id' => $params['id']))['Data'][0];
                        //remove old translations
                        $model->Remove(array('anahtar' => $oldObj['anahtar']));
                        //foreach translate values
                        foreach ($params['language'] as $l) {
                            //create object 
                            $obj =  array(
                                'anahtar' => $params['anahtar'],
                                'sys_cev_sayfa' => isset($params['sys_cev_sayfa']) ? $params['sys_cev_sayfa'] : 'Girilmedi',
                                'sys_cev_ceviri' => $l['value'] != '-' ? $l['value'] : $params['anahtar'],
                                'sys_cev_dil_id' => $l['id'],
                                'updated_by' => $params['_wid'],
                                'updated_on' => date('Y-m-d h:i:s')
                            );
                            //insert to database "sys_ceviriler" 
                            //it will turn result 
                            $result = $model->Add($obj);
                        }
                        _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['anahtar'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_DilCeviriGun']);
                    }
                    break;
                case "trash":
                    $oldObj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    //it will create object 
                    $obj =  array(
                        'id' => $params['id'],
                        'dil_durum' => 0,
                        'deleted_by' => $params['_wid'],
                        'deleted_on' => date('Y-m-d h:i:s')
                    );
                    //it will turn result 
                    $result = $model->Update($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['anahtar'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_DilCeviriCop']);
                    break;
            }

            //if result is true it will return true
            if ($result['Result']) {
                //commit query if everything is okey
                $this->CI->db->trans_commit();
                return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
            } else {
                //rollback query on error
                $this->CI->db->trans_rollback();
                return ['message' => 'Dil ekleme işleminde hata oluştu AR-GE ekibine bildiriniz !!', 'result' => false, 'Error' => isset($result['Data']) ? $result['Data'] : 'Data Yok'];
            }
        } catch (Exception $th) {
            $errsection = $th;
            //rollback query on error
            $this->CI->db->trans_rollback();
            if (strlen($this->CI->db->error()['message']) > 1) {
                $errsection = $this->CI->db->error();
            }
            return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
        }
    }

    /**
     * this method will do system country transactions
     * if transactions is success it will return true
     * TR : Sistem ülke işlemlerinin yapıldığı metotdur
     */
    public function ulkeevents($params)
    {
        try {
            //it will load model
            $this->CI->load->model('Sys_Ulkeler_model');
            $this->CI->load->model('Sys_Ceviriler_model');
            $model =  $this->CI->Sys_Ulkeler_model;
            $dilmodel = $this->CI->Sys_Ceviriler_model;
            $result = false;
            //start query transaction 
            $this->CI->db->trans_begin();
            //it will decide to transaction from 'event' parameter
            switch ($params['event']) {
                case "add":
                    $baslik_dil_key = 'd_Ulke_' . str_replace(".", "", microtime(true));
                    //it will create object 
                    $bobj =  array(
                        'sys_ul_baslik' => $params['sys_ul_baslik'],
                        'sys_ul_kod' =>  $params['sys_ul_kod'],
                        'sys_ul_aciklama' => isset($params['sys_ul_aciklama']) ? $params['sys_ul_aciklama'] : 'Girilmedi.',
                        'created_by' => $params['_wid'],
                        'sys_ul_durum' => 1,
                        'baslik_dil_key' => $baslik_dil_key,
                        'sys_ul_alvat' => isset($params['sys_ul_alvat']) ? $params['sys_ul_alvat'] : 0,
                        'sys_ul_satvat' => isset($params['sys_ul_satvat']) ? $params['sys_ul_satvat'] : 0,
                        'sys_ul_gumruk'=>isset($params['sys_ul_gumruk']) ? $params['sys_ul_gumruk'] : 0,
                    );
                    if (isset($params['sys_ul_pbirim'])) {
                        $bobj['sys_ul_pbirim'] = $params['sys_ul_pbirim'];
                    }
                    if (isset($params['sys_ul_vat'])) {
                        $bobj['sys_ul_vat'] = $params['sys_ul_vat'];
                    }
                    //it will turn result 
                    $result = $model->Add($bobj);
                    //insert language informations 
                    if (count($params['language']['sys_ul_baslik']) > 0) {
                        foreach ($params['language']['sys_ul_baslik'] as $l) {
                            $obj =  array(
                                'anahtar' => $baslik_dil_key,
                                'sys_cev_dil_id' => $l['langid'],
                                'sys_cev_sayfa' => 'ulkeler',
                                'sys_cev_ceviri' => $l['value']
                            );
                            $result = $dilmodel->Add($obj);
                        }
                    }
                    //
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $bobj['sys_ul_baslik'], 'log_tip' => 'master', 'ref' => '', 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_UlkeEk']);
                    break;
                case "update":
                    //it will create object    
                    $obj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    $oldObj = $obj;
                    foreach ($params as $k => $v) {
                        if (array_key_exists($k, $obj)) {
                            $obj[$k] = $v;
                        }
                    }
                    $obj['updated_by'] = $params['_wid'];
                    $obj['updated_on'] = date('Y-m-d h:i:s');
                    //it will turn result 
                    $result = $model->Update($obj);
                    //update language rows
                    if (count($params['language']['sys_ul_baslik']) > 0) {
                        foreach ($params['language']['sys_ul_baslik'] as $l) {
                            //create array (id must be come with posted data)
                            $obj =  array(
                                'id' => $l['id'],
                                'sys_cev_ceviri' => $l['value'],
                                'updated_by' => $params['_wid'],
                                'updated_on' => date('Y-m-d h:i:s')
                            );
                            //update record
                            $result = $dilmodel->Update($obj);
                        }
                    }
                    ///
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['sys_ul_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_UlkeGun']);
                    break;
                case "trash":
                    $oldObj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    //it will create object 
                    $obj =  array(
                        'id' => $params['id'],
                        'sys_ul_durum' => 0,
                        'deleted_by' => $params['_wid'],
                        'deleted_on' => date('Y-m-d h:i:s')
                    );
                    //it will turn result 
                    $result =   $model->Update($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['sys_ul_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_UlkeCop']);
                    break;
                case "get":
                    if (isset($params['where'])) {
                        $result = $model->Get($params['where']);
                    } else {
                        $result = $model->Get();
                    }
                    break;
            }
            //if result is true it will return true
            if ($result['Result']) {
                //commit query if everything is okey
                $this->CI->db->trans_commit();
                return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
            } else {
                //rollback query on error
                $this->CI->db->trans_rollback();
                return ['message' => 'Hata Oluştu..', 'result' => false, 'Error' => isset($result['Data']) ? $result['Data'] : 'Data Yok'];
            }
        } catch (Exception $th) {
            $errsection = $th;
            //rollback query on error
            $this->CI->db->trans_rollback();
            if (strlen($this->CI->db->error()['message']) > 1) {
                $errsection = $this->CI->db->error();
            }
            return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
        }
    }

    /**
     * this method will do system country transactions
     * if transactions is success it will return true
     * TR : Sistem şehir işlemlerinin yapıldığı metotdur
     */
    public function sehirevents($params)
    {
        try {
            //it will load model
            $this->CI->load->model('Sys_Sehirler_model');
            $model =  $this->CI->Sys_Sehirler_model;
            $result = false;
            //start query transaction 
            $this->CI->db->trans_begin();
            //it will decide to transaction from 'event' parameter
            switch ($params['event']) {
                case "add":
                    //it will create object 
                    $obj =  array(
                        'sys_seh_baslik' => $params['sys_seh_baslik'],
                        'sys_seh_ulke' => $params['sys_seh_ulke'],
                        'created_by' => $params['_wid'],
                        'sys_seh_durum' => 1
                    );
                    //it will turn result 
                    $result = $model->Add($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $obj['sys_seh_baslik'], 'log_tip' => 'master', 'ref' => '', 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_SehirEk']);
                    break;
                case "update":
                    //it will create object    
                    $obj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    $oldObj = $obj;
                    foreach ($params as $k => $v) {
                        if (array_key_exists($k, $obj)) {
                            $obj[$k] = $v;
                        }
                    }
                    $obj['updated_by'] = $params['_wid'];
                    $obj['updated_on'] = date('Y-m-d h:i:s');
                    //it will turn result 
                    $result = $model->Update($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['sys_seh_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_SehirGun']);
                    break;
                case "trash":
                    $oldObj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    //it will create object 
                    $obj =  array(
                        'id' => $params['id'],
                        'sys_seh_durum' => 0,
                        'deleted_by' => $params['_wid'],
                        'deleted_on' => date('Y-m-d h:i:s')
                    );
                    //it will turn result 
                    $result =   $model->Update($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['sys_seh_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_SehirCop']);
                    break;
                case "get":
                    if (isset($params['where'])) {
                        $result = $model->Get($params['where']);
                    } else {
                        $result = $model->Get();
                    }
                    break;
            }
            //if result is true it will return true
            if ($result['Result']) {
                //commit query if everything is okey
                $this->CI->db->trans_commit();
                return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
            } else {
                //rollback query on error
                $this->CI->db->trans_rollback();
                return ['message' => 'Hata Oluştu..', 'result' => false, 'Error' => isset($result['Data']) ? $result['Data'] : 'Data Yok'];
            }
        } catch (Exception $th) {
            $errsection = $th;
            //rollback query on error
            $this->CI->db->trans_rollback();
            if (strlen($this->CI->db->error()['message']) > 1) {
                $errsection = $this->CI->db->error();
            }
            return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
        }
    }

    /**
     * this method will do system currencies transactions
     * if transactions is success it will return true
     * TR : Sistem para birimi işlemlerinin yapıldığı metotdur
     */
    public function pbirimevents($params)
    {
        try {
            //it will load model
            $this->CI->load->model('Sys_Pbirim_model');
            $model =  $this->CI->Sys_Pbirim_model;
            $result = false;
            //start query transaction 
            $this->CI->db->trans_begin();
            //it will decide to transaction from 'event' parameter
            switch ($params['event']) {
                case "add":
                    //it will create object 
                    $obj =  array(
                        'sys_pbir_kod' => $params['sys_pbir_kod'],
                        'sys_pbir_icon' => $params['sys_pbir_icon'],
                        'sys_pbir_key' => $params['sys_pbir_key'],
                        'created_by' => $params['_wid'],
                        'sys_pbir_durum' => 1
                    );
                    //it will turn result 
                    $result = $model->Add($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $obj['sys_pbir_kod'], 'log_tip' => 'master', 'ref' => '', 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_PBirEk']);
                    break;
                case "update":
                    //it will create object    
                    $obj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    $oldObj = $obj;
                    foreach ($params as $k => $v) {
                        if (array_key_exists($k, $obj)) {
                            $obj[$k] = $v;
                        }
                    }
                    $obj['updated_by'] = $params['_wid'];
                    $obj['updated_on'] = date('Y-m-d h:i:s');
                    //it will turn result 
                    $result = $model->Update($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['sys_pbir_kod'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_PBirGun']);
                    break;
                case "trash":
                    $oldObj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    //it will create object 
                    $obj =  array(
                        'id' => $params['id'],
                        'sys_pbir_durum' => 0,
                        'deleted_by' => $params['_wid'],
                        'deleted_on' => date('Y-m-d h:i:s')
                    );
                    //it will turn result 
                    $result =   $model->Update($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['sys_pbir_kod'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_PBirCop']);
                    break;
                case "get":
                    if (isset($params['where'])) {
                        $result = $model->Get($params['where']);
                    } else {
                        $result = $model->Get();
                    }
                    break;
            }
            //if result is true it will return true
            if ($result['Result']) {
                //commit query if everything is okey
                $this->CI->db->trans_commit();
                return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
            } else {
                //rollback query on error
                $this->CI->db->trans_rollback();
                return ['message' => 'Hata Oluştu..', 'result' => false, 'Error' => isset($result['Data']) ? $result['Data'] : 'Data Yok'];
            }
        } catch (Exception $th) {
            $errsection = $th;
            //rollback query on error
            $this->CI->db->trans_rollback();
            if (strlen($this->CI->db->error()['message']) > 1) {
                $errsection = $this->CI->db->error();
            }
            return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
        }
    }

    /**
     * this method will do system cargo company transactions
     * if transactions is success it will return true
     * TR : Sistem kargo firması işlemlerinin yapıldığı metotdur
     */
    public function kargoevents($params)
    {
        try {
            //it will load model
            $this->CI->load->model('Sys_Kargo_model');
            $model =  $this->CI->Sys_Kargo_model;
            $result = false;
            //start query transaction 
            $this->CI->db->trans_begin();
            //it will decide to transaction from 'event' parameter
            switch ($params['event']) {
                case "add":
                    //it will create object 
                    $obj =  array(
                        'sys_car_baslik' => $params['sys_car_baslik'],
                        'sys_car_yetkili' => $params['sys_car_yetkili'],
                        'sys_car_tel' => $params['sys_car_tel'],
                        'sys_car_adres' => $params['sys_car_adres'],
                        'sys_car_aciklama' => $params['sys_car_aciklama'],
                        'created_by' => $params['_wid'],
                        'sys_car_durum' => 1
                    );
                    //it will turn result 
                    $result = $model->Add($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $obj['sys_car_baslik'], 'log_tip' => 'master', 'ref' => '', 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_KargoEk']);
                    break;
                case "update":
                    //it will create object    
                    $obj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    $oldObj = $obj;
                    foreach ($params as $k => $v) {
                        if (array_key_exists($k, $obj)) {
                            $obj[$k] = $v;
                        }
                    }
                    $obj['updated_by'] = $params['_wid'];
                    $obj['updated_on'] = date('Y-m-d h:i:s');
                    //it will turn result 
                    $result = $model->Update($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['sys_car_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_KargoGun']);
                    break;
                case "trash":
                    $oldObj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    //it will create object 
                    $obj =  array(
                        'id' => $params['id'],
                        'sys_car_durum' => 0,
                        'deleted_by' => $params['_wid'],
                        'deleted_on' => date('Y-m-d h:i:s')
                    );
                    //it will turn result 
                    $result =   $model->Update($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['sys_car_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_KargoCop']);
                    break;
                case "get":
                    if (isset($params['where'])) {
                        $result = $model->Get($params['where']);
                    } else {
                        $result = $model->Get();
                    }
                    break;
            }
            //if result is true it will return true
            if ($result['Result']) {
                //commit query if everything is okey
                $this->CI->db->trans_commit();
                return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
            } else {
                //rollback query on error
                $this->CI->db->trans_rollback();
                return ['message' => 'Hata Oluştu..', 'result' => false, 'Error' => isset($result['Data']) ? $result['Data'] : 'Data Yok'];
            }
        } catch (Exception $th) {
            $errsection = $th;
            //rollback query on error
            $this->CI->db->trans_rollback();
            if (strlen($this->CI->db->error()['message']) > 1) {
                $errsection = $this->CI->db->error();
            }
            return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
        }
    }

    /**
     * this method will do system delivery transactions
     * if transactions is success it will return true
     * TR : Sistem teslimat işlemlerinin yapıldığı metotdur
     */
    public function teslimatevents($params)
    {
        try {
            //it will load model
            $this->CI->load->model('Sys_Teslimat_model');
            $this->CI->load->model('Sys_Ceviriler_model');
            $model =  $this->CI->Sys_Teslimat_model;
            $dilmodel = $this->CI->Sys_Ceviriler_model;
            $result = false;
            //start query transaction 
            $this->CI->db->trans_begin();
            //it will decide to transaction from 'event' parameter
            switch ($params['event']) {
                case "add":
                    $baslik_dil_key = 'd_Teslimat_' . str_replace(".", "", microtime(true));
                    //it will create object 
                    $bobj =  array(
                        'sys_tes_baslik' => $params['sys_tes_baslik'],
                        'created_by' => $params['_wid'],
                        'sys_tes_durum' => 1,
                        'baslik_dil_key' => $baslik_dil_key
                    );
                    //it will turn result 
                    $result = $model->Add($bobj);
                    //insert language informations 
                    if (count($params['language']['sys_tes_baslik']) > 0) {
                        foreach ($params['language']['sys_tes_baslik'] as $l) {
                            $obj =  array(
                                'anahtar' => $baslik_dil_key,
                                'sys_cev_dil_id' => $l['langid'],
                                'sys_cev_sayfa' => 'teslimat',
                                'sys_cev_ceviri' => $l['value']
                            );
                            $result = $dilmodel->Add($obj);
                        }
                    }
                    //
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $bobj['sys_tes_baslik'], 'log_tip' => 'master', 'ref' => '', 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_TeslimatEk']);
                    break;
                case "update":
                    //it will create object    
                    $obj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    $oldObj = $obj;
                    foreach ($params as $k => $v) {
                        if (array_key_exists($k, $obj)) {
                            $obj[$k] = $v;
                        }
                    }
                    $obj['updated_by'] = $params['_wid'];
                    $obj['updated_on'] = date('Y-m-d h:i:s');
                    //it will turn result 
                    $result = $model->Update($obj);
                    //update language rows
                    if (count($params['language']['sys_tes_baslik']) > 0) {
                        foreach ($params['language']['sys_tes_baslik'] as $l) {
                            //create array (id must be come with posted data)
                            $obj =  array(
                                'id' => $l['id'],
                                'sys_cev_ceviri' => $l['value'],
                                'updated_by' => $params['_wid'],
                                'updated_on' => date('Y-m-d h:i:s')
                            );
                            //update record
                            $result = $dilmodel->Update($obj);
                        }
                    }
                    ///
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['sys_tes_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_TeslimatGun']);
                    break;
                case "trash":
                    $oldObj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    //it will create object 
                    $obj =  array(
                        'id' => $params['id'],
                        'sys_tes_durum' => 0,
                        'deleted_by' => $params['_wid'],
                        'deleted_on' => date('Y-m-d h:i:s')
                    );
                    //it will turn result 
                    $result = $model->Update($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['sys_tes_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_TeslimatCop']);
                    break;
                case "get":
                    if (isset($params['where'])) {
                        $result = $model->Get($params['where']);
                    } else {
                        $result = $model->Get();
                    }
                    break;
            }
            //if result is true it will return true
            if ($result['Result']) {
                //commit query if everything is okey
                $this->CI->db->trans_commit();
                return ['message' => 'Teslimat İşlemi Başarılı..', 'result' => true, 'data' => $result['Data']];
            } else {
                //rollback query on error
                $this->CI->db->trans_rollback();
                return ['message' => 'Hata Oluştu..', 'result' => false, 'Error' => isset($result['Data']) ? $result['Data'] : 'Data Yok'];
            }
        } catch (Exception $th) {
            $errsection = $th;
            //rollback query on error
            $this->CI->db->trans_rollback();
            if (strlen($this->CI->db->error()['message']) > 1) {
                $errsection = $this->CI->db->error();
            }
            return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
        }
    }

    /**
     * this method will do system price transactions
     * if transactions is success it will return true
     * TR : Sistem fiyat işlemlerinin yapıldığı metotdur
     */
    public function fiyatevents($params)
    {
        try {
            //it will load model
            $this->CI->load->model('Sys_Fiyatlar_model');
            $this->CI->load->model('Sys_Ceviriler_model');
            $model =  $this->CI->Sys_Fiyatlar_model;
            $dilmodel = $this->CI->Sys_Ceviriler_model;
            $result = false;
            //start query transaction 
            $this->CI->db->trans_begin();
            //it will decide to transaction from 'event' parameter
            switch ($params['event']) {
                case "add":
                    $baslik_dil_key = 'd_Fiyat_' . str_replace(".", "", microtime(true));
                    //it will create object 
                    $bobj =  array(
                        'sys_fi_baslik' => $params['sys_fi_baslik'],
                        'sys_fi_aciklama' => isset($params['sys_fi_aciklama']) ? $params['sys_fi_aciklama'] : 'Girilmedi.',
                        'created_by' => $params['_wid'],
                        'sys_fi_durum' => 1,
                        'baslik_dil_key' => $baslik_dil_key
                    );
                    //it will turn result 
                    $result = $model->Add($bobj);
                    //insert language informations 
                    if (count($params['language']['sys_fi_baslik']) > 0) {
                        foreach ($params['language']['sys_fi_baslik'] as $l) {
                            $obj =  array(
                                'anahtar' => $baslik_dil_key,
                                'sys_cev_dil_id' => $l['langid'],
                                'sys_cev_sayfa' => 'fiyatlar',
                                'sys_cev_ceviri' => $l['value']
                            );
                            $result = $dilmodel->Add($obj);
                        }
                    }
                    //
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $bobj['sys_fi_baslik'], 'log_tip' => 'master', 'ref' => '', 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_FiyatEk']);
                    break;
                case "update":
                    //it will create object    
                    $obj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    $oldObj = $obj;
                    foreach ($params as $k => $v) {
                        if (array_key_exists($k, $obj)) {
                            $obj[$k] = $v;
                        }
                    }
                    $obj['updated_by'] = $params['_wid'];
                    $obj['updated_on'] = date('Y-m-d h:i:s');
                    //it will turn result 
                    $result = $model->Update($obj);
                    //update language rows
                    if (count($params['language']['sys_fi_baslik']) > 0) {
                        foreach ($params['language']['sys_fi_baslik'] as $l) {
                            //create array (id must be come with posted data)
                            $obj =  array(
                                'id' => $l['id'],
                                'sys_cev_ceviri' => $l['value'],
                                'updated_by' => $params['_wid'],
                                'updated_on' => date('Y-m-d h:i:s')
                            );
                            //update record
                            $result = $dilmodel->Update($obj);
                        }
                    }
                    ///
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['sys_fi_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_FiyatGun']);
                    break;
                case "trash":
                    $oldObj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    //it will create object 
                    $obj =  array(
                        'id' => $params['id'],
                        'sys_fi_durum' => 0,
                        'deleted_by' => $params['_wid'],
                        'deleted_on' => date('Y-m-d h:i:s')
                    );
                    //it will turn result 
                    $result = $model->Update($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['sys_fi_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_FiyatCop']);
                    break;
                case "get":
                    if (isset($params['where'])) {
                        $result = $model->Get($params['where']);
                    } else {
                        $result = $model->Get();
                    }
                    break;
            }
            //if result is true it will return true
            if ($result['Result']) {
                //commit query if everything is okey
                $this->CI->db->trans_commit();
                return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
            } else {
                //rollback query on error
                $this->CI->db->trans_rollback();
                return ['message' => 'Hata Oluştu..', 'result' => false, 'Error' => isset($result['Data']) ? $result['Data'] : 'Data Yok'];
            }
        } catch (Exception $th) {
            $errsection = $th;
            //rollback query on error
            $this->CI->db->trans_rollback();
            if (strlen($this->CI->db->error()['message']) > 1) {
                $errsection = $this->CI->db->error();
            }
            return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
        }
    }

    /**
     * this method will do product Unit type transactions
     * if transactions is success it will return true
     * TR : Ürün birim işlemlerinin yapıldığı metotdur
     */
    public function birimevents($params)
    {
        try {
            //it will load model
            $this->CI->load->model('Urun_Birim_model');
            $this->CI->load->model('Sys_Ceviriler_model');
            $model =  $this->CI->Urun_Birim_model;
            $dilmodel = $this->CI->Sys_Ceviriler_model;
            $result = false;
            //start query transaction 
            $this->CI->db->trans_begin();
            //it will decide to transaction from 'event' parameter
            switch ($params['event']) {
                case "add":
                    $baslik_dil_key = 'd_UrunBirim_' . str_replace(".", "", microtime(true));
                    //it will create object 
                    $bobj =  array(
                        'ur_bir_baslik' => $params['ur_bir_baslik'],
                        'created_by' => $params['_wid'],
                        'ur_bir_durum' => 1,
                        'baslik_dil_key' => $baslik_dil_key
                    );
                    //it will turn result 
                    $result = $model->Add($bobj);
                    //insert language informations 
                    if (count($params['language']['ur_bir_baslik']) > 0) {
                        foreach ($params['language']['ur_bir_baslik'] as $l) {
                            $obj =  array(
                                'anahtar' => $baslik_dil_key,
                                'sys_cev_dil_id' => $l['langid'],
                                'sys_cev_sayfa' => 'urunbirim',
                                'sys_cev_ceviri' => $l['value']
                            );
                            $result = $dilmodel->Add($obj);
                        }
                    }
                    //
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $bobj['ur_bir_baslik'], 'log_tip' => 'master', 'ref' => '', 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_UrbirEk']);
                    break;
                case "update":
                    //it will create object    
                    $obj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    $oldObj = $obj;
                    foreach ($params as $k => $v) {
                        if (array_key_exists($k, $obj)) {
                            $obj[$k] = $v;
                        }
                    }
                    $obj['updated_by'] = $params['_wid'];
                    $obj['updated_on'] = date('Y-m-d h:i:s');
                    //it will turn result 
                    $result = $model->Update($obj);
                    //update language rows
                    if (count($params['language']['ur_bir_baslik']) > 0) {
                        foreach ($params['language']['ur_bir_baslik'] as $l) {
                            //create array (id must be come with posted data)
                            $obj =  array(
                                'id' => $l['id'],
                                'sys_cev_ceviri' => $l['value'],
                                'updated_by' => $params['_wid'],
                                'updated_on' => date('Y-m-d h:i:s')
                            );
                            //update record
                            $result = $dilmodel->Update($obj);
                        }
                    }
                    ///
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['ur_bir_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_UrBirGun']);
                    break;
                case "trash":
                    $oldObj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    //it will create object 
                    $obj =  array(
                        'id' => $params['id'],
                        'ur_bir_durum' => 0,
                        'deleted_by' => $params['_wid'],
                        'deleted_on' => date('Y-m-d h:i:s')
                    );
                    //it will turn result 
                    $result = $model->Update($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['ur_bir_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_UrbirCop']);
                    break;
                case "get":
                    if (isset($params['where'])) {
                        $result = $model->Get($params['where']);
                    } else {
                        $result = $model->Get();
                    }
                    break;
            }
            //if result is true it will return true
            if ($result['Result']) {
                //commit query if everything is okey
                $this->CI->db->trans_commit();
                return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
            } else {
                //rollback query on error
                $this->CI->db->trans_rollback();
                return ['message' => 'Hata Oluştu..', 'result' => false, 'Error' => isset($result['Data']) ? $result['Data'] : 'Data Yok'];
            }
        } catch (Exception $th) {
            $errsection = $th;
            //rollback query on error
            $this->CI->db->trans_rollback();
            if (strlen($this->CI->db->error()['message']) > 1) {
                $errsection = $this->CI->db->error();
            }
            return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
        }
    }

    /**
     * this method will do system category transactions
     * if transactions is success it will return true
     * TR : Sistem kategori işlemlerinin yapıldığı metotdur
     */
    public function syskatevents($params)
    {
        try {
            /**
             * first it will load necessary models
             * tr : önce gerekli olan modelleri yükler
             */
            $this->CI->load->model('Sys_Kategoriler_model');
            $this->CI->load->model('Sys_Ceviriler_model');
            $model =  $this->CI->Sys_Kategoriler_model;
            $dilmodel = $this->CI->Sys_Ceviriler_model;
            $result = false;
            /**
             * starting query point
             * tr: sorgu başlama noktası
             */
            $this->CI->db->trans_begin();
            /**
             * it will decide to event from 'event' parameter
             * tr : gerekli görevin ne olduğuna 'event' parametresinden karar verir
             */
            switch ($params['event']) {
                case "add":
                    $baslik_dil_key = 'd_SistemKategori_' . str_replace(".", "", microtime(true));
                    /**it will create object 
                     * tr : objeyi oluşturur
                     */
                    $bobj =  array(
                        'sys_kat_baslik' => $params['sys_kat_baslik'],
                        'sys_kat_ust' => isset($params['sys_kat_ust']) ? $params['sys_kat_ust'] : 0,
                        'created_by' => $params['_wid'],
                        'sys_kat_durum' => 1,
                        'sys_kat_tip' => $params['sys_kat_tip'],
                        'baslik_dil_key' => $baslik_dil_key
                    );
                    /**
                     * it will return db result
                     * tr : veri tabanı yanıtını döner
                     */
                    $result = $model->Add($bobj);
                    /**
                     * insert language informations
                     * tr : dil bilgilerini girer
                     */
                    if (count($params['language']['sys_kat_baslik']) > 0) {
                        foreach ($params['language']['sys_kat_baslik'] as $l) {
                            $obj =  array(
                                'anahtar' => $baslik_dil_key,
                                'sys_cev_dil_id' => $l['langid'],
                                'sys_cev_sayfa' => 'sistemkategori',
                                'sys_cev_ceviri' => $l['value']
                            );
                            /**
                             * it will return db result
                             * tr : veri tabanı yanıtını döner
                             */
                            $result = $dilmodel->Add($obj);
                        }
                    }
                    //region loging
                    $log_message = '';
                    /**
                     * it will decide log message from 'sys_kat_tip' parameter
                     * tr : log mesajına 'sys_kat_tip' parametresinden karar verir
                     */
                    switch ($params['sys_kat_tip']) {
                        case "CARI":
                            $log_message = 'l_SBolgeEkle';
                            break;
                        case "PERS":
                            $log_message = 'l_DepEk';
                            break;
                        case "URUN":
                            $log_message = 'l_UrKatEk';
                            break;
                    }
                    /**
                     * it will save log record
                     * tr : log mesajını kaydeder
                     */
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $bobj['sys_kat_baslik'], 'log_tip' => 'master', 'ref' => '', 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => $log_message]);
                    //endregion
                    break;
                case "update":
                    /**
                     * it will get old object 
                     * tr : eski objeyi çeker
                     */
                    $obj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    $oldObj = $obj;
                    /**
                     * it will create object 
                     * tr : objeyi oluşturur
                     */
                    foreach ($params as $k => $v) {
                        if (array_key_exists($k, $obj)) {
                            $obj[$k] = $v;
                        }
                    }
                    $obj['updated_by'] = $params['_wid'];
                    $obj['updated_on'] = date('Y-m-d h:i:s');
                    /**
                     * it will return db result
                     * tr : veri tabanı yanıtını döner
                     */
                    $result = $model->Update($obj);
                    /**
                     * it will update language records
                     * tr : dil kayıtlarını günceller
                     */
                    if (count($params['language']['sys_kat_baslik']) > 0) {
                        foreach ($params['language']['sys_kat_baslik'] as $l) {
                            /**
                             * it will create object 
                             * tr : objeyi oluşturur
                             */
                            $obj =  array(
                                'id' => $l['id'],
                                'sys_cev_ceviri' => $l['value'],
                                'updated_by' => $params['_wid'],
                                'updated_on' => date('Y-m-d h:i:s')
                            );
                            /**
                             * it will return db result
                             * tr : veri tabanı yanıtını döner
                             */
                            $result = $dilmodel->Update($obj);
                        }
                    }
                    //region loging
                    $log_message = '';
                    /**
                     * it will decide log message from 'sys_kat_tip' parameter
                     * tr : log mesajına 'sys_kat_tip' parametresinden karar verir
                     */
                    switch ($params['sys_kat_tip']) {
                        case "CARI":
                            $log_message = 'l_SBolGuncel';
                            break;
                        case "PERS":
                            $log_message = 'l_DepGun';
                            break;
                        case "URUN":
                            $log_message = 'l_UrKatGun';
                            break;
                    }
                    /**
                     * it will save log record
                     * tr : log mesajını kaydeder
                     */
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['sys_kat_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' =>  $log_message]);
                    //endregion
                    break;
                case "trash":
                    /**
                     * it will get old object 
                     * tr : eski objeyi çeker
                     */
                    $oldObj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    /**
                     * it will create object 
                     * tr : objeyi oluşturur
                     */
                    $obj =  array(
                        'id' => $params['id'],
                        'sys_kat_durum' => 0,
                        'deleted_by' => $params['_wid'],
                        'deleted_on' => date('Y-m-d h:i:s')
                    );
                    /**
                     * it will return db result
                     * tr : veri tabanı yanıtını döner
                     */
                    $result = $model->Update($obj);
                    //region loging
                    $log_message = '';
                    /**
                     * it will decide log message from 'sys_kat_tip' parameter
                     * tr : log mesajına 'sys_kat_tip' parametresinden karar verir
                     */
                    switch ($params['sys_kat_tip']) {
                        case "CARI":
                            $log_message = 'l_SBolCop';
                            break;
                        case "PERS":
                            $log_message = 'l_DepCop';
                            break;
                        case "URUN":
                            $log_message = 'l_UrKatCop';
                            break;
                    }
                    /**
                     * it will save log record
                     * tr : log mesajını kaydeder
                     */
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['sys_kat_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => $log_message]);
                    //endregion
                    break;
                case "get":
                    if (isset($params['where'])) {
                        $result = $model->Get($params['where']);
                    } else {
                        $result = $model->Get();
                    }
                    break;
                case "getcon":
                    $this->CI->load->model('Sys_Kat_Baglanti_model');
                    $model =  $this->CI->Sys_Kat_Baglanti_model;
                    if (isset($params['where'])) {
                        $result = $model->Get($params['where']);
                    } else {
                        $result = $model->Get();
                    }
                    break;
            }
            /**
             * it will return result
             * tr : sonucu döner
             */
            if ($result['Result']) {
                /**
                 * it will commit query if everything is okey
                 * tr : eğer herşey doğru ise sorguyu onaylar
                 */
                $this->CI->db->trans_commit();
                return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
            } else {
                /**
                 * if result is false it will rollback query
                 * tr: sonuç false ise sorguyu geri çeker
                 */
                $this->CI->db->trans_rollback();
                return ['message' => 'Hata Oluştu..', 'result' => false, 'Error' => isset($result['Data']) ? $result['Data'] : 'Data Yok'];
            }
        } catch (Exception $th) {
            $errsection = $th;
            /**
             * if it will throw error it will rollback query
             * tr: eğer hataya düşer ise sorugu geri çeker
             */
            $this->CI->db->trans_rollback();
            if (strlen($this->CI->db->error()['message']) > 1) {
                $errsection = $this->CI->db->error();
            }
            return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
        }
    }

    /**
     * this method will do system group type transactions
     * if transactions is success it will return true
     * TR : Sistem grup tipi işlemlerinin yapıldığı metotdur
     */
    public function sysgrtipevents($params)
    {
        try {
            //it will load model
            $this->CI->load->model('Sys_Grup_Tip_model');
            $this->CI->load->model('Sys_Ceviriler_model');
            $model =  $this->CI->Sys_Grup_Tip_model;
            $dilmodel = $this->CI->Sys_Ceviriler_model;
            $result = false;
            //start query transaction 
            $this->CI->db->trans_begin();
            //it will decide to transaction from 'event' parameter
            switch ($params['event']) {
                case "add":
                    $baslik_dil_key = 'd_GrupTip_' . str_replace(".", "", microtime(true));
                    //it will create object 
                    $bobj =  array(
                        'gr_tip_baslik' => $params['gr_tip_baslik'],
                        'created_by' => $params['_wid'],
                        'gr_tip_durum' => 1,
                        'baslik_dil_key' => $baslik_dil_key
                    );
                    //it will turn result 
                    $result = $model->Add($bobj);
                    //insert language informations 
                    if (count($params['language']['gr_tip_baslik']) > 0) {
                        foreach ($params['language']['gr_tip_baslik'] as $l) {
                            $obj =  array(
                                'anahtar' => $baslik_dil_key,
                                'sys_cev_dil_id' => $l['langid'],
                                'sys_cev_sayfa' => 'gruptip',
                                'sys_cev_ceviri' => $l['value']
                            );
                            $result = $dilmodel->Add($obj);
                        }
                    }
                    //
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $bobj['gr_tip_baslik'], 'log_tip' => 'master', 'ref' => '', 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_GrupTipEk']);
                    break;
                case "update":
                    //it will create object    
                    $obj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    $oldObj = $obj;
                    foreach ($params as $k => $v) {
                        if (array_key_exists($k, $obj)) {
                            $obj[$k] = $v;
                        }
                    }
                    $obj['updated_by'] = $params['_wid'];
                    $obj['updated_on'] = date('Y-m-d h:i:s');
                    //it will turn result 
                    $result = $model->Update($obj);
                    //update language rows
                    if (count($params['language']['gr_tip_baslik']) > 0) {
                        foreach ($params['language']['gr_tip_baslik'] as $l) {
                            //create array (id must be come with posted data)
                            $obj =  array(
                                'id' => $l['id'],
                                'sys_cev_ceviri' => $l['value'],
                                'updated_by' => $params['_wid'],
                                'updated_on' => date('Y-m-d h:i:s')
                            );
                            //update record
                            $result = $dilmodel->Update($obj);
                        }
                    }
                    ///
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['gr_tip_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_GrupTipGun']);
                    break;
                case "trash":
                    $oldObj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    //it will create object 
                    $obj =  array(
                        'id' => $params['id'],
                        'gr_tip_durum' => 0,
                        'deleted_by' => $params['_wid'],
                        'deleted_on' => date('Y-m-d h:i:s')
                    );
                    //it will turn result 
                    $result = $model->Update($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['gr_tip_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_GrupTipCop']);
                    break;
                case "get":
                    if (isset($params['where'])) {
                        $result = $model->Get($params['where']);
                    } else {
                        $result = $model->Get();
                    }
                    break;
            }
            //if result is true it will return true
            if ($result['Result']) {
                //commit query if everything is okey
                $this->CI->db->trans_commit();
                return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
            } else {
                //rollback query on error
                $this->CI->db->trans_rollback();
                return ['message' => 'Hata Oluştu..', 'result' => false, 'Error' => isset($result['Data']) ? $result['Data'] : 'Data Yok'];
            }
        } catch (Exception $th) {
            $errsection = $th;
            //rollback query on error
            $this->CI->db->trans_rollback();
            if (strlen($this->CI->db->error()['message']) > 1) {
                $errsection = $this->CI->db->error();
            }
            return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
        }
    }

    /**
     * this method will do system group transactions
     * if transactions is success it will return true
     * TR : Sistem grup işlemlerinin yapıldığı metotdur
     */
    public function sysgrupevents($params)
    {
        try {
            log_message('error',json_encode($params));
            $result = false;
            $errsection = "Post datası Hatalı ..";
            if (!isset($params['ref']) || strlen(trim($params['ref']))==0) {
                $referance = str_replace(".", "", microtime(true));
            } else {
                $referance = $params['ref'];
            }
            /** 
             * query start point  
             * tr : sorgu başlangıç noktası
             */
            $this->CI->db->trans_begin();
            /**
             * it will decide to transaction from 'event' parameter
             * tr : 'event' parametresi ile işlem türüne karar verir
             */
            switch ($params['event']) {
                case "trash":
                    /**
                     * 1- group trashing transaction 
                     * tr : group çöpe atma işlemi
                     */
                    /**
                     * error message 
                     * tr : hata mesajı
                     */
                    $errsection = "Grup Çöpe Atılırken Hata ref=>" . $referance;
                    /**
                     *  group model loading
                     * tr :group modelinin yüklenmesi 
                     */
                    $this->CI->load->model('Sys_Grup_model');
                    $model = $this->CI->Sys_Grup_model;
                    /* get group data
                    tr : grup datanın alınması
                    */
                    $obj = (array) $model->Get(['ref' => $referance])['Data'][0];
                    /** 
                     * make status '0'
                     * tr : durumun 0 yapılması
                     */
                    $obj['gr_durum'] = 0;
                    $obj['updated_by'] = $params['_wid'];
                    $obj['updated_on'] = date('Y-m-d h:i:s');
                    /**
                     * model sended for update 
                     * tr : model güncellemeye gönderildi
                     */
                    $result = $model->Update($obj);
                    /**
                     * if there is not any problem
                     * tr: eğer bir sorun yok ise
                     */
                    if ($result['Result']) {
                        /**
                         * accept query
                         * tr : sorguyu kabul et
                         */
                        $this->CI->db->trans_commit();
                        _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $obj['gr_baslik'], 'log_tip' => 'master', 'ref' => 'ref-' . $referance, 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_GrupCop']);
                        return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
                    } else {
                        log_message('error',  $this->CI->db->last_query());
                        //$errsection = $this->CI->db->error();
                        $this->CI->db->trans_rollback();
                        return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
                    }
                    break;
                case "update":
                    $result['Result'] = false;
                    if (isset($params['grup'])) {
                        /**
                         * 1- group main record update 
                         * tr : ana grup kaydının eklenmesi
                         */
                        /**
                         * error message 
                         * tr :hata mesajı
                         */
                        $errsection = "Grup Tablosu Güncellenirken Hata ref=>" . $referance;
                        /**
                         * group model loading
                         * tr :grup modelinin yüklenmesi 
                         */
                        $this->CI->load->model('Sys_Grup_model');
                        $model = $this->CI->Sys_Grup_model;
                        /**
                         * get group object
                         * tr :group objesini çek
                         */
                        $obj = (array) $model->Get(['id' => $params['grup']['id']])['Data'][0];
                        /**
                         * update group object
                         * tr :group objesini güncelle 
                         */
                        foreach ($params['grup'] as $k => $v) {
                            if (array_key_exists($k, $obj)) {
                                $obj[$k] = $v;
                            }
                        }
                        $obj['gr_durum'] = $params['grup']['gr_durum'] == "" ? 1 : $params['grup']['gr_durum'];
                        $obj['updated_by'] = $params['_wid'];
                        $obj['updated_on'] = date('Y-m-d h:i:s');
                        /* send object to db for update
                        tr :personel objesini güncelleme için gönder
                        */
                        $result = $model->Update($obj);
                        if ($result) {
                            /** 
                             * it will load group connection model
                             * tr : grup bağlantı modelini yükler
                             */
                            $this->CI->load->model('Sys_Grup_Baglanti_model');
                            $model = $this->CI->Sys_Grup_Baglanti_model;
                            $model->Remove(array('ref' => $referance));
                            if (isset($params['gr_baglanti']) && count($params['gr_baglanti']) > 0) {
                                /**
                                 * for every connection
                                 * tr : her bir bağlantı için
                                 */
                                foreach ($params['gr_baglanti'] as $b) {
                                    $b['ref'] = $referance;
                                    $b['created_by'] = $params['_wid'];
                                    $result = $model->Add($b);
                                }
                            }
                        }
                    }
                    /**
                     * if there is not any problem
                     * tr: eğer bir sorun yok ise
                     */
                    if ($result['Result']) {
                        /**
                         * accept query
                         * tr : sorguyu kabul et
                         */
                        $this->CI->db->trans_commit();
                        _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $params['grup']['gr_baslik'], 'log_tip' => 'master', 'ref' => 'ref-' . $referance, 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_GrupGun']);
                        return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
                    } else {
                        $errsection = $this->CI->db->error();
                        /**
                         * rollback query and send error data
                         * tr : sorguyu geri çek ve hata verisini gönder
                         */
                        $this->CI->db->trans_rollback();
                        return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => isset($errsection) ? $errsection : 'Data Yok.'];
                    }

                    break;
                case "add":
                    /**
                     * 1- group main record insert 
                     * tr : ana grup kaydının eklenmesi
                     */
                    /**
                     * error message 
                     * tr :hata mesajı
                     */
                    $errsection = "Grup Tablosu Eklenirken Hata ref=>" . $referance;
                    /** 
                     * it will load group model
                     * tr : grup modelini yükler
                     */
                    $this->CI->load->model('Sys_Grup_model');
                    $model = $this->CI->Sys_Grup_model;
                    /** 
                     * set personel object 
                     * tr : personel objesi atandı
                     */
                    $pobj = $params['grup'];
                    $pobj['created_by'] = $params['_wid'];
                    $pobj['ref'] = $referance;
                    $pobj['gr_durum'] = 1;
                    /**
                     * there is not id created yet so jus unset it
                     * tr : henüz id oluşturulmadı o yüzden ne gelirse modelden at
                     */
                    unset($pobj['id']);
                    /**
                     * send clean model for insert
                     * tr : temiz modeli ekleme için gönder
                     */
                    $result = $model->Add($pobj);
                    /**
                     * if there is not any problem
                     * tr : eğer bir problem yok ise
                     */
                    if ($result) {
                        if (isset($params['gr_baglanti']) && count($params['gr_baglanti']) > 0) {
                            /** 
                             * it will load group connection model
                             * tr : grup bağlantı modelini yükler
                             */
                            $this->CI->load->model('Sys_Grup_Baglanti_model');
                            $model = $this->CI->Sys_Grup_Baglanti_model;
                            /**
                             * for every connection
                             * tr : her bir bağlantı için
                             */
                            foreach ($params['gr_baglanti'] as $b) {
                                $b['ref'] = $referance;
                                $b['created_by'] = $params['_wid'];
                                $result = $model->Add($b);
                            }
                        }
                    }
                    /**
                     * if there is not any problem
                     * tr: eğer bir sorun yok ise
                     */
                    if ($result['Result']) {
                        /**
                         * accept query
                         * tr : sorguyu kabul et
                         */
                        $this->CI->db->trans_commit();
                        _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $params['grup']['gr_baslik'], 'log_tip' => 'master', 'ref' => 'ref-' . $referance, 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_GrupEklendi']);
                        return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
                    } else {
                        $errsection = $this->CI->db->error();
                        /**
                         * rollback query and send error data
                         * tr : sorguyu geri çek ve hata verisini gönder
                         */
                        $this->CI->db->trans_rollback();
                        return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => isset($errsection) ? $errsection : 'Data Yok.'];
                    }
                    break;
                case "get":
                    $this->CI->load->model('Sys_Grup_model');
                    $model = $this->CI->Sys_Grup_model;
                    $result = [];
                    if (isset($params['where'])) {
                        $result = $model->Get($params['where']);
                    } else {
                        $result['Result'] = false;
                    }
                    /**
                     * if there is not any problem
                     * tr: eğer bir sorun yok ise
                     */
                    if ($result['Result']) {
                        /**
                         * accept query
                         * tr : sorguyu kabul et
                         */
                        $this->CI->db->trans_commit();
                        return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
                    } else {
                        /**
                         * rollback query and send error data
                         * tr : sorguyu geri çek ve hata verisini gönder
                         */
                        $this->CI->db->trans_rollback();
                        return ['message' => 'Hata Oluştu..', 'result' => false, 'Error' => isset($result['Data']) ? $result['Data'] : 'Data Yok'];
                    }
                    break;
                case "getcon":
                    $this->CI->load->model('Sys_Grup_Baglanti_model');
                    $model = $this->CI->Sys_Grup_Baglanti_model;
                    $result = [];
                    if (isset($params['where'])) {
                        $result = $model->Get($params['where']);
                    } else {
                        $result['Result'] = false;
                    }
                    /**
                     * if there is not any problem
                     * tr: eğer bir sorun yok ise
                     */
                    if ($result['Result']) {
                        /**
                         * accept query
                         * tr : sorguyu kabul et
                         */
                        $this->CI->db->trans_commit();
                        return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
                    } else {
                        /**
                         * rollback query and send error data
                         * tr : sorguyu geri çek ve hata verisini gönder
                         */
                        $this->CI->db->trans_rollback();
                        return ['message' => 'Hata Oluştu..', 'result' => false, 'Error' => isset($result['Data']) ? $result['Data'] : 'Data Yok'];
                    }
                    break;
            }
        } catch (Exception $th) {
            $errsection = $th;
            log_message('error', $th);
            //rollback query on error
            $this->CI->db->trans_rollback();
            if (strlen($this->CI->db->error()['message']) > 1) {
                $errsection = $this->CI->db->error();
            }
            return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
        }
    }

    /**
     * this method will do system bank transactions
     * if transactions is success it will return true
     * TR : Sistem banka işlemlerinin yapıldığı metotdur
     */
    public function sysbanevents($params)
    {
        try {
            //it will load model
            $this->CI->load->model('Sys_Bankalar_model');
            $model =  $this->CI->Sys_Bankalar_model;
            $result = false;
            //start query transaction 
            $this->CI->db->trans_begin();
            //it will decide to transaction from 'event' parameter
            switch ($params['event']) {
                case "add":
                    //it will create object 
                    $obj =  array(
                        'sys_ban_baslik' => $params['sys_ban_baslik'],
                        'sys_ban_sube' => $params['sys_ban_sube'],
                        'sys_ban_aciklama' => $params['sys_ban_aciklama'],
                        'created_by' => $params['_wid'],
                        'sys_ban_durum' => 1
                    );
                    //it will turn result 
                    $result = $model->Add($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $obj['sys_ban_baslik'], 'log_tip' => 'master', 'ref' => '', 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_BanEk']);
                    break;
                case "update":
                    //it will create object    
                    $obj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    $oldObj = $obj;
                    foreach ($params as $k => $v) {
                        if (array_key_exists($k, $obj)) {
                            $obj[$k] = $v;
                        }
                    }
                    $obj['updated_by'] = $params['_wid'];
                    $obj['updated_on'] = date('Y-m-d h:i:s');
                    //it will turn result 
                    $result = $model->Update($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['sys_ban_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_BanGun']);
                    break;
                case "trash":
                    $oldObj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    //it will create object 
                    $obj =  array(
                        'id' => $params['id'],
                        'sys_ban_durum' => 0,
                        'deleted_by' => $params['_wid'],
                        'deleted_on' => date('Y-m-d h:i:s')
                    );
                    //it will turn result 
                    $result =   $model->Update($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['sys_ban_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_BanCop']);
                    break;
                case "get":
                    if (isset($params['where'])) {
                        $result = $model->Get($params['where']);
                    } else {
                        $result = $model->Get();
                    }
                    break;
            }
            //if result is true it will return true
            if ($result['Result']) {
                //commit query if everything is okey
                $this->CI->db->trans_commit();
                return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
            } else {
                //rollback query on error
                $this->CI->db->trans_rollback();
                return ['message' => 'Hata Oluştu..', 'result' => false, 'Error' => isset($result['Data']) ? $result['Data'] : 'Data Yok'];
            }
        } catch (Exception $th) {
            $errsection = $th;
            //rollback query on error
            $this->CI->db->trans_rollback();
            if (strlen($this->CI->db->error()['message']) > 1) {
                $errsection = $this->CI->db->error();
            }
            return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
        }
    }

    /**
     * this method will do address types transactions
     * if transactions is success it will return true
     * TR : Sistem adres tipleri işlemlerinin gerçekleştiği bölgedir.
     */
    public function sysatevents($params)
    {
        try {
            //it will load model
            $this->CI->load->model('Sys_Ceviriler_model');
            $this->CI->load->model('Sys_Adrestipleri_model');
            $model = $this->CI->Sys_Adrestipleri_model;
            $dilmodel = $this->CI->Sys_Ceviriler_model;
            $result = false;
            //start query transaction 
            $this->CI->db->trans_begin();
            //it will decide to transaction from 'event' parameter
            switch ($params['event']) {
                case "add":
                    $baslik_dil_key = 'd_AdresTipi_' . str_replace(".", "", microtime(true));
                    //it will create object 
                    $bobj =  array(
                        'sys_at_baslik' => $params['sys_at_baslik'],
                        'baslik_dil_key' => $baslik_dil_key,
                        'created_by' => $params['_wid'],
                        'sys_at_durum' => 1
                    );
                    //it will turn result 
                    $result =  $model->Add($bobj);
                    //insert language informations 
                    if (count($params['language']['sys_at_baslik']) > 0) {
                        foreach ($params['language']['sys_at_baslik'] as $l) {
                            $obj =  array(
                                'anahtar' => $baslik_dil_key,
                                'sys_cev_dil_id' => $l['langid'],
                                'sys_cev_sayfa' => 'cari_durum',
                                'sys_cev_ceviri' => $l['value']
                            );
                            $result = $dilmodel->Add($obj);
                        }
                    }
                    //
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $bobj['sys_at_baslik'], 'log_tip' => 'master', 'ref' => '', 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_AtEkle']);
                    break;
                case "update":
                    //it will create object  
                    $obj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    $oldObj = $obj;
                    foreach ($params as $k => $v) {
                        if (array_key_exists($k, $obj)) {
                            $obj[$k] = $v;
                        }
                    }
                    $obj['updated_by'] = $params['_wid'];
                    $obj['updated_on'] = date('Y-m-d h:i:s');
                    //it will turn result 
                    $result = $model->Update($obj);
                    //update language rows
                    if (count($params['language']['sys_at_baslik']) > 0) {
                        foreach ($params['language']['sys_at_baslik'] as $l) {
                            //create array (id must be come with posted data)
                            $obj =  array(
                                'id' => $l['id'],
                                'sys_cev_ceviri' => $l['value'],
                                'updated_by' => $params['_wid'],
                                'updated_on' => date('Y-m-d h:i:s')
                            );
                            //update record
                            $result = $dilmodel->Update($obj);
                        }
                    }
                    ///
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['sys_at_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_AtGuncel']);
                    break;
                case "trash":
                    $oldObj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    //it will create object 
                    $obj =  array(
                        'id' => $params['id'],
                        'sys_at_durum' => 0,
                        'deleted_by' => $params['_wid'],
                        'deleted_on' => date('Y-m-d h:i:s')
                    );
                    //it will turn result 
                    $result = $model->Update($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['sys_at_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_AtCop']);
                    break;
                case "get":
                    if (isset($params['where'])) {
                        $result = $model->Get($params['where']);
                    } else {
                        $result = $model->Get();
                    }
                    break;
            }

            //if result is true it will return true
            if ($result['Result']) {
                //commit query if everything is okey
                $this->CI->db->trans_commit();
                return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
            } else {
                //rollback query on error
                $this->CI->db->trans_rollback();
                return ['message' => 'Hata Oluştu..', 'result' => false, 'Error' => isset($result['Data']) ? $result['Data'] : 'Data Yok'];
            }
        } catch (Exception $th) {
            $errsection = $th;
            //rollback query on error
            $this->CI->db->trans_rollback();
            if (strlen($this->CI->db->error()['message']) > 1) {
                $errsection = $this->CI->db->error();
            }
            return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
        }
    }
    
    /**
     * this method will do client personel position transactions
     * if transactions is success it will return true
     * TR : Cari personel pozisyon işlemlerinin gerçekleştiği bölgedir.
     */
    public function ppozisyonevents($params)
    {
        try {
            //it will load model
            $this->CI->load->model('Sys_Ceviriler_model');
            $this->CI->load->model('Per_Pozisyon_model');
            $model =  $this->CI->Per_Pozisyon_model;
            $dilmodel = $this->CI->Sys_Ceviriler_model;
            $result = false;
            //start query transaction 
            $this->CI->db->trans_begin();
            //it will decide to transaction from 'event' parameter
            switch ($params['event']) {
                case "add":
                    $baslik_dil_key = 'd_Per_Pozisyon_' . str_replace(".", "", microtime(true));
                    //it will create object 
                    $bobj =  array(
                        'per_poz_baslik' => $params['per_poz_baslik'],
                        'baslik_dil_key' => $baslik_dil_key,
                        'created_by' => $params['_wid'],
                        'per_poz_durum' => 1
                    );
                    //it will turn result 
                    $result =  $model->Add($bobj);
                    //insert language informations 
                    if (count($params['language']['per_poz_baslik']) > 0) {
                        foreach ($params['language']['per_poz_baslik'] as $l) {
                            $obj =  array(
                                'anahtar' => $baslik_dil_key,
                                'sys_cev_dil_id' => $l['langid'],
                                'sys_cev_sayfa' => 'per_poz',
                                'sys_cev_ceviri' => $l['value']
                            );
                            $result = $dilmodel->Add($obj);
                        }
                    }
                    //
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $bobj['per_poz_baslik'], 'log_tip' => 'master', 'ref' => '', 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_PPozEkle']);
                    break;
                case "update":
                    //it will create object  
                    $obj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    $oldObj = $obj;
                    foreach ($params as $k => $v) {
                        if (array_key_exists($k, $obj)) {
                            $obj[$k] = $v;
                        }
                    }
                    $obj['updated_by'] = $params['_wid'];
                    $obj['updated_on'] = date('Y-m-d h:i:s');
                    //it will turn result 
                    $result = $model->Update($obj);
                    //update language rows
                    if (count($params['language']['per_poz_baslik']) > 0) {
                        foreach ($params['language']['per_poz_baslik'] as $l) {
                            //create array (id must be come with posted data)
                            $obj =  array(
                                'id' => $l['id'],
                                'sys_cev_ceviri' => $l['value'],
                                'updated_by' => $params['_wid'],
                                'updated_on' => date('Y-m-d h:i:s')
                            );
                            //update record
                            $result = $dilmodel->Update($obj);
                        }
                    }
                    ///
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['per_poz_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_PPozGuncel']);
                    break;
                case "trash":
                    $oldObj = (array) $model->Get(['id' => $params['id']])['Data'][0];
                    //it will create object 
                    $obj =  array(
                        'id' => $params['id'],
                        'per_poz_durum' => 0,
                        'deleted_by' => $params['_wid'],
                        'deleted_on' => date('Y-m-d h:i:s')
                    );
                    //it will turn result 
                    $result = $model->Update($obj);
                    _log(['log_kul_id' => $params['_wid'], 'log_ip' => $params['_ip'], 'log_aciklama' => $oldObj['per_poz_baslik'], 'log_tip' => 'master', 'ref' => 'id-' . $params['id'], 'created_by' => $params['_wid'], 'log_lokasyon' => 'falna*falan', 'log_aksiyon' => 'l_PPozCop']);
                    break;
                case "get":
                    if (isset($params['where'])) {
                        $result = $model->Get($params['where']);
                    } else {
                        $result = $model->Get();
                    }
                    break;
            }

            //if result is true it will return true
            if ($result['Result']) {
                //commit query if everything is okey
                $this->CI->db->trans_commit();
                return ['message' => 'İşlem Başarılı..', 'result' => true, 'data' => $result['Data']];
            } else {
                //rollback query on error
                $this->CI->db->trans_rollback();
                return ['message' => 'Hata Oluştu..', 'result' => false, 'Error' => isset($result['Data']) ? $result['Data'] : 'Data Yok'];
            }
        } catch (Exception $th) {
            $errsection = $th;
            //rollback query on error
            $this->CI->db->trans_rollback();
            if (strlen($this->CI->db->error()['message']) > 1) {
                $errsection = $this->CI->db->error();
            }
            return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
        }
    }
    #region System Events

    /**
     * Check username is valid or not
     * tr : Kullanıcı adı geçerlimi değilmi kontrol et
     */
    public function validevents($params){
        try {
            switch($params['event']){
                case 'username':
                    /**
                     * load users model
                     * tr : kullanıcılar modelini yükle
                     */
                    $this->CI->load->model('Kullanicilar_model');
                    $User = $this->CI->Kullanicilar_model->Get(['kul_kadi' => $params['username']]);
                    /**
                     * if user is exist return false
                     * tr : eğer kullanıcı mevcut false döndür
                     */
                    if ($User['Result']) {
                        if(isset($params['ref']) && $params['ref']==$User['Data'][0]->ref){
                            return ['message' => 'Kullanıcı Mevcut Değil', 'result' => true];
                        }
                        return ['message' => 'Kullanıcı Mevcut', 'result' => false];
                    }
                    return ['message' => 'Kullanıcı Mevcut Değil', 'result' => true];
            }
               
        } catch (Exception $th) {
            $errsection = $th;
            log_message('error',$errsection);
            //rollback query on error
            if (strlen($this->CI->db->error()['message']) > 1) {
                $errsection = $this->CI->db->error();
            }
            return ['message' => 'Hata Oluştu Arge Departmanına Başvurunuz..', 'result' => false, 'Error' => $errsection];
        }
    }


    #endregion

}
