<?php

/**
 * this model will only do "urun_parametre_model" table transactions
 */

class urun_parametre_model extends CI_Model
{
    private $TableName = "urun_parametre";
    /**
     * row id as integer
     */
    public $id;
    /**
     * referance code as string
     */
    public $ref;
    /**
     * maximum stock limit as numeric
     */
    public $ur_par_maxstok;
    /**
     * minimum stock limit as numeric
     */
    public $ur_par_minstok;
    /**
     * description as string
     */
    public $ur_par_stokaciklama;
    /**
     * status as integer
     */
    public $ur_par_hareketbirim;
    /**
     * minimum order value as numeric
     */
    public $ur_par_min_sipmik;
    /**
     * maximum order value as numeric
     */
    public $ur_par_mak_sipmik;
    /**
     * order unit id as integer
     */
    public $ur_par_sipbir;
    /**
     * product minimum order day as integer 
     */
    public $ur_par_min_tedsuresi;
    /**
     * product account number 1
     */
    public $ur_par_al_hesap;
    /**
     * product account number 2
     */
    public $ur_par_al_iadehesap;
    /**
     * product account number 3
     */
    public $ur_par_sat_hesap;
    /**
     * product account number 3
     */
    public $ur_par_sat_iadehesap;

    public $created_by;
    public $created_on;
    public $updated_by;
    public $updated_on;
    public $deleted_by;
    public $deleted_on;
    public $undeleted_by;
    public $undeleted_on;

    /**
     * this method will return rows from database
     * if user will send  ['Key'=>'Value','OtherKey'=>'OtherValue'] to method , this will add  "where" parameter to sql query
     */
    public function Get($where = null)
    {
        try {
            //if parameter will come add "where" to sql query
            if (!is_null($where)) {
                $this->db->where($where);
            }

            //return rows if result is not empty
            $return = $this->db->get($this->TableName)->result();
            if (!empty($return)) {
                return ['Data' => $return, 'Result' => true];
            }
            return ['Result' => false, 'Data' => 'No Data'];
        } catch (\Throwable $th) {
            //if error will happen it will write error
            return ['Result' => false, 'Data' => $th];
        }
    }

    /**
     * this method will add row to database
     * 'item' parameter will include keys of this model as array
     */
    public function Add($item)
    {
        try {
            //if transaction will complete it will return true 
            if ($this->db->insert($this->TableName, $item)) {
                return ['Result' => true, 'Data' => 'true'];
            }
            return ['Result' => false, 'Data' => 'false'];
        } catch (\Throwable $th) {
            //if error will happen it will write error 
            return ['Result' => false, 'Data' => $th];
        }
    }

    /**
     * this method will update row in database
     * 'item' parameter will include keys of this model as array
     * always send id because is primary key and "where" param will search by id
     */
    public function Update($item)
    {
        try {
            //if transaction will complete it will return true 
            if (isset($item['id'])) {
                $this->db->where('id', $item['id']);
                return $this->db->update($this->TableName, $item) ? ['Result' => true, 'Data' => 'true'] : ['Result' => false, 'Data' => 'false'];
            }
            return ['Result' => false, 'Data' => 'false'];
        } catch (\Throwable $th) {
            //if error will happen it will write error 
            return ['Result' => false, 'Data' => $th];
        }
    }
}
