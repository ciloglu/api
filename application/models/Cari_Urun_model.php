<?php

/**
 * this model will only do "Cari_Urun_model" table transactions
 */

class Cari_Urun_model extends CI_Model
{
    private $TableName = "cari_urun";


    /**
     * row id as integer
     */
    public $id;
    /**
     * client referance code as string
     */
    public $ref;
    /**
     * product id as integer
     */
    public $cari_ur_id;
    /**
     * Price as int
     */
    public $cari_ur_fiyat;
    /**
     * Old Price as string
     */
    public $cari_ur_nfiyat;

    /**
     * date as date
     */
    public $cari_ur_tarih;
    /**
     * vat as integer
     */
    public $cari_ur_kdv;
    /**
     * discount as integer
     */
    public $cari_ur_iskonto;

    /**
     * Status as int
     */
    public $cari_ur_durum;

    /**
     * log infos as Date and worker id
     */
    public $created_by;
    public $created_on;
    public $updated_by;
    public $updated_on;
    public $deleted_by;
    public $deleted_on;
    public $undeleted_by;
    public $undeleted_on;

    /**
     * this method will return rows from database
     * if user will send  ['Key'=>'Value','OtherKey'=>'OtherValue'] to method , this will add  "where" parameter to sql query
     */
    public function Get($where = null)
    {
        try {
            //if parameter will come add "where" to sql query
            if (!is_null($where)) {
                $this->db->where($where);
            }
            //join product table
            $this->db->join('urun', 'urun.id =' . $this->TableName . '.cari_ur_id');
            //make select for organized data
            $this->db->select($this->TableName . '.id as id ,urun.urun_ad as ad,' . $this->TableName . '.cari_ur_id,' . $this->TableName . '.cari_ur_fiyat,' . $this->TableName . '.cari_ur_nfiyat,' . $this->TableName . '.cari_ur_tarih,' . $this->TableName . '.cari_ur_kdv,' . $this->TableName . '.cari_ur_iskonto,' . $this->TableName . '.cari_ur_durum,' . $this->TableName . '.ref');
            //return rows if result is not empty
            $return = $this->db->get($this->TableName)->result();

            if (!empty($return)) {
                return ['Data' => $return, 'Result' => true];
            }
            // result is empty
            return ['Result' => false];
        } catch (\Throwable $th) {
            //if error will happen it will write error
            return ['Result' => false, 'Data' => $th];
        }
    }


    /**
     * this method will add row to database
     * 'item' parameter will include keys of this model as array
     */
    public function Add($item)
    {
        try {
            //if transaction will complete it will return true 
            if ($this->db->insert($this->TableName, $item)) {
                return ['Result' => true, 'Data' => 'true'];
            }
            return ['Result' => false, 'Data' => 'false'];
        } catch (\Throwable $th) {
            //if error will happen it will write error 
            return ['Result' => false, 'Data' => $th];
        }
    }

    /**
     * this method will update row in database
     * 'item' parameter will include keys of this model as array
     * always send id because is primary key and "where" param will search by id
     */
    public function Update($item)
    {
        try {
            //if transaction will complete it will return true 
            if (isset($item['id'])) {
                $this->db->where('id', $item['id']);
                return $this->db->update($this->TableName, $item) ? ['Result' => true, 'Data' => 'true'] : ['Result' => false, 'Data' => 'false'];
            }
            return ['Result' => false, 'Data' => 'false'];
        } catch (\Throwable $th) {
            //if error will happen it will write error 
            return ['Result' => false, 'Data' => $th];
        }
    }
}
