<?php
/**
 * this model will only do "Personel_model" table transactions
 */

class Personel_model extends CI_Model
{
    private $TableName = "personel";
    /**
     * row id as integer
     */
    public $id;
    /**
     * worker referance code as string 
     */
    public $ref;
    /**
     * personel image as string
     */
    public $per_img;
    /**
     * personel name as string
     */
    public $per_ad;
    /**
     * personel id number as string
     */
    public $per_kimlikno;
    /**
     * personel birth date as date
     */
    public $per_dogtarih;
    /**
     * personel birth place as string
     */
    public $per_dogyer;
    /**
     * personel mother name as string
     */
    public $per_annead;
    /**
     * personel father name as string
     */
    public $per_babaad;
    /**
     * personel military status as integer
     */
    public $per_askerlikdurum;
    /**
     * personel marital status as integer
     */
    public $per_medenidurum;
    /**
     * personel gender as integer
     */
    public $per_cinsiyet;
    /**
     * supervisor as integer
     */
    public $per_sorumlu;
    /**
     * personel id card as string
     */
    public $per_kimlik;
    /**
     * personel blood type as string
     */
    public $per_kangrup;
    /**
     * personel driver licanse(us)  type as string
     */
    public $per_surucubel;
    /**
     * personel enter date as date
     */
    public $per_giris;
    /**
     * personel annual leave limit as date
     */
    public $per_yillikizin;
     /**
     * personel accounting code as date
     */
    public $per_muhkod;
     /**
     * personel description as date
     */
    public $per_aciklama;
    /**
     * personel status as integer (id passive or active)
     */
    public $per_durum;
    
    public $created_by;
    public $created_on;
    public $updated_by;
    public $updated_on;
    public $deleted_by;
    public $deleted_on;
    public $undeleted_by;
    public $undeleted_on;

    /**
     * this method will return rows from database
     * if user will send  ['Key'=>'Value','OtherKey'=>'OtherValue'] to method , this will add  "where" parameter to sql query
     */
    public function Get($where=null){
        try {
            //if parameter will come add "where" to sql query
            if(!is_null($where)){
                $this->db->where($where);
            }
            //return rows if result is not empty
            $return=$this->db->get($this->TableName)->result();
            
            if(!empty($return)){
                return ['Data'=>$return,'Result'=>true];
            }
            // result is empty
            return ['Result'=>false];
        } catch (\Throwable $th) {
            //if error will happen it will write error
            return ['Result'=>false,'Data'=>$th];
        }
    }


    /**
     * this method will add row to database
     * 'item' parameter will include keys of this model as array
     */
    public function Add($item){
        try {
            //if transaction will complete it will return true 
            if($this->db->insert($this->TableName, $item)){
                return ['Result'=>true,'Data'=>'true'];
            }
            return ['Result'=>false,'Data'=>'false'];
        } catch (\Throwable $th) {
            //if error will happen it will write error 
            return ['Result'=>false,'Data'=>$th];
        }
    }

    /**
     * this method will update row in database
     * 'item' parameter will include keys of this model as array
     * always send id because is primary key and "where" param will search by id
     */
    public function Update($item){
        try {
            //if transaction will complete it will return true 
            if(isset($item['id'])){
                $this->db->where('id', $item['id']);
                return $this->db->update($this->TableName, $item) ? ['Result'=>true,'Data'=>'true'] : ['Result'=>false,'Data'=>'false'];
            }
            return ['Result'=>false,'Data'=>'false'];
        } catch (\Throwable $th) {
            //if error will happen it will write error 
            return ['Result'=>false,'Data'=>$th];
        }
    }
}