<?php
/**
 * this model will only do "Cari_model" table transactions
 */

class Cari_model extends CI_Model
{
    private $TableName = "cari";


    /**
     * row id as integer
     */
    public $id;
    /**
     * client referance code as string
     */
    public $ref;
    /**
     * client code as string
     */
    public $cari_kod;
    /**
     * parent code as string
     */
    public $cari_ukod;
    /**
     * client short name as string
     */
    public $cari_kadi;
    /**
     * client long name as string
     */
    public $cari_unvan;
    /**
     * client status as int
     */
    public $cari_durum;
    /**
     * client type as int
     */
    public $cari_tipi;
    /**
     * client phone as string
     */
    public $cari_telgsm;
    /**
     * client office phone as string
     */
    public $cari_telofis;
    /**
     * client fax number as string
     */
    public $cari_fax;
    /**
     * web site as string
     */
    public $cari_web;
    /**
     * client instagram as string
     */
    public $cari_instagram;
    /**
     * client facebook as string
     */
    public $cari_facebook;
    /**
     * client twitter as string
     */
    public $cari_twitter;
    /**
     * client email as string
     */
    public $cari_mail;
    /**
     * client country as int
     */
    public $cari_ulke;
    /**
     * client city as int
     */
    public $cari_sehir;
    /**
     * client zip code as string
     */
    public $cari_pkodu;
    /**
     * client state as string
     */
    public $cari_eyalet;
    /**
     * client latitude as string
     */
    public $cari_enlem;
    /**
     * client longitude as string
     */
    public $cari_boylam;
    /**
     * client address as string
     */
    public $cari_adres;
    /**
     * client image file way as string
     */
    public $cari_img;
    /**
     * company id  as int
     */
    public $cari_sirid;
    /**
     * Status as int
     */
    public $cari_sdurum;
    
    
    /**
     * log infos as Date and worker id
     */
    public $created_by;
    public $created_on;
    public $updated_by;
    public $updated_on;
    public $deleted_by;
    public $deleted_on;
    public $undeleted_by;
    public $undeleted_on;
   
    /**
     * this method will return rows from database
     * if user will send  ['Key'=>'Value','OtherKey'=>'OtherValue'] to method , this will add  "where" parameter to sql query
     */
    public function Get($where=null){
        try {
            //if parameter will come add "where" to sql query
            if(!is_null($where)){
                $this->db->where($where);
            }
            //return rows if result is not empty
            $return=$this->db->get($this->TableName)->result();
            
            if(!empty($return)){
                return ['Data'=>$return,'Result'=>true];
            }
            // result is empty
            return ['Result'=>false];
        } catch (\Throwable $th) {
            //if error will happen it will write error
            return ['Result'=>false,'Data'=>$th];
        }
    }


    /**
     * this method will add row to database
     * 'item' parameter will include keys of this model as array
     */
    public function Add($item){
        try {
            //if transaction will complete it will return true 
            if($this->db->insert($this->TableName, $item)){
                return ['Result'=>true,'Data'=>'true'];
            }
            return ['Result'=>false,'Data'=>'false'];
        } catch (\Throwable $th) {
            //if error will happen it will write error 
            return ['Result'=>false,'Data'=>$th];
        }
    }

    /**
     * this method will update row in database
     * 'item' parameter will include keys of this model as array
     * always send id because is primary key and "where" param will search by id
     */
    public function Update($item){
        try {
            //if transaction will complete it will return true 
            if(isset($item['id'])){
                $this->db->where('id', $item['id']);
                return $this->db->update($this->TableName, $item) ? ['Result'=>true,'Data'=>'true'] : ['Result'=>false,'Data'=>'false'];
            }
            return ['Result'=>false,'Data'=>'false'];
        } catch (\Throwable $th) {
            //if error will happen it will write error 
            return ['Result'=>false,'Data'=>$th];
        }
    }
}