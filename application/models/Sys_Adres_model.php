<?php

/**
 * this model will only do "Cari_Tipi_model" table transactions
 */

class Sys_Adres_model extends CI_Model
{
    private $TableName = "sys_adres";


    /**
     * row id as integer
     */
    public $id;
    /**
     * table referance code as string
     */
    public $ref;
    /**
     * Title as string
     */
    public $sya_ad_baslik;
    /**
     * Latitude as string
     */
    public $sys_ad_enlem;
    /**
     * Logitude as string
     */
    public $sys_ad_boylam;
    /**
     * Description as string
     */
    public $sys_ad_aciklama;
    /**
     * Refeance type as int
     */
    public $sys_ad_tip;
    /**
     * Country as int
     */
    public $sys_ad_ulke;
    /**
     * City as int
     */
    public $sys_ad_sehir;
    /**
     * State as int
     */
    public $sys_ad_eyalet;
    /**
     * Address category as int
     */
    public $sys_ad_kat;
    /**
     * Status as int
     */
    public $sys_ad_durum;

    /**
     * log infos as Date and worker id
     */
    public $created_by;
    public $created_on;
    public $updated_by;
    public $updated_on;
    public $deleted_by;
    public $deleted_on;
    public $undeleted_by;
    public $undeleted_on;

    /**
     * this method will return rows from database
     * if user will send  ['Key'=>'Value','OtherKey'=>'OtherValue'] to method , this will add  "where" parameter to sql query
     */
    public function Get($where = null)
    {
        try {
            //if parameter will come add "where" to sql query
            if (!is_null($where)) {
                $this->db->where($where);
            }
            //return rows if result is not empty
            $return = $this->db->get($this->TableName)->result();
            if (!empty($return)) {
                return ['Data' => $return, 'Result' => true];
            }
            // result is empty
            return ['Result' => false];
        } catch (\Throwable $th) {
            //if error will happen it will write error
            return ['Result' => false, 'Data' => $th];
        }
    }


    /**
     * this method will add row to database
     * 'item' parameter will include keys of this model as array
     */
    public function Add($item)
    {
        try {
            //if transaction will complete it will return true 
            if ($this->db->insert($this->TableName, $item)) {
                return ['Result' => true, 'Data' => 'true'];
            }
            return ['Result' => false, 'Data' => 'false'];
        } catch (\Throwable $th) {
            //if error will happen it will write error 
            return ['Result' => false, 'Data' => $th];
        }
    }

    /**
     * this method will update row in database
     * 'item' parameter will include keys of this model as array
     * always send id because is primary key and "where" param will search by id
     */
    public function Update($item)
    {
        try {
            //if transaction will complete it will return true 
            if (isset($item['id'])) {
                $this->db->where('id', $item['id']);
                return $this->db->update($this->TableName, $item) ? ['Result' => true, 'Data' => 'true'] : ['Result' => false, 'Data' => 'false'];
            }
            return ['Result' => false, 'Data' => 'false'];
        } catch (\Throwable $th) {
            //if error will happen it will write error 
            return ['Result' => false, 'Data' => $th];
        }
    }
}
