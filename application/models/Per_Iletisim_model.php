<?php
/**
 * this model will only do "Per_Iletisim_model" table transactions
 */

class Per_Iletisim_model extends CI_Model
{
    private $TableName = "per_iletisim";


    /**
     * row id as integer
     */
    public $id;
     /**
     * personel referance code as string
     */
    public $ref;
    /**
     * phone as string
     */
    public $per_il_ceptel;
    /**
     * job phone as string
     */
    public $per_il_istel;
    /**
     * mail as string
     */
    public $per_il_mail;
    /**
     * job mail as string
     */
    public $per_il_ismail;
    /**
     * Relative person phone as string
     */
    public $per_il_yakceptel;
    /**
     * Relative person desc as string
     */
    public $per_il_yakdiger;
    /**
     * Relative person status  as string
     */
    public $per_il_yekder;
    /**
     * Country as string
     */
    public $per_il_ulke;
    /**
     * City as string
     */
    public $per_il_sehir;
    /**
     * State / Area as string
     */
    public $per_il_bolge;
    /**
     * Latitude as string
     */
    public $per_il_enlem;
    /**
     * Longitude as string
     */
    public $per_il_boylem;
    /**
     * address as string
     */
    public $per_il_adres;
    /**
     * facebook as string
     */
    public $per_il_facebook;
    /**
     * instagram as string
     */
    public $per_il_instagram;
    /**
     * twitter as string
     */
    public $per_il_twitter;
    /**
     * linkedin as string
     */
    public $per_il_linkedin;
   
    /**
     * log infos as date and worker id
     */
    public $created_by;
    public $created_on;
    public $updated_by;
    public $updated_on;
    public $deleted_by;
    public $deleted_on;
    public $undeleted_by;
    public $undeleted_on;
   
    /**
     * this method will return rows from database
     * if user will send  ['Key'=>'Value','OtherKey'=>'OtherValue'] to method , this will add  "where" parameter to sql query
     */
    public function Get($where=null){
        try {
            //if parameter will come add "where" to sql query
            if(!is_null($where)){
                $this->db->where($where);
            }
            //return rows if result is not empty
            $return=$this->db->get($this->TableName)->result();
            if(!empty($return)){
                return ['Data'=>$return,'Result'=>true];
            }
            return ['Result'=>false];
        } catch (\Throwable $th) {
            //if error will happen it will write error
            return ['Result'=>false,'Data'=>$th];
        }
    }


    /**
     * this method will add row to database
     * 'item' parameter will include keys of this model as array
     */
    public function Add($item){
        try {
            //if transaction will complete it will return true 
            if($this->db->insert($this->TableName, $item)){
                return ['Result'=>true,'Data'=>'true'];
            }
            return ['Result'=>false,'Data'=>'false'];
        } catch (\Throwable $th) {
            //if error will happen it will write error 
            return ['Result'=>false,'Data'=>$th];
        }
    }

    /**
     * this method will update row in database
     * 'item' parameter will include keys of this model as array
     * always send id because is primary key and "where" param will search by id
     */
    public function Update($item){
        try {
            //if transaction will complete it will return true 
            if(isset($item['id'])){
                $this->db->where('id', $item['id']);
                return $this->db->update($this->TableName, $item) ? ['Result'=>true,'Data'=>'true'] : ['Result'=>false,'Data'=>'false'];
            }
            return ['Result'=>false,'Data'=>'false'];
        } catch (\Throwable $th) {
            //if error will happen it will write error 
            return ['Result'=>false,'Data'=>$th];
        }
    }
}