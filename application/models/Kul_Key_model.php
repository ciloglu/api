<?php
/**
 * this model will only do "kul_Key_model" table transactions
 */

class Kul_Key_model extends CI_Model
{
    private $TableName = "kul_key";
    /**
     * row id as integer
     */
    public $id;
    /**
     * Kullanicilar table referance id as integer
     */
    public $kul_key_refid;
    /**
     * api token as string
     */
    public $kul_key_token;
    /**
     * token expre date as  timestamp
     */
    public $kul_key_expire;
    /**
     * key is active or pasive as integer
     */
    public $kul_key_status;
    /**
     * user hash key for client machine
     */
    public $kul_key_hash;

    /**
     * log infos as daten and worker id
     */
    public $created_by;
    public $created_on;
    public $updated_by;
    public $updated_on;
    public $deleted_by;
    public $deleted_on;
    public $undeleted_by;
    public $undeleted_on;
   
    /**
     * this method will return rows from database
     * if user will send  ['Key'=>'Value','OtherKey'=>'OtherValue'] to method , this will add  "where" parameter to sql query
     */
    public function Get($where=null){
        try {
            //if parameter will come add "where" to sql query
            if(!is_null($where)){
                $this->db->where($where);
            }
            //return rows if result is not empty
            $return=$this->db->get($this->TableName)->result();
            if(!empty($return)){
                return ['Data'=>$return,'Result'=>true];
            }
            return ['Result'=>false];
        } catch (\Throwable $th) {
            //if error will happen it will write error
            return ['Result'=>false,'Data'=>$th];
        }
    }


    /**
     * this method will add row to database
     * 'item' parameter will include keys of this model as array
     * example ['Per_key_Refid'=>1,'Per_key_Token'=>'falan']
     */
    public function Add($item){
        try {
            //if transaction will complete it will return true 
            if($this->db->insert($this->TableName, $item)){
                return ['Result'=>true];
            }
            return ['Result'=>false];
        } catch (\Throwable $th) {
            //if error will happen it will write error 
            return ['Result'=>false,'Data'=>$th];
        }
    }

    /**
     * this method will update row in database
     * 'item' parameter will include keys of this model as array
     * example ['Per_key_Refid'=>1,'Per_key_Token'=>'falan']
     * always send id because is primary key and "where" param will search by id
     */
    public function Update($item){
        try {
            //if transaction will complete it will return true 
            if(isset($item['id'])){
                $this->db->where('id', $item['id']);
                return $this->db->update($this->TableName, $item) ? ['Result'=>true] : ['Result'=>false];
            }
            return ['Result'=>false];
        } catch (\Throwable $th) {
            //if error will happen it will write error 
            return ['Result'=>false,'Data'=>$th];
        }
    }
}