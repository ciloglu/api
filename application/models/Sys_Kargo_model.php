<?php
/**
 * this model will only do "sys_kargo_model" table transactions
 */

class sys_kargo_model extends CI_Model
{
    private $TableName = "sys_kargo";
    /**
     * row id as integer
     */
    public $id;
    /**
     * title as string
     */
    public $sys_car_baslik;
    /**
     * personel as string
     */
    public $sys_car_yetkili;
    /**
     * address as string
     */
    public $sys_car_adres;
    /**
     * description as string
     */
    public $sys_car_aciklama;
    /**
     * telefon as string
     */
    public $sys_car_tel;
    /**
     * status as int
     */
    public $sys_car_durum;
   
    public $created_by;
    public $created_on;
    public $updated_by;
    public $updated_on;
    public $deleted_by;
    public $deleted_on;
    public $undeleted_by;
    public $undeleted_on;

     /**
     * this method will return rows from database
     * if user will send  ['Key'=>'Value','OtherKey'=>'OtherValue'] to method , this will add  "where" parameter to sql query
     */
    public function Get($where=null){
        try {
             //if parameter will come add "where" to sql query
            if(!is_null($where)){
                $this->db->where($where);
            }
            //return rows if result is not empty
            $return=$this->db->get($this->TableName)->result();
            if(!empty($return)){
                return ['Data'=>$return,'Result'=>true];
            }
            return ['Result'=>false,'Data'=>'No Data'];
        } catch (\Throwable $th) {
             //if error will happen it will write error
             return ['Result'=>false,'Data'=>$th];
        }
    }

    /**
     * this method will add row to database
     * 'item' parameter will include keys of this model as array
     */
    public function Add($item){
        try {
            //if transaction will complete it will return true 
            if($this->db->insert($this->TableName, $item)){
                return ['Result'=>true,'Data'=>'true'];
            }
            return ['Result'=>false,'Data'=>'false'];
        } catch (\Throwable $th) {
            //if error will happen it will write error 
            return ['Result'=>false,'Data'=>$th];
        }
    }

    /**
     * this method will update row in database
     * 'item' parameter will include keys of this model as array
     * always send id because is primary key and "where" param will search by id
     */
    public function Update($item){
        try {
            //if transaction will complete it will return true 
            if(isset($item['id'])){
                $this->db->where('id', $item['id']);
                return $this->db->update($this->TableName, $item) ? ['Result'=>true,'Data'=>'true'] : ['Result'=>false,'Data'=>'false'];
            }
            return ['Result'=>false,'Data'=>'false'];
        } catch (\Throwable $th) {
            //if error will happen it will write error 
            return ['Result'=>false,'Data'=>$th];
        }
    }
}